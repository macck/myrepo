﻿using EntityHomework.Model;
using EntityHomework.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityHomework.Repository
{
    class RWEntityHomework<T> : IRWEntityHomework<T> where T : Entity
    {
        private readonly EntityHomeworkContext _context;

        public RWEntityHomework(EntityHomeworkContext context)
        {
            this._context = context;
        }

        public void AddBuilding(T building, int userId)
        {
            throw new NotImplementedException();
        }

        public void AddHero(T hero, int userId)
        {
            throw new NotImplementedException();
        }

        public bool CreateUser(T entity, string nickName)
        {

            if (_context.Users.Where(x => x.NickName == nickName).Count() == 0)
            {
                _context.Set<T>().Add(entity);
                _context.SaveChanges();
                return true;
            }
            else
                return false;

        }

        public IList<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public IList<T> GetAllBuildings(int userId)
        {
            throw new NotImplementedException();
        }

        public IList<T> GetAllHeroes(int userId)
        {
            throw new NotImplementedException();
        }

        public IList<T> GetAllResources(int userId)
        {
            throw new NotImplementedException();
        }

        public T GetBuilding(int userId, int buildingId)
        {
            throw new NotImplementedException();
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Where(x => x.Id == id).First();
        }

        public int ValidateLogin(string nickName, string password)
        {
            if (_context.Users.Where(x => x.NickName == nickName).Where(y => y.Password == password).Count() == 1)
            {
                return _context.Users.Where(x => x.NickName == nickName).Select(i => i.Id).FirstOrDefault();
            }
            else return 0;
          
        }
        public IList<int> GetIds()
        {
            return _context.Users.Select(i => i.Id).ToList();
            

        }


        public T GetHero(int userId, int heroId)
        {
            throw new NotImplementedException();
        }

        public T GetResource(int userId, int resourceId)
        {
            throw new NotImplementedException();
        }

        public IList<User> GetUsers()
        {
            var userList = _context.Set<User>().ToList();
            return userList;
        }

        public void EditSession(T entity)
        {
            _context.Entry<T>(entity).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }
    }
}
