/*
 * LabeledDataSource.h
 *
 *  Created on: 02.12.2016
 *      Author: Macck
 */

#ifndef LABELEDDATASOURCE_H_
#define LABELEDDATASOURCE_H_

#include <vector>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include "inttypes.h"
#include <iostream>
#include "DataSource.h"

class LabeledDataSource : DataSource {

private:
			bool hasDataLeft; // to indicate if there is anything to read in input str
			bool isFileOpen;
			bool dataUsed; // flag to test if string have been already parsed into vector of ints
			std::fstream inputStream;

public:
	LabeledDataSource(const std::string & path);
	virtual ~LabeledDataSource();
};

#endif /* LABELEDDATASOURCE_H_ */
