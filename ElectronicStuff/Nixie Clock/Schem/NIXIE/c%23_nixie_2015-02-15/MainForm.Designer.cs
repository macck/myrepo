﻿namespace nixie
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolSettings = new System.Windows.Forms.ToolStripButton();
            this.toolAbout = new System.Windows.Forms.ToolStripButton();
            this.toolExit = new System.Windows.Forms.ToolStripButton();
            this.toolConnect = new System.Windows.Forms.ToolStripButton();
            this.toolSet = new System.Windows.Forms.ToolStripButton();
            this.toolScroll = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolConnect,
            this.toolSet,
            this.toolScroll});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(274, 46);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip4";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolSettings,
            this.toolAbout,
            this.toolExit});
            this.toolStrip2.Location = new System.Drawing.Point(0, 46);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(243, 46);
            this.toolStrip2.TabIndex = 4;
            this.toolStrip2.Text = "toolStrip5";
            // 
            // toolSettings
            // 
            this.toolSettings.AutoSize = false;
            this.toolSettings.Image = global::nixie.Properties.Resources.gtk_preferences;
            this.toolSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolSettings.Name = "toolSettings";
            this.toolSettings.Size = new System.Drawing.Size(80, 43);
            this.toolSettings.Text = "Ustawienia";
            this.toolSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolSettings.Click += new System.EventHandler(this.toolSettings_Click);
            // 
            // toolAbout
            // 
            this.toolAbout.AutoSize = false;
            this.toolAbout.Image = global::nixie.Properties.Resources.dialog_information_;
            this.toolAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolAbout.Name = "toolAbout";
            this.toolAbout.Size = new System.Drawing.Size(80, 43);
            this.toolAbout.Text = "O programie";
            this.toolAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolAbout.Click += new System.EventHandler(this.toolAbout_Click);
            // 
            // toolExit
            // 
            this.toolExit.AutoSize = false;
            this.toolExit.Image = global::nixie.Properties.Resources.application_exit;
            this.toolExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolExit.Name = "toolExit";
            this.toolExit.Size = new System.Drawing.Size(80, 43);
            this.toolExit.Text = "Wyjście";
            this.toolExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolExit.Click += new System.EventHandler(this.toolExit_Click);
            // 
            // toolConnect
            // 
            this.toolConnect.AutoSize = false;
            this.toolConnect.Image = global::nixie.Properties.Resources.gtk_yes;
            this.toolConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolConnect.Name = "toolConnect";
            this.toolConnect.Size = new System.Drawing.Size(80, 43);
            this.toolConnect.Text = "Połącz";
            this.toolConnect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolConnect.Click += new System.EventHandler(this.toolConnect_Click);
            // 
            // toolSet
            // 
            this.toolSet.AutoSize = false;
            this.toolSet.Image = global::nixie.Properties.Resources.document_open_recent;
            this.toolSet.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolSet.Name = "toolSet";
            this.toolSet.Size = new System.Drawing.Size(80, 43);
            this.toolSet.Text = "Nastaw";
            this.toolSet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolSet.Click += new System.EventHandler(this.toolSet_Click);
            // 
            // toolScroll
            // 
            this.toolScroll.AutoSize = false;
            this.toolScroll.Image = global::nixie.Properties.Resources.view_refresh;
            this.toolScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolScroll.Name = "toolScroll";
            this.toolScroll.Size = new System.Drawing.Size(80, 43);
            this.toolScroll.Text = "Odtruwanie";
            this.toolScroll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolScroll.Click += new System.EventHandler(this.toolScroll_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 90);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zegar nixie IN-12";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolConnect;
        private System.Windows.Forms.ToolStripButton toolSet;
        private System.Windows.Forms.ToolStripButton toolScroll;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolSettings;
        private System.Windows.Forms.ToolStripButton toolAbout;
        private System.Windows.Forms.ToolStripButton toolExit;
    }
}

