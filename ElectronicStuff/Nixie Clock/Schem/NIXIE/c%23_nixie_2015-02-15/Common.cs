﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nixie
{
    //sygnatury obslugiwanych prockow - do command-line'owego uruchamiania bootloadera
    public enum AvrSignatures
    {
        Atmega88 = 0x1e930a,
        Atmega88P = 0x1e930f,
        Atmega168 = 0x1e9406,
        Atmega168P = 0x1e940b
    }

    //kontynuacja klasy Uart - obsluga poszczegolnych komend
    public static partial class Uart
    {
        public static string Scroll()
        {
            return SendCommand(0);
        }

        public static string SetTime(int hour, int minute, int second=0)
        {
            byte h_ = (byte)(((hour / 10) << 4) | (hour % 10));
            byte m_ = (byte)(((minute / 10) << 4) | (minute % 10));
            byte s_ = (byte)(((second / 10) << 4) | (second % 10));
            return SendCommand(1, new byte[] { h_, m_, s_ });
        }

        public static string SetDate(int day, int month, int year) //2000 <= year <= 2099
        {
            year -= 2000;
            byte d_ = (byte)(((day / 10) << 4) | (day % 10));
            byte m_ = (byte)(((month / 10) << 4) | (month % 10));
            byte y_ = (byte)(((year / 10) << 4) | (year % 10));
            return SendCommand(2, new byte[] { d_, m_, y_ });
        }

        public static string SetAlarm(int hour, int minute)
        {
            byte h_ = (byte)(((hour / 10) << 4) | (hour % 10));
            byte m_ = (byte)(((minute / 10) << 4) | (minute % 10));
            return SendCommand(3, new byte[] { h_, m_ });
        }

        public static string SetDigits(int digit1, int digit2, int digit3, int digit4)
        {
            byte arg1 = (byte)((digit1 << 4) + digit2);
            byte arg2 = (byte)((digit3 << 4) + digit4);
            return SendCommand(5, new byte[] { arg1, arg2 });
        }
    }

    //metody wspolne dla innych klas
    public static class Common
    {
        public static void ShowError(string text, string caption = "Błąd")
        {
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    //ulatwienie dostepu do zmiennych konfiguracyjnych
    public static class Config
    {
        public static byte FrameStartByte
        {
            get { return Properties.Settings.Default.FrameStartByte; }
        }

        public static byte FrameStopByte
        {
            get { return Properties.Settings.Default.FrameStopByte; }
        }

        public static string PortName
        {
            get { return Properties.Settings.Default.PortName; }
            set { Properties.Settings.Default.PortName = value; }
        }

        public static int PortBaud
        {
            get { return Properties.Settings.Default.PortBaud; }
            set { Properties.Settings.Default.PortBaud = value; }
        }

        public static AvrSignatures AvrSignature
        {
            get { return Properties.Settings.Default.AvrSignature; }
            set { Properties.Settings.Default.AvrSignature = value; }
        }

        public static string BootloaderPath
        {
            get { return Properties.Settings.Default.BootladerPath; }
            set { Properties.Settings.Default.BootladerPath = value; }
        }

        public static string AboutWebsite
        {
            get { return Properties.Settings.Default.AboutWebsite; }
        }

        public static string ResetString
        {
            get { return Properties.Settings.Default.ResetString; }
        }

        //zapisywanie aktualnych wartosci w pliku konfiguracyjnym
        public static void Save()
        {
            Properties.Settings.Default.Save();
        }
    }
}
