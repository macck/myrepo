################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../BinaryFileDataSource.cpp \
../FileDataSource.cpp \
../main.cpp 

OBJS += \
./BinaryFileDataSource.o \
./FileDataSource.o \
./main.o 

CPP_DEPS += \
./BinaryFileDataSource.d \
./FileDataSource.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
BinaryFileDataSource.o: ../BinaryFileDataSource.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"BinaryFileDataSource.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


