﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectCatalog
{
    public partial class WindowGuitarEdit : Form
    {
        public Guitar guitarToEdit;

        public WindowGuitarEdit()
        {
            InitializeComponent();
            pictureBoxGuitarPic.SizeMode = PictureBoxSizeMode.StretchImage;
            result = DialogResult.None;
        }

        public WindowGuitarEdit(Guitar obj)
        {
            InitializeComponent();
            pictureBoxGuitarPic.SizeMode = PictureBoxSizeMode.StretchImage;
            result = DialogResult.None;
            guitarToEdit = obj;

        }

        string fileName;
        public DialogResult result;




        /* ------------ FUNC BLOCK -----------*/

                    /// <summary>
                    /// Returns path to opened file, if failed returns empty string ("")
                    /// </summary>
        private string OpenFile()
        {

            OpenFileDialog openFileGuitars = new OpenFileDialog();
            DialogResult result = openFileGuitars.ShowDialog();
            string fileName = "";
            if (result == DialogResult.OK)
            {
                if (openFileGuitars.FileName.EndsWith(".png") || openFileGuitars.FileName.EndsWith(".jpg") || openFileGuitars.FileName.EndsWith(".jpeg"))
                {
                    fileName = openFileGuitars.FileName;
                }
                 
                pictureBoxGuitarPic.Image = Image.FromFile(fileName);
            }

            return fileName;
        }

        /// <summary>
        /// Shows info about obj. guitar in textboxes on editForm
        /// </summary>
        /// <param name="obj"></param>
        public void LoadInfo()
        {
            // load data
            textBoxDescription.Text = guitarToEdit.GetDescription();
            textBoxManufacturer.Text = guitarToEdit.Producent;
            textBoxModel.Text = guitarToEdit.Model;
            textBoxColor.Text = guitarToEdit.Kolor;
            listBoxStringCount.Text = guitarToEdit.Struny.ToString();
            listBoxType.Text = guitarToEdit.Typ;
            fileName = guitarToEdit.GetPathToImage();

            if (fileName != null)
            {  
                pictureBoxGuitarPic.Image = Image.FromFile(fileName);
            }

        }

        /// <summary>
        /// To save present infomation to object
        /// </summary>
        private void SaveEdit()
        {
            guitarToEdit.Producent = textBoxManufacturer.Text;
            guitarToEdit.Model = textBoxModel.Text;
            guitarToEdit.Kolor = textBoxColor.Text;
            guitarToEdit.Struny = int.Parse(listBoxStringCount.Text);
            guitarToEdit.Typ = listBoxType.Text;
            guitarToEdit.SetPathToImage(fileName);
            guitarToEdit.SetDescription(textBoxDescription.Text);
            MessageBox.Show("Zapisano", "Informacja"); 
        }
    
        /*------------ ENDOF FUNC BLOCK -------------- */


        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveEdit();
        }

        private void buttonEditClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonChangeImage_Click(object sender, EventArgs e)
        {
            fileName = OpenFile();
        }
    }
}
