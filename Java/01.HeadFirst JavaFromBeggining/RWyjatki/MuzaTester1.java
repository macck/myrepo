import javax.sound.midi.*;
import java.util.*;

class ZlyWyjatek extends Exception{
	
}

class JeszczeGorszyWyjatek extends ZlyWyjatek{
	
}

class JeszczeTragiczniejszyWyjatek extends ZlyWyjatek{
	
}
public class MuzaTester1{
	
	public void graj()
	{
		try {
			Sequencer sekwenser = MidiSystem.getSequencer();
			System.out.println("Mamy sekwenser");
		
		} catch (MidiUnavailableException ex){
			System.out.println("PROBLEM!");
		}
		
		
	} // end graj
	
	
	
	public void niedobraMetoda() throws ZlyWyjatek{ // i jak tutaj dam se klase bazowa to jest pieknie i w spaniale bo moge rzucac potomnymi a bloki catch beda lapac swoje odpowiednie
		throw new JeszczeGorszyWyjatek();
	}
	public void wywolamZlaMetode()
	{
		try{
			this.niedobraMetoda();
			
		}
		catch (JeszczeGorszyWyjatek ex)
		{
			System.out.println("Our Class Exception:  Wszystko bum bo gorszy wyjatek");
		}
		catch (JeszczeTragiczniejszyWyjatek ex)
		{
			System.out.println("Our Class Exception:  Wszystko bum bo TRAGICZNY wyjatek");
		}
		catch (ZlyWyjatek ex)
		{
			System.out.println("Our Class Exception:  Wszystko bum bo ZLY");
		}
		finally {
			System.out.println("bum czy nie, we need to carry on");
		}
	}
	
	public static void main(String[] args){
		MuzaTester1 mt = new MuzaTester1();
		mt.graj();
		mt.wywolamZlaMetode();
	}
	
	
}