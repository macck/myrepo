﻿namespace Z1Calc
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.richTextBoxCalcOutput = new System.Windows.Forms.RichTextBox();
            this.buttonSwitchKappa = new System.Windows.Forms.Button();
            this.buttonDivide = new System.Windows.Forms.Button();
            this.buttonMultiply = new System.Windows.Forms.Button();
            this.buttonSquared = new System.Windows.Forms.Button();
            this.buttonSqrt = new System.Windows.Forms.Button();
            this.buttonNatBase = new System.Windows.Forms.Button();
            this.buttonNumPi = new System.Windows.Forms.Button();
            this.buttonEquals = new System.Windows.Forms.Button();
            this.buttonSubtract = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonInput0 = new System.Windows.Forms.Button();
            this.buttonInput3 = new System.Windows.Forms.Button();
            this.buttonInput2 = new System.Windows.Forms.Button();
            this.buttonInput1 = new System.Windows.Forms.Button();
            this.buttonInput6 = new System.Windows.Forms.Button();
            this.buttonInput5 = new System.Windows.Forms.Button();
            this.buttonInput4 = new System.Windows.Forms.Button();
            this.buttonInput9 = new System.Windows.Forms.Button();
            this.buttonInput8 = new System.Windows.Forms.Button();
            this.buttonInput7 = new System.Windows.Forms.Button();
            this.axKappaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.buttonUltimate = new System.Windows.Forms.Button();
            this.webBrowserEmbeded = new System.Windows.Forms.WebBrowser();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonComa = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.axKappaPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBoxCalcOutput
            // 
            this.richTextBoxCalcOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBoxCalcOutput.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.richTextBoxCalcOutput.Location = new System.Drawing.Point(31, 12);
            this.richTextBoxCalcOutput.Name = "richTextBoxCalcOutput";
            this.richTextBoxCalcOutput.Size = new System.Drawing.Size(410, 82);
            this.richTextBoxCalcOutput.TabIndex = 125;
            this.richTextBoxCalcOutput.Text = "";
            this.richTextBoxCalcOutput.TextChanged += new System.EventHandler(this.richTextBoxCalcOutput_TextChanged);
            // 
            // buttonSwitchKappa
            // 
            this.buttonSwitchKappa.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSwitchKappa.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSwitchKappa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonSwitchKappa.Location = new System.Drawing.Point(137, 100);
            this.buttonSwitchKappa.Name = "buttonSwitchKappa";
            this.buttonSwitchKappa.Size = new System.Drawing.Size(100, 47);
            this.buttonSwitchKappa.TabIndex = 124;
            this.buttonSwitchKappa.Text = "KAPPA";
            this.buttonSwitchKappa.UseVisualStyleBackColor = false;
            this.buttonSwitchKappa.Click += new System.EventHandler(this.buttonSwitchKappa_Click);
            // 
            // buttonDivide
            // 
            this.buttonDivide.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonDivide.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDivide.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonDivide.Location = new System.Drawing.Point(349, 381);
            this.buttonDivide.Name = "buttonDivide";
            this.buttonDivide.Size = new System.Drawing.Size(100, 70);
            this.buttonDivide.TabIndex = 123;
            this.buttonDivide.Text = "/";
            this.buttonDivide.UseVisualStyleBackColor = false;
            this.buttonDivide.Click += new System.EventHandler(this.buttonDivide_Click);
            // 
            // buttonMultiply
            // 
            this.buttonMultiply.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonMultiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonMultiply.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonMultiply.Location = new System.Drawing.Point(349, 305);
            this.buttonMultiply.Name = "buttonMultiply";
            this.buttonMultiply.Size = new System.Drawing.Size(100, 70);
            this.buttonMultiply.TabIndex = 122;
            this.buttonMultiply.Text = "*";
            this.buttonMultiply.UseVisualStyleBackColor = false;
            this.buttonMultiply.Click += new System.EventHandler(this.buttonMultiply_Click);
            // 
            // buttonSquared
            // 
            this.buttonSquared.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSquared.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSquared.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonSquared.Location = new System.Drawing.Point(349, 229);
            this.buttonSquared.Name = "buttonSquared";
            this.buttonSquared.Size = new System.Drawing.Size(100, 70);
            this.buttonSquared.TabIndex = 121;
            this.buttonSquared.Text = "x^y";
            this.buttonSquared.UseVisualStyleBackColor = false;
            this.buttonSquared.Click += new System.EventHandler(this.buttonSquared_Click);
            // 
            // buttonSqrt
            // 
            this.buttonSqrt.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSqrt.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonSqrt.Location = new System.Drawing.Point(349, 151);
            this.buttonSqrt.Name = "buttonSqrt";
            this.buttonSqrt.Size = new System.Drawing.Size(100, 70);
            this.buttonSqrt.TabIndex = 120;
            this.buttonSqrt.Text = "Sqrt";
            this.buttonSqrt.UseVisualStyleBackColor = false;
            this.buttonSqrt.Click += new System.EventHandler(this.buttonSqrt_Click);
            // 
            // buttonNatBase
            // 
            this.buttonNatBase.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonNatBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNatBase.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonNatBase.Location = new System.Drawing.Point(31, 457);
            this.buttonNatBase.Name = "buttonNatBase";
            this.buttonNatBase.Size = new System.Drawing.Size(100, 70);
            this.buttonNatBase.TabIndex = 119;
            this.buttonNatBase.Text = "e";
            this.buttonNatBase.UseVisualStyleBackColor = false;
            this.buttonNatBase.Click += new System.EventHandler(this.buttonNatBase_Click);
            // 
            // buttonNumPi
            // 
            this.buttonNumPi.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonNumPi.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNumPi.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonNumPi.Location = new System.Drawing.Point(137, 457);
            this.buttonNumPi.Name = "buttonNumPi";
            this.buttonNumPi.Size = new System.Drawing.Size(100, 70);
            this.buttonNumPi.TabIndex = 118;
            this.buttonNumPi.Text = "π";
            this.buttonNumPi.UseVisualStyleBackColor = false;
            this.buttonNumPi.Click += new System.EventHandler(this.buttonNumPi_Click);
            // 
            // buttonEquals
            // 
            this.buttonEquals.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonEquals.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEquals.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonEquals.Location = new System.Drawing.Point(243, 457);
            this.buttonEquals.Name = "buttonEquals";
            this.buttonEquals.Size = new System.Drawing.Size(100, 70);
            this.buttonEquals.TabIndex = 117;
            this.buttonEquals.Text = "=";
            this.buttonEquals.UseVisualStyleBackColor = false;
            this.buttonEquals.Click += new System.EventHandler(this.buttonEquals_Click);
            // 
            // buttonSubtract
            // 
            this.buttonSubtract.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSubtract.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSubtract.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonSubtract.Location = new System.Drawing.Point(243, 381);
            this.buttonSubtract.Name = "buttonSubtract";
            this.buttonSubtract.Size = new System.Drawing.Size(100, 70);
            this.buttonSubtract.TabIndex = 116;
            this.buttonSubtract.Text = "-";
            this.buttonSubtract.UseVisualStyleBackColor = false;
            this.buttonSubtract.Click += new System.EventHandler(this.buttonSubtract_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAdd.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonAdd.Location = new System.Drawing.Point(31, 381);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(100, 70);
            this.buttonAdd.TabIndex = 115;
            this.buttonAdd.Text = "+";
            this.buttonAdd.UseVisualStyleBackColor = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonInput0
            // 
            this.buttonInput0.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput0.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput0.Location = new System.Drawing.Point(137, 381);
            this.buttonInput0.Name = "buttonInput0";
            this.buttonInput0.Size = new System.Drawing.Size(100, 70);
            this.buttonInput0.TabIndex = 114;
            this.buttonInput0.Text = "0";
            this.buttonInput0.UseVisualStyleBackColor = false;
            this.buttonInput0.Click += new System.EventHandler(this.buttonInput0_Click);
            // 
            // buttonInput3
            // 
            this.buttonInput3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput3.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput3.Location = new System.Drawing.Point(243, 305);
            this.buttonInput3.Name = "buttonInput3";
            this.buttonInput3.Size = new System.Drawing.Size(100, 70);
            this.buttonInput3.TabIndex = 113;
            this.buttonInput3.Text = "3";
            this.buttonInput3.UseVisualStyleBackColor = false;
            this.buttonInput3.Click += new System.EventHandler(this.buttonInput3_Click);
            // 
            // buttonInput2
            // 
            this.buttonInput2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput2.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput2.Location = new System.Drawing.Point(137, 305);
            this.buttonInput2.Name = "buttonInput2";
            this.buttonInput2.Size = new System.Drawing.Size(100, 70);
            this.buttonInput2.TabIndex = 112;
            this.buttonInput2.Text = "2";
            this.buttonInput2.UseVisualStyleBackColor = false;
            this.buttonInput2.Click += new System.EventHandler(this.buttonInput2_Click);
            // 
            // buttonInput1
            // 
            this.buttonInput1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput1.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput1.Location = new System.Drawing.Point(31, 305);
            this.buttonInput1.Name = "buttonInput1";
            this.buttonInput1.Size = new System.Drawing.Size(100, 70);
            this.buttonInput1.TabIndex = 111;
            this.buttonInput1.Text = "1";
            this.buttonInput1.UseVisualStyleBackColor = false;
            this.buttonInput1.Click += new System.EventHandler(this.buttonInput1_Click);
            // 
            // buttonInput6
            // 
            this.buttonInput6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput6.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput6.Location = new System.Drawing.Point(243, 229);
            this.buttonInput6.Name = "buttonInput6";
            this.buttonInput6.Size = new System.Drawing.Size(100, 70);
            this.buttonInput6.TabIndex = 110;
            this.buttonInput6.Text = "6";
            this.buttonInput6.UseVisualStyleBackColor = false;
            this.buttonInput6.Click += new System.EventHandler(this.buttonInput6_Click);
            // 
            // buttonInput5
            // 
            this.buttonInput5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput5.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput5.Location = new System.Drawing.Point(137, 229);
            this.buttonInput5.Name = "buttonInput5";
            this.buttonInput5.Size = new System.Drawing.Size(100, 70);
            this.buttonInput5.TabIndex = 109;
            this.buttonInput5.Text = "5";
            this.buttonInput5.UseVisualStyleBackColor = false;
            this.buttonInput5.Click += new System.EventHandler(this.buttonInput5_Click);
            // 
            // buttonInput4
            // 
            this.buttonInput4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput4.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput4.Location = new System.Drawing.Point(31, 229);
            this.buttonInput4.Name = "buttonInput4";
            this.buttonInput4.Size = new System.Drawing.Size(100, 70);
            this.buttonInput4.TabIndex = 108;
            this.buttonInput4.Text = "4";
            this.buttonInput4.UseVisualStyleBackColor = false;
            this.buttonInput4.Click += new System.EventHandler(this.buttonInput4_Click);
            // 
            // buttonInput9
            // 
            this.buttonInput9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput9.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput9.Location = new System.Drawing.Point(243, 153);
            this.buttonInput9.Name = "buttonInput9";
            this.buttonInput9.Size = new System.Drawing.Size(100, 70);
            this.buttonInput9.TabIndex = 107;
            this.buttonInput9.Text = "9";
            this.buttonInput9.UseVisualStyleBackColor = false;
            this.buttonInput9.Click += new System.EventHandler(this.buttonInput9_Click);
            // 
            // buttonInput8
            // 
            this.buttonInput8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput8.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput8.Location = new System.Drawing.Point(137, 153);
            this.buttonInput8.Name = "buttonInput8";
            this.buttonInput8.Size = new System.Drawing.Size(100, 70);
            this.buttonInput8.TabIndex = 106;
            this.buttonInput8.Text = "8";
            this.buttonInput8.UseVisualStyleBackColor = false;
            this.buttonInput8.Click += new System.EventHandler(this.buttonInput8_Click);
            // 
            // buttonInput7
            // 
            this.buttonInput7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonInput7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonInput7.ForeColor = System.Drawing.Color.Navy;
            this.buttonInput7.Location = new System.Drawing.Point(31, 153);
            this.buttonInput7.Name = "buttonInput7";
            this.buttonInput7.Size = new System.Drawing.Size(100, 70);
            this.buttonInput7.TabIndex = 105;
            this.buttonInput7.Text = "7";
            this.buttonInput7.UseVisualStyleBackColor = false;
            this.buttonInput7.Click += new System.EventHandler(this.buttonInput7_Click);
            // 
            // axKappaPlayer
            // 
            this.axKappaPlayer.Enabled = true;
            this.axKappaPlayer.Location = new System.Drawing.Point(31, 533);
            this.axKappaPlayer.Name = "axKappaPlayer";
            this.axKappaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axKappaPlayer.OcxState")));
            this.axKappaPlayer.Size = new System.Drawing.Size(418, 317);
            this.axKappaPlayer.TabIndex = 126;
            this.axKappaPlayer.Enter += new System.EventHandler(this.axKappaPlayer_Enter);
            // 
            // buttonUltimate
            // 
            this.buttonUltimate.BackColor = System.Drawing.Color.Beige;
            this.buttonUltimate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUltimate.ForeColor = System.Drawing.Color.Goldenrod;
            this.buttonUltimate.Location = new System.Drawing.Point(243, 100);
            this.buttonUltimate.Name = "buttonUltimate";
            this.buttonUltimate.Size = new System.Drawing.Size(206, 47);
            this.buttonUltimate.TabIndex = 127;
            this.buttonUltimate.Text = "Ultimate Answers";
            this.buttonUltimate.UseVisualStyleBackColor = false;
            this.buttonUltimate.Click += new System.EventHandler(this.buttonUltimate_Click);
            // 
            // webBrowserEmbeded
            // 
            this.webBrowserEmbeded.Location = new System.Drawing.Point(484, 12);
            this.webBrowserEmbeded.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserEmbeded.Name = "webBrowserEmbeded";
            this.webBrowserEmbeded.Size = new System.Drawing.Size(688, 515);
            this.webBrowserEmbeded.TabIndex = 128;
            this.webBrowserEmbeded.Url = new System.Uri("http://www.wolframalpha.com/", System.UriKind.Absolute);
            this.webBrowserEmbeded.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonClear.ForeColor = System.Drawing.Color.DarkBlue;
            this.buttonClear.Location = new System.Drawing.Point(31, 100);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(100, 47);
            this.buttonClear.TabIndex = 129;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = false;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonComa
            // 
            this.buttonComa.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonComa.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonComa.ForeColor = System.Drawing.Color.DarkGreen;
            this.buttonComa.Location = new System.Drawing.Point(349, 457);
            this.buttonComa.Name = "buttonComa";
            this.buttonComa.Size = new System.Drawing.Size(100, 70);
            this.buttonComa.TabIndex = 130;
            this.buttonComa.Text = ",";
            this.buttonComa.UseVisualStyleBackColor = false;
            this.buttonComa.Click += new System.EventHandler(this.buttonComa_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 862);
            this.Controls.Add(this.buttonComa);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.webBrowserEmbeded);
            this.Controls.Add(this.buttonUltimate);
            this.Controls.Add(this.axKappaPlayer);
            this.Controls.Add(this.richTextBoxCalcOutput);
            this.Controls.Add(this.buttonSwitchKappa);
            this.Controls.Add(this.buttonDivide);
            this.Controls.Add(this.buttonMultiply);
            this.Controls.Add(this.buttonSquared);
            this.Controls.Add(this.buttonSqrt);
            this.Controls.Add(this.buttonNatBase);
            this.Controls.Add(this.buttonNumPi);
            this.Controls.Add(this.buttonEquals);
            this.Controls.Add(this.buttonSubtract);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonInput0);
            this.Controls.Add(this.buttonInput3);
            this.Controls.Add(this.buttonInput2);
            this.Controls.Add(this.buttonInput1);
            this.Controls.Add(this.buttonInput6);
            this.Controls.Add(this.buttonInput5);
            this.Controls.Add(this.buttonInput4);
            this.Controls.Add(this.buttonInput9);
            this.Controls.Add(this.buttonInput8);
            this.Controls.Add(this.buttonInput7);
            this.Name = "Form1";
            this.Text = "Calc V1.0";
            ((System.ComponentModel.ISupportInitialize)(this.axKappaPlayer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxCalcOutput;
        private System.Windows.Forms.Button buttonSwitchKappa;
        private System.Windows.Forms.Button buttonDivide;
        private System.Windows.Forms.Button buttonMultiply;
        private System.Windows.Forms.Button buttonSquared;
        private System.Windows.Forms.Button buttonSqrt;
        private System.Windows.Forms.Button buttonNatBase;
        private System.Windows.Forms.Button buttonNumPi;
        private System.Windows.Forms.Button buttonEquals;
        private System.Windows.Forms.Button buttonSubtract;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonInput0;
        private System.Windows.Forms.Button buttonInput3;
        private System.Windows.Forms.Button buttonInput2;
        private System.Windows.Forms.Button buttonInput1;
        private System.Windows.Forms.Button buttonInput6;
        private System.Windows.Forms.Button buttonInput5;
        private System.Windows.Forms.Button buttonInput4;
        private System.Windows.Forms.Button buttonInput9;
        private System.Windows.Forms.Button buttonInput8;
        private System.Windows.Forms.Button buttonInput7;
        private AxWMPLib.AxWindowsMediaPlayer axKappaPlayer;
        private System.Windows.Forms.Button buttonUltimate;
        private System.Windows.Forms.WebBrowser webBrowserEmbeded;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonComa;
    }
}

