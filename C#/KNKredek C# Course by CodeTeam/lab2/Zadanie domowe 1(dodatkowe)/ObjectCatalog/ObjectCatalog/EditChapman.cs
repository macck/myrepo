﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectCatalog
{
    public partial class EditChapman : Form
    {
        public EditChapman(ChapmanStick obj)
        {
            InitializeComponent();
            pictureBoxGuitarPic.SizeMode = PictureBoxSizeMode.StretchImage;
            result = DialogResult.None;
            chapmanToEdit = obj;
        }

        string fileName;
        public DialogResult result;
        public ChapmanStick chapmanToEdit;



        /* ------------ FUNC BLOCK -----------*/

        /// <summary>
        /// Returns path to opened file, if failed returns empty string ("")
        /// </summary>
        private string OpenFile()
        {

            OpenFileDialog openFileGuitars = new OpenFileDialog();
            DialogResult result = openFileGuitars.ShowDialog();
            string fileName = "";
            if (result == DialogResult.OK)
            {
                if (openFileGuitars.FileName.EndsWith(".png") || openFileGuitars.FileName.EndsWith(".jpg") || openFileGuitars.FileName.EndsWith(".jpeg"))
                {
                    fileName = openFileGuitars.FileName;
                }

                pictureBoxGuitarPic.Image = Image.FromFile(fileName);
            }

            return fileName;
        }

        /// <summary>
        /// Shows info about obj. guitar in textboxes on editForm
        /// </summary>
        /// <param name="obj"></param>
        public void LoadInfo()
        {
            // load data
            textBoxDescription.Text = chapmanToEdit.GetDescription();
            textBoxManufacturer.Text = chapmanToEdit.Producent;
            textBoxModel.Text = chapmanToEdit.Model;
            textBoxColor.Text = chapmanToEdit.Kolor;
            listBoxBassStrings.Text = chapmanToEdit.StrunyBasowe.ToString();
            listBoxMelodicStrings.Text = chapmanToEdit.StrunyMelodyczne.ToString();
            listBoxStringCount.Text = chapmanToEdit.Struny.ToString();
            listBoxType.Text = chapmanToEdit.Typ;
            fileName = chapmanToEdit.GetPathToImage();

            if (fileName != null)
            {
                pictureBoxGuitarPic.Image = Image.FromFile(fileName);
            }

        }

        /// <summary>
        /// To save present infomation to object
        /// </summary>
        private void SaveEdit()
        {
            chapmanToEdit.Producent = textBoxManufacturer.Text;
            chapmanToEdit.Model = textBoxModel.Text;
            chapmanToEdit.Kolor = textBoxColor.Text;
            chapmanToEdit.Struny = int.Parse(listBoxStringCount.Text);
            chapmanToEdit.StrunyBasowe = int.Parse(listBoxBassStrings.Text);
            chapmanToEdit.StrunyMelodyczne = int.Parse(listBoxMelodicStrings.Text);
            chapmanToEdit.SetPathToImage(fileName);
            chapmanToEdit.SetDescription(textBoxDescription.Text);
            MessageBox.Show("Zapisano", "Informacja");
        }

        /*------------ ENDOF FUNC BLOCK -------------- */


        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveEdit();
        }

        private void buttonEditClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonChangeImage_Click(object sender, EventArgs e)
        {
            fileName = OpenFile();
        }
    }
}