﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MusicClub
{
    class DataBaseTable
    {
        private SqlDataAdapter dataAdapter;
        private void FillDataGridView(SqlDataAdapter dataAdapter, DataGridView dataGridViewDatabase)
        {
            dataGridViewDatabase.DataSource = null;
            DataTable dataTable = new DataTable();
           dataAdapter.Fill(dataTable);
            dataGridViewDatabase.DataSource = dataTable;
        }

        public virtual SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            dataAdapter = new SqlDataAdapter(databaseQuery, sqlCon);
            FillDataGridView(dataAdapter, dataGridView);
            return dataAdapter;
        }

        public virtual void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            dataAdapter = new SqlDataAdapter(text, sqlCon);
            FillDataGridView(dataAdapter, dataGridViewDatabase);
        }
    }
}
