﻿namespace nixie
{
    partial class SetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabTime = new System.Windows.Forms.TabPage();
            this.btnSendTime = new System.Windows.Forms.Button();
            this.btnLocalTime = new System.Windows.Forms.Button();
            this.numSecond = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numMinute = new System.Windows.Forms.NumericUpDown();
            this.numHour = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabDate = new System.Windows.Forms.TabPage();
            this.btnSendDate = new System.Windows.Forms.Button();
            this.numYear = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numMonth = new System.Windows.Forms.NumericUpDown();
            this.numDay = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabAlarm = new System.Windows.Forms.TabPage();
            this.btnSendAlarm = new System.Windows.Forms.Button();
            this.numAMinute = new System.Windows.Forms.NumericUpDown();
            this.numAHour = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabDigits = new System.Windows.Forms.TabPage();
            this.comDigit4 = new System.Windows.Forms.ComboBox();
            this.comDigit3 = new System.Windows.Forms.ComboBox();
            this.comDigit2 = new System.Windows.Forms.ComboBox();
            this.comDigit1 = new System.Windows.Forms.ComboBox();
            this.btnSendDigits = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHour)).BeginInit();
            this.tabDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDay)).BeginInit();
            this.tabAlarm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAMinute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAHour)).BeginInit();
            this.tabDigits.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabTime);
            this.tabControl1.Controls.Add(this.tabDate);
            this.tabControl1.Controls.Add(this.tabAlarm);
            this.tabControl1.Controls.Add(this.tabDigits);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(194, 140);
            this.tabControl1.TabIndex = 0;
            // 
            // tabTime
            // 
            this.tabTime.Controls.Add(this.btnSendTime);
            this.tabTime.Controls.Add(this.btnLocalTime);
            this.tabTime.Controls.Add(this.numSecond);
            this.tabTime.Controls.Add(this.label3);
            this.tabTime.Controls.Add(this.numMinute);
            this.tabTime.Controls.Add(this.numHour);
            this.tabTime.Controls.Add(this.label2);
            this.tabTime.Controls.Add(this.label1);
            this.tabTime.Location = new System.Drawing.Point(4, 22);
            this.tabTime.Name = "tabTime";
            this.tabTime.Padding = new System.Windows.Forms.Padding(3);
            this.tabTime.Size = new System.Drawing.Size(186, 114);
            this.tabTime.TabIndex = 0;
            this.tabTime.Text = "Czas";
            this.tabTime.UseVisualStyleBackColor = true;
            // 
            // btnSendTime
            // 
            this.btnSendTime.Location = new System.Drawing.Point(8, 85);
            this.btnSendTime.Name = "btnSendTime";
            this.btnSendTime.Size = new System.Drawing.Size(50, 23);
            this.btnSendTime.TabIndex = 7;
            this.btnSendTime.Text = "Wyślij";
            this.btnSendTime.UseVisualStyleBackColor = true;
            this.btnSendTime.Click += new System.EventHandler(this.btnSendTime_Click);
            // 
            // btnLocalTime
            // 
            this.btnLocalTime.Location = new System.Drawing.Point(64, 85);
            this.btnLocalTime.Name = "btnLocalTime";
            this.btnLocalTime.Size = new System.Drawing.Size(115, 23);
            this.btnLocalTime.TabIndex = 6;
            this.btnLocalTime.Text = "Wyślij czas lokalny";
            this.btnLocalTime.UseVisualStyleBackColor = true;
            this.btnLocalTime.Click += new System.EventHandler(this.btnLocalTime_Click);
            // 
            // numSecond
            // 
            this.numSecond.Location = new System.Drawing.Point(112, 59);
            this.numSecond.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numSecond.Name = "numSecond";
            this.numSecond.Size = new System.Drawing.Size(40, 20);
            this.numSecond.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sekunda:";
            // 
            // numMinute
            // 
            this.numMinute.Location = new System.Drawing.Point(112, 33);
            this.numMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numMinute.Name = "numMinute";
            this.numMinute.Size = new System.Drawing.Size(40, 20);
            this.numMinute.TabIndex = 3;
            // 
            // numHour
            // 
            this.numHour.Location = new System.Drawing.Point(112, 7);
            this.numHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.numHour.Name = "numHour";
            this.numHour.Size = new System.Drawing.Size(40, 20);
            this.numHour.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Minuta:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Godzina:";
            // 
            // tabDate
            // 
            this.tabDate.Controls.Add(this.btnSendDate);
            this.tabDate.Controls.Add(this.numYear);
            this.tabDate.Controls.Add(this.label4);
            this.tabDate.Controls.Add(this.numMonth);
            this.tabDate.Controls.Add(this.numDay);
            this.tabDate.Controls.Add(this.label5);
            this.tabDate.Controls.Add(this.label6);
            this.tabDate.Location = new System.Drawing.Point(4, 22);
            this.tabDate.Name = "tabDate";
            this.tabDate.Padding = new System.Windows.Forms.Padding(3);
            this.tabDate.Size = new System.Drawing.Size(186, 114);
            this.tabDate.TabIndex = 1;
            this.tabDate.Text = "Data";
            this.tabDate.UseVisualStyleBackColor = true;
            // 
            // btnSendDate
            // 
            this.btnSendDate.Location = new System.Drawing.Point(23, 85);
            this.btnSendDate.Name = "btnSendDate";
            this.btnSendDate.Size = new System.Drawing.Size(140, 23);
            this.btnSendDate.TabIndex = 15;
            this.btnSendDate.Text = "Wyślij";
            this.btnSendDate.UseVisualStyleBackColor = true;
            this.btnSendDate.Click += new System.EventHandler(this.btnSendDate_Click);
            // 
            // numYear
            // 
            this.numYear.Location = new System.Drawing.Point(102, 59);
            this.numYear.Maximum = new decimal(new int[] {
            2099,
            0,
            0,
            0});
            this.numYear.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numYear.Name = "numYear";
            this.numYear.Size = new System.Drawing.Size(50, 20);
            this.numYear.TabIndex = 13;
            this.numYear.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Rok:";
            // 
            // numMonth
            // 
            this.numMonth.Location = new System.Drawing.Point(112, 33);
            this.numMonth.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numMonth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMonth.Name = "numMonth";
            this.numMonth.Size = new System.Drawing.Size(40, 20);
            this.numMonth.TabIndex = 11;
            this.numMonth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numDay
            // 
            this.numDay.Location = new System.Drawing.Point(112, 7);
            this.numDay.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.numDay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDay.Name = "numDay";
            this.numDay.Size = new System.Drawing.Size(40, 20);
            this.numDay.TabIndex = 10;
            this.numDay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Miesiąc";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Dzień";
            // 
            // tabAlarm
            // 
            this.tabAlarm.Controls.Add(this.btnSendAlarm);
            this.tabAlarm.Controls.Add(this.numAMinute);
            this.tabAlarm.Controls.Add(this.numAHour);
            this.tabAlarm.Controls.Add(this.label8);
            this.tabAlarm.Controls.Add(this.label9);
            this.tabAlarm.Location = new System.Drawing.Point(4, 22);
            this.tabAlarm.Name = "tabAlarm";
            this.tabAlarm.Padding = new System.Windows.Forms.Padding(3);
            this.tabAlarm.Size = new System.Drawing.Size(186, 114);
            this.tabAlarm.TabIndex = 2;
            this.tabAlarm.Text = "Alarm";
            this.tabAlarm.UseVisualStyleBackColor = true;
            // 
            // btnSendAlarm
            // 
            this.btnSendAlarm.Location = new System.Drawing.Point(23, 85);
            this.btnSendAlarm.Name = "btnSendAlarm";
            this.btnSendAlarm.Size = new System.Drawing.Size(140, 23);
            this.btnSendAlarm.TabIndex = 15;
            this.btnSendAlarm.Text = "Wyślij";
            this.btnSendAlarm.UseVisualStyleBackColor = true;
            this.btnSendAlarm.Click += new System.EventHandler(this.btnSendAlarm_Click);
            // 
            // numAMinute
            // 
            this.numAMinute.Location = new System.Drawing.Point(112, 33);
            this.numAMinute.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numAMinute.Name = "numAMinute";
            this.numAMinute.Size = new System.Drawing.Size(40, 20);
            this.numAMinute.TabIndex = 11;
            // 
            // numAHour
            // 
            this.numAHour.Location = new System.Drawing.Point(112, 7);
            this.numAHour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.numAHour.Name = "numAHour";
            this.numAHour.Size = new System.Drawing.Size(40, 20);
            this.numAHour.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Minuta:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Godzina:";
            // 
            // tabDigits
            // 
            this.tabDigits.Controls.Add(this.label7);
            this.tabDigits.Controls.Add(this.comDigit4);
            this.tabDigits.Controls.Add(this.comDigit3);
            this.tabDigits.Controls.Add(this.comDigit2);
            this.tabDigits.Controls.Add(this.comDigit1);
            this.tabDigits.Controls.Add(this.btnSendDigits);
            this.tabDigits.Location = new System.Drawing.Point(4, 22);
            this.tabDigits.Name = "tabDigits";
            this.tabDigits.Padding = new System.Windows.Forms.Padding(3);
            this.tabDigits.Size = new System.Drawing.Size(186, 114);
            this.tabDigits.TabIndex = 3;
            this.tabDigits.Text = "Cyfry";
            this.tabDigits.UseVisualStyleBackColor = true;
            // 
            // comDigit4
            // 
            this.comDigit4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comDigit4.FormattingEnabled = true;
            this.comDigit4.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "-"});
            this.comDigit4.Location = new System.Drawing.Point(136, 31);
            this.comDigit4.Name = "comDigit4";
            this.comDigit4.Size = new System.Drawing.Size(34, 21);
            this.comDigit4.TabIndex = 20;
            // 
            // comDigit3
            // 
            this.comDigit3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comDigit3.FormattingEnabled = true;
            this.comDigit3.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "-"});
            this.comDigit3.Location = new System.Drawing.Point(96, 31);
            this.comDigit3.Name = "comDigit3";
            this.comDigit3.Size = new System.Drawing.Size(34, 21);
            this.comDigit3.TabIndex = 19;
            // 
            // comDigit2
            // 
            this.comDigit2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comDigit2.FormattingEnabled = true;
            this.comDigit2.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "-"});
            this.comDigit2.Location = new System.Drawing.Point(56, 31);
            this.comDigit2.Name = "comDigit2";
            this.comDigit2.Size = new System.Drawing.Size(34, 21);
            this.comDigit2.TabIndex = 18;
            // 
            // comDigit1
            // 
            this.comDigit1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comDigit1.FormattingEnabled = true;
            this.comDigit1.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "-"});
            this.comDigit1.Location = new System.Drawing.Point(16, 31);
            this.comDigit1.Name = "comDigit1";
            this.comDigit1.Size = new System.Drawing.Size(34, 21);
            this.comDigit1.TabIndex = 17;
            // 
            // btnSendDigits
            // 
            this.btnSendDigits.Location = new System.Drawing.Point(23, 85);
            this.btnSendDigits.Name = "btnSendDigits";
            this.btnSendDigits.Size = new System.Drawing.Size(140, 23);
            this.btnSendDigits.TabIndex = 16;
            this.btnSendDigits.Text = "Wyślij";
            this.btnSendDigits.UseVisualStyleBackColor = true;
            this.btnSendDigits.Click += new System.EventHandler(this.btnSendDigits_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = global::nixie.Properties.Resources.gtk_apply;
            this.btnClose.Location = new System.Drawing.Point(28, 142);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(139, 27);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Zamknij";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Wartości tymczasowe:";
            // 
            // SetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(194, 172);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Nastaw";
            this.Load += new System.EventHandler(this.SetForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabTime.ResumeLayout(false);
            this.tabTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHour)).EndInit();
            this.tabDate.ResumeLayout(false);
            this.tabDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDay)).EndInit();
            this.tabAlarm.ResumeLayout(false);
            this.tabAlarm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAMinute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAHour)).EndInit();
            this.tabDigits.ResumeLayout(false);
            this.tabDigits.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabTime;
        private System.Windows.Forms.TabPage tabAlarm;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.NumericUpDown numSecond;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numMinute;
        private System.Windows.Forms.NumericUpDown numHour;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSendTime;
        private System.Windows.Forms.Button btnLocalTime;
        private System.Windows.Forms.TabPage tabDate;
        private System.Windows.Forms.Button btnSendDate;
        private System.Windows.Forms.NumericUpDown numYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numMonth;
        private System.Windows.Forms.NumericUpDown numDay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSendAlarm;
        private System.Windows.Forms.NumericUpDown numAMinute;
        private System.Windows.Forms.NumericUpDown numAHour;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabDigits;
        private System.Windows.Forms.ComboBox comDigit1;
        private System.Windows.Forms.Button btnSendDigits;
        private System.Windows.Forms.ComboBox comDigit4;
        private System.Windows.Forms.ComboBox comDigit3;
        private System.Windows.Forms.ComboBox comDigit2;
        private System.Windows.Forms.Label label7;

    }
}