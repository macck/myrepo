﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MusicClub
{
    class Inventory : DataBaseTable
    {

        public override SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            databaseQuery = "SELECT Inventory.ID, Equipment.Name AS 'Nazwa', Inventory.Amount AS 'Ilość', Equipment.Description AS 'OPIS' , Inventory.Value AS 'Wartość'  FROM Inventory Join Equipment on EquipmentID=Equipment.ID";
            return base.ShowRecords(sqlCon, dataGridView, databaseQuery);

        }

        public override void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            string databaseQuery = "SELECT * FROM Inventory WHERE Value LIKE" + "'" + text + "%'";
            base.SearchBy(sqlCon, dataGridViewDatabase, databaseQuery);
        }
    }
}

