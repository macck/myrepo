﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using WebDatabase.Models;

namespace ApiDatabase.Models
{
    public class Order : EntityObjModel
    {
        [Required]
        public int UserId { get; set; }
        public bool IsAccepted { get; set; }
        public virtual ICollection<ProductOrder> OrderedProducts { get; set; }
    }
}