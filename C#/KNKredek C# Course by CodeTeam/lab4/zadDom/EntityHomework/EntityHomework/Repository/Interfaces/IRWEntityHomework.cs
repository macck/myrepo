﻿using EntityHomework.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityHomework.Repository.Interfaces
{
    public interface IRWEntityHomework <T> where T : Entity
    {
        // basics //
        IList<T> GetAll();
        IList<User> GetUsers();
        IList<int> GetIds();
        T GetById(int id);
        bool CreateUser(T entity, string nickName);
        int ValidateLogin(string nickName, string password);
        void EditSession(T entity);


        // extensions //
        //Resources//
        T GetResource(int userId, int resourceId);
        IList<T> GetAllResources(int userId);
        //Buildings//

        void AddBuilding(T building, int userId);
        T GetBuilding(int userId, int buildingId);
        IList<T> GetAllBuildings(int userId);

        //Heroes //
        void AddHero(T hero, int userId);
        T GetHero(int userId, int heroId);
        IList<T> GetAllHeroes(int userId);





    }
}
