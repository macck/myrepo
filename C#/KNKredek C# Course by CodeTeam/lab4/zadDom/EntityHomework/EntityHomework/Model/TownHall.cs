﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityHomework.Model
{
    public class TownHall : Entity
    {
        public int Level { get; set; }
        public int Defence { get; set; }
    }
}
