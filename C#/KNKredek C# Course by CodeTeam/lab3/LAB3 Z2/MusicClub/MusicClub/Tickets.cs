﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MusicClub
{
    class Tickets : DataBaseTable
    {

        public override SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            databaseQuery = "SELECT Tickets.ID, Tickets.Type AS 'Typ', Tickets.Price AS 'Cena', Concerts.Performer as 'Wykonawca', Concerts.DateOfEvent AS 'Data', Guests.Name AS 'Imię nabywcy', Guests.Surname AS 'Nazwisko nabywcy'  FROM Tickets "+
                "Join Concerts on EventID=Concerts.ID Join Guests on GuestID=Guests.ID";
            return base.ShowRecords(sqlCon, dataGridView, databaseQuery);
        }

        public override void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            string databaseQuery = "SELECT * FROM Tickets WHERE Type LIKE" + "'" + text + "%'";
            base.SearchBy(sqlCon, dataGridViewDatabase, databaseQuery);
        }
    }
}
