#ifndef GRACZ_INCLUDED
#define GRACZ_INCLUDED
#include <vector>
#include "karta.h"
#include <ctime>
#include <cstdlib>
#include <algorithm>


class Gracz{
std::string Imie;
std::vector<Karta> hand;

public:

    Gracz();
    Gracz(std::string const & Imie);
    void HandTakeCard(Karta const & k);
    void HandSort();
    void HandShowDown();
    Karta HandPassCard(unsigned int HandIndex);
    std::string PlayerName();
    virtual std::string PlayerSurname();
    int HandCardColorCheck(int CardIndex);
    unsigned int HandSize();
    void HandValues(int index);
    virtual int ReturnDecision()=0;
};

class HumanGracz : public Gracz
{

std::string Nazwisko;


public:

    HumanGracz();
    HumanGracz(std::string const & Imie,std::string const & Nazwisko);
    int ReturnDecision();
    std::string PlayerSurname();

};

class CpuGracz : public Gracz
{

public:

    CpuGracz();
    CpuGracz(std::string const & Imie);
    int ReturnDecision();


};



#endif // GRACZ_INCLUDED
