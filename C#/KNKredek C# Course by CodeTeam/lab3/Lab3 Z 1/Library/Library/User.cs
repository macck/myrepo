﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    class User
    {
        static SqlCommand sqlCommand = new SqlCommand();
        private static SqlDataAdapter sqlDataAdapter;
        
        private static void FillDataGridViewLib(SqlDataAdapter dataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null; // clear
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;

        }

        public static void ShowAllUsers(SqlConnection sqlConnection, DataGridView dataGridViewLibrary)
        {
            sqlDataAdapter = new SqlDataAdapter("SELECT Name, Surname FROM Users", sqlConnection);
            FillDataGridViewLib(sqlDataAdapter, dataGridViewLibrary);
        }


        /// <summary>
        /// Adds new user into table Users to SQL Database given in sqlConnection, displays output into dataGridView
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="dataGridView"></param>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        public static void AddUser(SqlConnection sqlConnection, DataGridView dataGridView, string name, string surname)
        {
            try
            {
                if(name != null && surname != null)
                {
                    sqlConnection.Open();
                    string cmd = "INSERT INTO Users (Name, Surname) VALUES ('" + name + "','" + surname + "')";
                    sqlCommand = new SqlCommand(cmd, sqlConnection); // prepare cmd to exec on given sqlCon
                    sqlCommand.ExecuteNonQuery(); // exec cmd
                    ShowAllUsers(sqlConnection, dataGridView);
                    MessageBox.Show("Dodano użytkownika!", "Informacja");

                    sqlConnection.Close();
                }

            }
            catch
            {
                MessageBox.Show("Nie udało się, ale zespół wykwalifikowanych małp już zajmuje się tym problemem!");
            }
        }

    }
}
