﻿namespace ObjectCatalog
{
    partial class WindowGuitarEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxStringCount = new System.Windows.Forms.ListBox();
            this.labelColor = new System.Windows.Forms.Label();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.listBoxType = new System.Windows.Forms.ListBox();
            this.labelGuitarType = new System.Windows.Forms.Label();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.textBoxManufacturer = new System.Windows.Forms.TextBox();
            this.labelStringCount = new System.Windows.Forms.Label();
            this.labelModel = new System.Windows.Forms.Label();
            this.labelManufacturer = new System.Windows.Forms.Label();
            this.labelAddFormTitle = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.pictureBoxGuitarPic = new System.Windows.Forms.PictureBox();
            this.buttonChangeImage = new System.Windows.Forms.Button();
            this.buttonEditClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxStringCount
            // 
            this.listBoxStringCount.FormattingEnabled = true;
            this.listBoxStringCount.Items.AddRange(new object[] {
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.listBoxStringCount.Location = new System.Drawing.Point(9, 206);
            this.listBoxStringCount.Name = "listBoxStringCount";
            this.listBoxStringCount.Size = new System.Drawing.Size(127, 17);
            this.listBoxStringCount.TabIndex = 31;
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelColor.Location = new System.Drawing.Point(12, 312);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(54, 24);
            this.labelColor.TabIndex = 30;
            this.labelColor.Text = "Kolor";
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(9, 339);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(129, 20);
            this.textBoxColor.TabIndex = 29;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescription.Location = new System.Drawing.Point(11, 360);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(57, 25);
            this.labelDescription.TabIndex = 28;
            this.labelDescription.Text = "Opis";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(9, 394);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(212, 130);
            this.textBoxDescription.TabIndex = 27;
            // 
            // listBoxType
            // 
            this.listBoxType.FormattingEnabled = true;
            this.listBoxType.Items.AddRange(new object[] {
            "elektryczna",
            "akustyczna",
            "klasyczna",
            "basowa"});
            this.listBoxType.Location = new System.Drawing.Point(9, 253);
            this.listBoxType.Name = "listBoxType";
            this.listBoxType.Size = new System.Drawing.Size(127, 56);
            this.listBoxType.TabIndex = 26;
            // 
            // labelGuitarType
            // 
            this.labelGuitarType.AutoSize = true;
            this.labelGuitarType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGuitarType.Location = new System.Drawing.Point(12, 226);
            this.labelGuitarType.Name = "labelGuitarType";
            this.labelGuitarType.Size = new System.Drawing.Size(91, 24);
            this.labelGuitarType.TabIndex = 25;
            this.labelGuitarType.Text = "Typ gitary";
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(9, 153);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(129, 20);
            this.textBoxModel.TabIndex = 24;
            // 
            // textBoxManufacturer
            // 
            this.textBoxManufacturer.Location = new System.Drawing.Point(9, 103);
            this.textBoxManufacturer.Name = "textBoxManufacturer";
            this.textBoxManufacturer.Size = new System.Drawing.Size(129, 20);
            this.textBoxManufacturer.TabIndex = 23;
            // 
            // labelStringCount
            // 
            this.labelStringCount.AutoSize = true;
            this.labelStringCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStringCount.Location = new System.Drawing.Point(12, 176);
            this.labelStringCount.Name = "labelStringCount";
            this.labelStringCount.Size = new System.Drawing.Size(94, 24);
            this.labelStringCount.TabIndex = 22;
            this.labelStringCount.Text = "Ilość strun";
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelModel.Location = new System.Drawing.Point(12, 126);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(63, 24);
            this.labelModel.TabIndex = 21;
            this.labelModel.Text = "Model";
            // 
            // labelManufacturer
            // 
            this.labelManufacturer.AutoSize = true;
            this.labelManufacturer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelManufacturer.Location = new System.Drawing.Point(12, 72);
            this.labelManufacturer.Name = "labelManufacturer";
            this.labelManufacturer.Size = new System.Drawing.Size(97, 24);
            this.labelManufacturer.TabIndex = 20;
            this.labelManufacturer.Text = "Producent";
            // 
            // labelAddFormTitle
            // 
            this.labelAddFormTitle.AutoSize = true;
            this.labelAddFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAddFormTitle.Location = new System.Drawing.Point(12, 9);
            this.labelAddFormTitle.Name = "labelAddFormTitle";
            this.labelAddFormTitle.Size = new System.Drawing.Size(242, 29);
            this.labelAddFormTitle.TabIndex = 32;
            this.labelAddFormTitle.Text = "Edytuj wpis w bazie";
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(287, 428);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(150, 50);
            this.buttonSave.TabIndex = 36;
            this.buttonSave.Text = "Zapisz";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // pictureBoxGuitarPic
            // 
            this.pictureBoxGuitarPic.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBoxGuitarPic.Location = new System.Drawing.Point(274, 9);
            this.pictureBoxGuitarPic.Name = "pictureBoxGuitarPic";
            this.pictureBoxGuitarPic.Size = new System.Drawing.Size(163, 350);
            this.pictureBoxGuitarPic.TabIndex = 35;
            this.pictureBoxGuitarPic.TabStop = false;
            // 
            // buttonChangeImage
            // 
            this.buttonChangeImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChangeImage.Location = new System.Drawing.Point(274, 360);
            this.buttonChangeImage.Name = "buttonChangeImage";
            this.buttonChangeImage.Size = new System.Drawing.Size(163, 50);
            this.buttonChangeImage.TabIndex = 34;
            this.buttonChangeImage.Text = "Zmień zdjęcie";
            this.buttonChangeImage.UseVisualStyleBackColor = true;
            this.buttonChangeImage.Click += new System.EventHandler(this.buttonChangeImage_Click);
            // 
            // buttonEditClose
            // 
            this.buttonEditClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEditClose.Location = new System.Drawing.Point(287, 497);
            this.buttonEditClose.Name = "buttonEditClose";
            this.buttonEditClose.Size = new System.Drawing.Size(150, 50);
            this.buttonEditClose.TabIndex = 33;
            this.buttonEditClose.Text = "Zamknij";
            this.buttonEditClose.UseVisualStyleBackColor = true;
            this.buttonEditClose.Click += new System.EventHandler(this.buttonEditClose_Click);
            // 
            // windowGuitarEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(449, 562);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.pictureBoxGuitarPic);
            this.Controls.Add(this.buttonChangeImage);
            this.Controls.Add(this.buttonEditClose);
            this.Controls.Add(this.labelAddFormTitle);
            this.Controls.Add(this.listBoxStringCount);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.textBoxColor);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.listBoxType);
            this.Controls.Add(this.labelGuitarType);
            this.Controls.Add(this.textBoxModel);
            this.Controls.Add(this.textBoxManufacturer);
            this.Controls.Add(this.labelStringCount);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.labelManufacturer);
            this.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Name = "windowGuitarEdit";
            this.Text = "Edit";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxStringCount;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.ListBox listBoxType;
        private System.Windows.Forms.Label labelGuitarType;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.TextBox textBoxManufacturer;
        private System.Windows.Forms.Label labelStringCount;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Label labelManufacturer;
        private System.Windows.Forms.Label labelAddFormTitle;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.PictureBox pictureBoxGuitarPic;
        private System.Windows.Forms.Button buttonChangeImage;
        private System.Windows.Forms.Button buttonEditClose;
    }
}