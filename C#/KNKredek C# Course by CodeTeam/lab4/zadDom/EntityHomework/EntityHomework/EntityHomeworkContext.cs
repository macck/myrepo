﻿using EntityHomework.Model;
using System.Data.Entity;

namespace EntityHomework
{
    public class EntityHomeworkContext : DbContext
    {
        public virtual DbSet<TownHall> Hall { get; set; }
        public virtual DbSet<GoldMine> Mine { get; set; }
        public virtual DbSet<Hero> Heroes { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public EntityHomeworkContext() : base("name=GameDatabase")
        {
            Database.SetInitializer<EntityHomeworkContext>(new DropCreateDatabaseIfModelChanges<EntityHomeworkContext>());
        }
    }
}
