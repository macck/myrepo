namespace WebDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test10 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductOrders", "Order_Id", "dbo.Orders");
            DropIndex("dbo.ProductOrders", new[] { "Order_Id" });
            AddColumn("dbo.ProductOrders", "Order_Id1", c => c.Int());
            AlterColumn("dbo.ProductOrders", "Order_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductOrders", "Order_Id1");
            AddForeignKey("dbo.ProductOrders", "Order_Id1", "dbo.Orders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductOrders", "Order_Id1", "dbo.Orders");
            DropIndex("dbo.ProductOrders", new[] { "Order_Id1" });
            AlterColumn("dbo.ProductOrders", "Order_Id", c => c.Int());
            DropColumn("dbo.ProductOrders", "Order_Id1");
            CreateIndex("dbo.ProductOrders", "Order_Id");
            AddForeignKey("dbo.ProductOrders", "Order_Id", "dbo.Orders", "Id");
        }
    }
}
