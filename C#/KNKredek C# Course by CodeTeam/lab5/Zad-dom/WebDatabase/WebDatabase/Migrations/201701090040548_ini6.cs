namespace WebDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ini6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "DeliveryAddres_Id", "dbo.Addresses");
            DropIndex("dbo.Users", new[] { "DeliveryAddres_Id" });
            DropColumn("dbo.Users", "DeliveryAddres_Id");
            DropTable("dbo.Addresses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Country = c.String(nullable: false),
                        Street = c.String(nullable: false),
                        City = c.String(nullable: false),
                        Postal = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Users", "DeliveryAddres_Id", c => c.Int());
            CreateIndex("dbo.Users", "DeliveryAddres_Id");
            AddForeignKey("dbo.Users", "DeliveryAddres_Id", "dbo.Addresses", "Id");
        }
    }
}
