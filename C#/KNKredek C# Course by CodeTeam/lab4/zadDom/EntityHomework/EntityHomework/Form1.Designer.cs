﻿namespace EntityHomework
{
    partial class EntityHomework
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewMain = new System.Windows.Forms.DataGridView();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonCreateUser = new System.Windows.Forms.Button();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.labelUserInfo = new System.Windows.Forms.Label();
            this.buttonEarnGold = new System.Windows.Forms.Button();
            this.labelAvailableActions = new System.Windows.Forms.Label();
            this.buttonUpgradeTownHall = new System.Windows.Forms.Button();
            this.buttonBuyHero = new System.Windows.Forms.Button();
            this.buttonUpgradeMine = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.buttonGameStart = new System.Windows.Forms.Button();
            this.textBoxHeroName = new System.Windows.Forms.TextBox();
            this.labelHeroName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewMain
            // 
            this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMain.Location = new System.Drawing.Point(12, 288);
            this.dataGridViewMain.Name = "dataGridViewMain";
            this.dataGridViewMain.Size = new System.Drawing.Size(844, 68);
            this.dataGridViewMain.TabIndex = 0;
            this.dataGridViewMain.Visible = false;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(729, 12);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(127, 20);
            this.textBoxUsername.TabIndex = 1;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(729, 38);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(127, 20);
            this.textBoxPassword.TabIndex = 2;
            // 
            // buttonCreateUser
            // 
            this.buttonCreateUser.Location = new System.Drawing.Point(729, 96);
            this.buttonCreateUser.Name = "buttonCreateUser";
            this.buttonCreateUser.Size = new System.Drawing.Size(127, 28);
            this.buttonCreateUser.TabIndex = 3;
            this.buttonCreateUser.Text = "Stwóż użytkownika";
            this.buttonCreateUser.UseVisualStyleBackColor = true;
            this.buttonCreateUser.Click += new System.EventHandler(this.buttonCreateUser_Click);
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Font = new System.Drawing.Font("Meiryo", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelUsername.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelUsername.Location = new System.Drawing.Point(644, 9);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(79, 28);
            this.labelUsername.TabIndex = 4;
            this.labelUsername.Text = "Nazwa";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Font = new System.Drawing.Font("Meiryo", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPassword.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelPassword.Location = new System.Drawing.Point(656, 37);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(67, 28);
            this.labelPassword.TabIndex = 5;
            this.labelPassword.Text = "Hasło";
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(729, 128);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(127, 28);
            this.buttonLogin.TabIndex = 7;
            this.buttonLogin.Text = "Zaloguj";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // labelUserInfo
            // 
            this.labelUserInfo.AutoSize = true;
            this.labelUserInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelUserInfo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelUserInfo.Location = new System.Drawing.Point(12, 9);
            this.labelUserInfo.Name = "labelUserInfo";
            this.labelUserInfo.Size = new System.Drawing.Size(456, 20);
            this.labelUserInfo.TabIndex = 8;
            this.labelUserInfo.Text = "Podaj nazwę użytkownika oraz hasło aby się zalogować.";
            // 
            // buttonEarnGold
            // 
            this.buttonEarnGold.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEarnGold.Location = new System.Drawing.Point(12, 257);
            this.buttonEarnGold.Name = "buttonEarnGold";
            this.buttonEarnGold.Size = new System.Drawing.Size(158, 25);
            this.buttonEarnGold.TabIndex = 9;
            this.buttonEarnGold.Text = "Zarabiaj złoto";
            this.buttonEarnGold.UseVisualStyleBackColor = true;
            this.buttonEarnGold.Visible = false;
            this.buttonEarnGold.Click += new System.EventHandler(this.buttonEarnGold_Click);
            // 
            // labelAvailableActions
            // 
            this.labelAvailableActions.AutoSize = true;
            this.labelAvailableActions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAvailableActions.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelAvailableActions.Location = new System.Drawing.Point(12, 234);
            this.labelAvailableActions.Name = "labelAvailableActions";
            this.labelAvailableActions.Size = new System.Drawing.Size(129, 20);
            this.labelAvailableActions.TabIndex = 10;
            this.labelAvailableActions.Text = "Dostępne ruchy: ";
            this.labelAvailableActions.Visible = false;
            // 
            // buttonUpgradeTownHall
            // 
            this.buttonUpgradeTownHall.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpgradeTownHall.Location = new System.Drawing.Point(234, 257);
            this.buttonUpgradeTownHall.Name = "buttonUpgradeTownHall";
            this.buttonUpgradeTownHall.Size = new System.Drawing.Size(158, 25);
            this.buttonUpgradeTownHall.TabIndex = 11;
            this.buttonUpgradeTownHall.Text = "Ulepsz ratusz";
            this.buttonUpgradeTownHall.UseVisualStyleBackColor = true;
            this.buttonUpgradeTownHall.Visible = false;
            this.buttonUpgradeTownHall.Click += new System.EventHandler(this.buttonUpgradeTownHall_Click);
            // 
            // buttonBuyHero
            // 
            this.buttonBuyHero.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonBuyHero.Location = new System.Drawing.Point(698, 257);
            this.buttonBuyHero.Name = "buttonBuyHero";
            this.buttonBuyHero.Size = new System.Drawing.Size(158, 25);
            this.buttonBuyHero.TabIndex = 12;
            this.buttonBuyHero.Text = "Ulepsz bohatera";
            this.buttonBuyHero.UseVisualStyleBackColor = true;
            this.buttonBuyHero.Visible = false;
            this.buttonBuyHero.Click += new System.EventHandler(this.buttonBuyHero_Click);
            // 
            // buttonUpgradeMine
            // 
            this.buttonUpgradeMine.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpgradeMine.Location = new System.Drawing.Point(468, 257);
            this.buttonUpgradeMine.Name = "buttonUpgradeMine";
            this.buttonUpgradeMine.Size = new System.Drawing.Size(158, 25);
            this.buttonUpgradeMine.TabIndex = 13;
            this.buttonUpgradeMine.Text = "Ulepsz kopalnię";
            this.buttonUpgradeMine.UseVisualStyleBackColor = true;
            this.buttonUpgradeMine.Visible = false;
            this.buttonUpgradeMine.Click += new System.EventHandler(this.buttonUpgradeMine_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.Location = new System.Drawing.Point(729, 160);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(127, 28);
            this.buttonLogout.TabIndex = 14;
            this.buttonLogout.Text = "Wyloguj";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Visible = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // buttonGameStart
            // 
            this.buttonGameStart.Location = new System.Drawing.Point(729, 194);
            this.buttonGameStart.Name = "buttonGameStart";
            this.buttonGameStart.Size = new System.Drawing.Size(127, 28);
            this.buttonGameStart.TabIndex = 15;
            this.buttonGameStart.Text = "Uruchom Gamemastera";
            this.buttonGameStart.UseVisualStyleBackColor = true;
            this.buttonGameStart.Click += new System.EventHandler(this.buttonGameStart_Click);
            // 
            // textBoxHeroName
            // 
            this.textBoxHeroName.Location = new System.Drawing.Point(729, 64);
            this.textBoxHeroName.Name = "textBoxHeroName";
            this.textBoxHeroName.Size = new System.Drawing.Size(127, 20);
            this.textBoxHeroName.TabIndex = 16;
            // 
            // labelHeroName
            // 
            this.labelHeroName.AutoSize = true;
            this.labelHeroName.Font = new System.Drawing.Font("Meiryo", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHeroName.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelHeroName.Location = new System.Drawing.Point(566, 64);
            this.labelHeroName.Name = "labelHeroName";
            this.labelHeroName.Size = new System.Drawing.Size(157, 28);
            this.labelHeroName.TabIndex = 17;
            this.labelHeroName.Text = "Imię bohatera";
            // 
            // EntityHomework
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SaddleBrown;
            this.ClientSize = new System.Drawing.Size(868, 372);
            this.Controls.Add(this.labelHeroName);
            this.Controls.Add(this.textBoxHeroName);
            this.Controls.Add(this.buttonGameStart);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.buttonUpgradeMine);
            this.Controls.Add(this.buttonBuyHero);
            this.Controls.Add(this.buttonUpgradeTownHall);
            this.Controls.Add(this.labelAvailableActions);
            this.Controls.Add(this.buttonEarnGold);
            this.Controls.Add(this.labelUserInfo);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.buttonCreateUser);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxUsername);
            this.Controls.Add(this.dataGridViewMain);
            this.Name = "EntityHomework";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewMain;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonCreateUser;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Label labelUserInfo;
        private System.Windows.Forms.Button buttonEarnGold;
        private System.Windows.Forms.Label labelAvailableActions;
        private System.Windows.Forms.Button buttonUpgradeTownHall;
        private System.Windows.Forms.Button buttonBuyHero;
        private System.Windows.Forms.Button buttonUpgradeMine;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Button buttonGameStart;
        private System.Windows.Forms.TextBox textBoxHeroName;
        private System.Windows.Forms.Label labelHeroName;
    }
}

