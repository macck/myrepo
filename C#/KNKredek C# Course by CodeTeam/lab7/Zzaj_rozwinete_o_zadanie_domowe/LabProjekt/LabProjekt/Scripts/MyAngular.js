﻿var app = angular.module("myApp", [])

app.controller("myController",function($scope)
{

    $scope.display = true

    $scope.changeDisplay = function ()
    {
        console.log('dupa')
        $scope.display = !$scope.display;
    }
    
    $scope.goToTeachers = function () {
        window.location.replace("/Home/Teacher/")
    }
        
    var StudentA = {
        name: 'Jan',
        ects: 10,
        isBreak: false,
        semester: 4,
        gradeList: [3, 4, 2, 5, 3, 2]
    }

    var StudentY = {
        name: 'Janek',
        ects: 15,
        isBreak: false,
        semester: 5,
        gradeList: [2, 4, 2, 5, 3, 2]
    }

    var StudentX = {
        name: 'Janusz',
        ects: 13,
        isBreak: false,
        semester: 7,
        gradeList: [5, 4, 2, 4, 3, 2]
    }

    var StudentZ = {
        name: 'Janeczek',
        ects: 4,
        isBreak: true,
        semester: 1,
        gradeList: [3, 4, 3, 5, 3, 2]
    }

    var StudentI = {
        name: 'Jasio',
        ects: 8,
        isBreak: false,
        semester: 6,
        gradeList: [4, 4, 3, 5, 3, 2, 2.5]
    }

    $scope.students = [StudentA,StudentX,StudentY,StudentZ,StudentI]

    })