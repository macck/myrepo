﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class Library : Form
    {
        private SqlConnection sqlConnection;
        public Library()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source=MACIEJ-KOMPUTER\\SQLEXPRESS; database=Library;Trusted_connection=yes");
        }

        // Insert new user to database
        private void AddUser()
        {
            if(textBoxName.Text.Length > 0 && textBoxSurname.Text.Length > 0)
            {
                User.AddUser(sqlConnection, dataGridViewLibrary, textBoxName.Text, textBoxSurname.Text);
            }

        }


        // Output books that have year in between textboxes
        private void SearchByYear()
        {
            Book.SearchByYear(sqlConnection, dataGridViewLibrary, textBoxYearFrom.Text, textBoxYearTo.Text);
        }

        private void buttonShowAllUsers_Click(object sender, EventArgs e)
        {
            User.ShowAllUsers(sqlConnection,dataGridViewLibrary);
        }

        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            AddUser();
        }

        private void buttonShowAllBooks_Click(object sender, EventArgs e)
        {
            Book.ShowAllBooks(sqlConnection, dataGridViewLibrary); // show all books in database
        }


        private void textBoxSearchByTitle_TextChanged(object sender, EventArgs e)
        {
            Book.SearchByTitle(sqlConnection,   dataGridViewLibrary,  textBoxSearchByTitle.Text); // show books by given characters array
        }

        private void buttonShowBorrowingsDetail_Click(object sender, EventArgs e)
        {
            Borrowing.ShowAllBorrowedBooks(sqlConnection, dataGridViewLibrary); // show all borrowed books including by whom
        }

        private void buttonSearchByYear_Click(object sender, EventArgs e)
        {
            SearchByYear();
        }

        private void buttonShowBorrowings_Click(object sender, EventArgs e)
        {
            Borrowing.ShowAllCurrentlyTaken(sqlConnection, dataGridViewLibrary); // show books w/o date of return
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            Borrowing.SortBy(sqlConnection, dataGridViewLibrary, comboBoxSortList.Text); // sort borrowed books by given criteria
        }
    }
}
