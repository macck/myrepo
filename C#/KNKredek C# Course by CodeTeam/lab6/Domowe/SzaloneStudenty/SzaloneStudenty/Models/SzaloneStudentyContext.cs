﻿using System.Data.Entity;

namespace SzaloneStudenty.Models
{
    public class SzaloneStudentyContext : DbContext
    {
        public virtual IDbSet<Student> Students { get; set; }
        public virtual IDbSet<Grade> Grades { get; set; }
    }
}