﻿using System;

public class Worker
{
    private string workerName;
    private float salary;

	public Worker()
	{
	}
    public Worker(string name, float salary)
    {
        this.workerName = name;
        this.salary = salary;
    }
}
