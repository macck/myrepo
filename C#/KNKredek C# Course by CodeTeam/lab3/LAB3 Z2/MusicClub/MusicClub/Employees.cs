﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MusicClub
{
    class Employees : DataBaseTable
    {

        public override SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            databaseQuery = "SELECT Employees.ID, Employees.Name AS 'Imię', Employees.Surname AS 'Nazwisko', Employees.Salary AS 'Pensja', Jobs.Job AS 'Stanowisko', Jobs.Bonus AS 'Premia', Schedules.Monday AS 'Pnd', " +
                "Schedules.Tuesday AS 'WT', Schedules.Wensday AS 'ŚR', Schedules.Thursday AS 'CZW', Schedules.Friday AS 'PT', Schedules.Saturday AS 'SOB', Schedules.Sunday AS 'NIE' FROM Employees JOIN Jobs on JobID=Jobs.ID JOIN Schedules on ScheduleID=Schedules.ID";
            return base.ShowRecords(sqlCon, dataGridView, databaseQuery);
        }

        public override void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            string databaseQuery = "SELECT * FROM Employees WHERE Name LIKE" + "'" + text + "%'";
            base.SearchBy(sqlCon, dataGridViewDatabase, databaseQuery);
        }
    }
}
