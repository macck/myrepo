﻿using EntityHomework.Model;
using EntityHomework.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityHomework
{
    public class RoundComponent
    {
        private int CurrentPlayerId { get; set; }
        private int ActionCount { get; set; }
        private int UserCount { get; set; }
        private const int MaxActions = 3;
        private List<int> ids;
        private static int index = 0;

        public RoundComponent( List<int> ids)
        {
            UserCount = ids.Count;
            this.ids = ids;
            CurrentPlayerId = ids.First();
            ActionCount = 0;
        }

        private void NextPlayer()
        {
            ActionCount = 0;
            index = (index + 1) % UserCount;
            CurrentPlayerId = ids[index];
        }

        public int GetCurrentPlayerID()
        {
            return CurrentPlayerId;
        }

        public int GetActionsLeft()
        {
            return MaxActions - ActionCount;
        }

        public bool PlayerDoAction(int playerId)
        {
            if (playerId == CurrentPlayerId)
            {
                if (ActionCount == MaxActions)
                {
                    NextPlayer();
                    return false;
                }

                else
                {
                    ActionCount++;
                    if (ActionCount == MaxActions)
                    {
                        NextPlayer();
                    }

                    return true;
                }
            }

            else

                return false;
   
        }



    }
}
