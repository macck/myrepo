namespace WebDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Order_Id", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "Order_Id");
        }
    }
}
