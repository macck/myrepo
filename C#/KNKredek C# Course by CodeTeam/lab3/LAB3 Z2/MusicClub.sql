USE [master]
GO
/****** Object:  Database [MusicClub]    Script Date: 2016-12-13 17:20:53 ******/
CREATE DATABASE [MusicClub]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MusicClub', FILENAME = N'I:\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\MusicClub.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MusicClub_log', FILENAME = N'I:\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\MusicClub_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MusicClub] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MusicClub].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MusicClub] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MusicClub] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MusicClub] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MusicClub] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MusicClub] SET ARITHABORT OFF 
GO
ALTER DATABASE [MusicClub] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MusicClub] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MusicClub] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MusicClub] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MusicClub] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MusicClub] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MusicClub] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MusicClub] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MusicClub] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MusicClub] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MusicClub] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MusicClub] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MusicClub] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MusicClub] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MusicClub] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MusicClub] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MusicClub] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MusicClub] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MusicClub] SET  MULTI_USER 
GO
ALTER DATABASE [MusicClub] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MusicClub] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MusicClub] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MusicClub] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MusicClub] SET DELAYED_DURABILITY = DISABLED 
GO
USE [MusicClub]
GO
/****** Object:  Table [dbo].[Concerts]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Concerts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Performer] [nvarchar](50) NOT NULL,
	[DateOfEvent] [date] NOT NULL,
 CONSTRAINT [PK_Concert] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Drinks]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drinks](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[Volume] [int] NOT NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_Drinks] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Salary] [int] NOT NULL,
	[JobID] [int] NOT NULL,
	[ScheduleID] [int] NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Equipment]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Equipment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NULL,
 CONSTRAINT [PK_Equipment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Guests]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guests](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NULL,
 CONSTRAINT [PK_Guests] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Inventory]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EquipmentID] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Jobs]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jobs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Job] [nvarchar](50) NOT NULL,
	[Bonus] [int] NULL,
 CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Receipts]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Receipts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GuestID] [int] NOT NULL,
	[DrinkID] [int] NULL,
	[Amount] [int] NOT NULL,
 CONSTRAINT [PK_Receipt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Schedules]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedules](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Monday] [bit] NOT NULL,
	[Tuesday] [bit] NOT NULL,
	[Wensday] [bit] NOT NULL,
	[Thursday] [bit] NOT NULL,
	[Friday] [bit] NOT NULL,
	[Saturday] [bit] NOT NULL,
	[Sunday] [bit] NOT NULL,
	[Shift] [int] NOT NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Supplies]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplies](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DrinkID] [int] NOT NULL,
	[Amount] [int] NOT NULL,
 CONSTRAINT [PK_Supplies] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 2016-12-13 17:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Price] [int] NOT NULL,
	[EventID] [int] NOT NULL,
	[GuestID] [int] NOT NULL,
 CONSTRAINT [PK_Ticket] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Concerts] ON 

INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (1, N'T.Love', CAST(N'2016-11-29' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (2, N'Dream Theater', CAST(N'2017-03-25' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (3, N'Metallica', CAST(N'2016-12-26' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (4, N'Justin Bimber', CAST(N'2034-04-15' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (5, N'Dżem', CAST(N'2017-01-23' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (6, N'Akurat', CAST(N'2017-02-03' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (7, N'Dziwoludy', CAST(N'2017-01-17' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (8, N'Jelonek', CAST(N'2016-12-29' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (9, N'Sabatonn', CAST(N'2017-02-11' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (10, N'DC/AC', CAST(N'2017-07-30' AS Date))
INSERT [dbo].[Concerts] ([ID], [Performer], [DateOfEvent]) VALUES (11, N'Amisze', CAST(N'2016-03-03' AS Date))
SET IDENTITY_INSERT [dbo].[Concerts] OFF
SET IDENTITY_INSERT [dbo].[Drinks] ON 

INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (2, N'Piwo', N'Zwykłe piwo', 500, 6)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (3, N'Wino', N'Czerwone wytrawne', 80, 9)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (4, N'Dobre piwo', N'APA - polecamy!', 500, 10)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (5, N'RIS', N'Russian imperial stout', 350, 21)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (6, N'Jack Daniels', N'Whiskey in the jar-o!', 40, 9)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (7, N'Wu-dżitsu', N'Nie próbuj walczyć', 40, 8)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (8, N'Woda', N'Zwykła woda', 500, 3)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (9, N'Cola', N'Coca-cola', 330, 4)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (11, N'Sprite', N'Cukru dużo', 330, 4)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (12, N'Gin z tonikiem', N'Dieta cud', 150, 10)
INSERT [dbo].[Drinks] ([ID], [Name], [Description], [Volume], [Price]) VALUES (13, N'Rum', N'Arr, piracie!', 50, 10)
SET IDENTITY_INSERT [dbo].[Drinks] OFF
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (1, N'James', N'Smith', 4000, 1, 1)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (5, N'Tomasz', N'Lis', 2, 6, 7)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (6, N'Neo', N'One', 2000, 2, 3)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (7, N'Zbysiek', N'Anonimowy', 1300, 3, 4)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (8, N'Klodia', N'Jakaśtam', 2000, 2, 2)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (9, N'Radek', N'Sprzątaj', 900, 3, 5)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (10, N'Genowefa', N'Niezbędna', 2500, 5, 2)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (11, N'Heniek', N'Pan', 2300, 4, 1)
INSERT [dbo].[Employees] ([ID], [Name], [Surname], [Salary], [JobID], [ScheduleID]) VALUES (12, N'Donald', N'Tusk', 5, 6, 7)
SET IDENTITY_INSERT [dbo].[Employees] OFF
SET IDENTITY_INSERT [dbo].[Equipment] ON 

INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (1, N'Krzesło', N'Drewniane krzesło dla gości')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (2, N'Stolik', N'Stolik ze stalową nogą bardzo ladna')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (3, N'Kolumna', N'Kolumna głośnikowa')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (4, N'Stół barowy', N'3 metry długości, ceramiczny blat')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (5, N'Szklanka do whisky', N'')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (6, N'Kieliszek', N'40ml kieliszek na mocny alkohol')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (7, N'Kufel ', N'600ml kufel do piwa')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (8, N'Kasa fiskalna', NULL)
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (9, N'Stołek barowy', NULL)
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (10, N'Wzmacniacz', N'wzmacniacz na scenę')
INSERT [dbo].[Equipment] ([ID], [Name], [Description]) VALUES (11, N'Reflektor', N'oświetlenie sceniczne')
SET IDENTITY_INSERT [dbo].[Equipment] OFF
SET IDENTITY_INSERT [dbo].[Guests] ON 

INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (1, N'Pan', N'Jakiśtam')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (2, N'KolejnyFajny', N'Gość')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (3, N'Szatan', N'Serduszko')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (4, N'Son', N'Goku')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (5, N'Zeno', N'Sama')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (6, N'Vegita', N'San')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (7, N'Kill', N'Bill')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (8, N'Mr', N'Anderson')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (9, N'Agent ', N'Smith')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (10, N'Harry ', N'Potter')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (11, N'Jean ', N'Van-Damme')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (12, N'Asterix', N'Obelix')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (13, N'Nicola', N'Tesa')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (15, N'Wiesiek', N'Wszywka')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (16, N'Mietek', N'Masochista')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (17, N'Niekryty ', N'Krytyk')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (18, N'Izak ', N'Skowyrski')
INSERT [dbo].[Guests] ([ID], [Name], [Surname]) VALUES (20, N'Jan', N'Zabłocki')
SET IDENTITY_INSERT [dbo].[Guests] OFF
SET IDENTITY_INSERT [dbo].[Inventory] ON 

INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (1, 1, 56, 2670)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (2, 2, 16, 2300)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (3, 3, 4, 6800)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (4, 4, 2, 7800)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (5, 5, 150, 1700)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (6, 6, 200, 1500)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (7, 7, 100, 2300)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (8, 8, 4, 3600)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (10, 9, 8, 1300)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (11, 10, 2, 6700)
INSERT [dbo].[Inventory] ([ID], [EquipmentID], [Amount], [Value]) VALUES (12, 11, 5, 4600)
SET IDENTITY_INSERT [dbo].[Inventory] OFF
SET IDENTITY_INSERT [dbo].[Jobs] ON 

INSERT [dbo].[Jobs] ([ID], [Job], [Bonus]) VALUES (1, N'Executive', 400)
INSERT [dbo].[Jobs] ([ID], [Job], [Bonus]) VALUES (2, N'Bartender', 150)
INSERT [dbo].[Jobs] ([ID], [Job], [Bonus]) VALUES (3, N'Service', 50)
INSERT [dbo].[Jobs] ([ID], [Job], [Bonus]) VALUES (4, N'Technician', 200)
INSERT [dbo].[Jobs] ([ID], [Job], [Bonus]) VALUES (5, N'Ticket seller', 130)
INSERT [dbo].[Jobs] ([ID], [Job], [Bonus]) VALUES (6, N'ToiletCleaner', 0)
SET IDENTITY_INSERT [dbo].[Jobs] OFF
SET IDENTITY_INSERT [dbo].[Receipts] ON 

INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (1, 1, 4, 2)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (2, 3, 2, 4)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (3, 1, 2, 1)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (4, 4, 6, 1)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (5, 2, 3, 7)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (6, 5, 5, 3)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (7, 8, 2, 4)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (8, 10, 2, 3)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (9, 11, 3, 12)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (10, 9, 3, 1)
INSERT [dbo].[Receipts] ([ID], [GuestID], [DrinkID], [Amount]) VALUES (11, 15, 8, 3)
SET IDENTITY_INSERT [dbo].[Receipts] OFF
SET IDENTITY_INSERT [dbo].[Schedules] ON 

INSERT [dbo].[Schedules] ([ID], [Monday], [Tuesday], [Wensday], [Thursday], [Friday], [Saturday], [Sunday], [Shift]) VALUES (1, 1, 1, 1, 1, 1, 1, 0, 1)
INSERT [dbo].[Schedules] ([ID], [Monday], [Tuesday], [Wensday], [Thursday], [Friday], [Saturday], [Sunday], [Shift]) VALUES (2, 1, 1, 1, 1, 1, 0, 1, 1)
INSERT [dbo].[Schedules] ([ID], [Monday], [Tuesday], [Wensday], [Thursday], [Friday], [Saturday], [Sunday], [Shift]) VALUES (3, 0, 1, 0, 1, 0, 1, 1, 2)
INSERT [dbo].[Schedules] ([ID], [Monday], [Tuesday], [Wensday], [Thursday], [Friday], [Saturday], [Sunday], [Shift]) VALUES (4, 1, 1, 0, 1, 0, 1, 1, 2)
INSERT [dbo].[Schedules] ([ID], [Monday], [Tuesday], [Wensday], [Thursday], [Friday], [Saturday], [Sunday], [Shift]) VALUES (5, 1, 1, 0, 0, 0, 1, 0, 1)
INSERT [dbo].[Schedules] ([ID], [Monday], [Tuesday], [Wensday], [Thursday], [Friday], [Saturday], [Sunday], [Shift]) VALUES (6, 1, 1, 1, 0, 1, 0, 1, 1)
INSERT [dbo].[Schedules] ([ID], [Monday], [Tuesday], [Wensday], [Thursday], [Friday], [Saturday], [Sunday], [Shift]) VALUES (7, 1, 1, 1, 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Schedules] OFF
SET IDENTITY_INSERT [dbo].[Supplies] ON 

INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (4, 2, 100)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (5, 3, 50)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (6, 4, 70)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (7, 5, 150)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (8, 6, 200)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (9, 7, 200)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (10, 8, 150)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (11, 9, 300)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (14, 11, 300)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (16, 12, 300)
INSERT [dbo].[Supplies] ([ID], [DrinkID], [Amount]) VALUES (17, 13, 143)
SET IDENTITY_INSERT [dbo].[Supplies] OFF
SET IDENTITY_INSERT [dbo].[Tickets] ON 

INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (1, N'VIP', 150, 2, 3)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (2, N'Standing', 100, 3, 4)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (3, N'Standing', 100, 2, 1)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (4, N'Standing', 100, 4, 6)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (5, N'Standing', 100, 1, 8)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (6, N'VIP', 150, 7, 10)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (8, N'VIP', 150, 9, 11)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (9, N'Sitting', 130, 8, 13)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (11, N'Sitting', 130, 2, 2)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (12, N'VIP', 150, 5, 9)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (13, N'VIP', 150, 6, 15)
INSERT [dbo].[Tickets] ([ID], [Type], [Price], [EventID], [GuestID]) VALUES (14, N'Standing', 100, 9, 4)
SET IDENTITY_INSERT [dbo].[Tickets] OFF
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Job] FOREIGN KEY([JobID])
REFERENCES [dbo].[Jobs] ([ID])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Job]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedules] ([ID])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_Schedule]
GO
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Equipment] FOREIGN KEY([EquipmentID])
REFERENCES [dbo].[Equipment] ([ID])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [FK_Inventory_Equipment]
GO
ALTER TABLE [dbo].[Receipts]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_Drinks] FOREIGN KEY([DrinkID])
REFERENCES [dbo].[Drinks] ([ID])
GO
ALTER TABLE [dbo].[Receipts] CHECK CONSTRAINT [FK_Receipt_Drinks]
GO
ALTER TABLE [dbo].[Receipts]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_Guests] FOREIGN KEY([GuestID])
REFERENCES [dbo].[Guests] ([ID])
GO
ALTER TABLE [dbo].[Receipts] CHECK CONSTRAINT [FK_Receipt_Guests]
GO
ALTER TABLE [dbo].[Supplies]  WITH CHECK ADD  CONSTRAINT [FK_Supplies_Drinks] FOREIGN KEY([DrinkID])
REFERENCES [dbo].[Drinks] ([ID])
GO
ALTER TABLE [dbo].[Supplies] CHECK CONSTRAINT [FK_Supplies_Drinks]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Concert] FOREIGN KEY([EventID])
REFERENCES [dbo].[Concerts] ([ID])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Ticket_Concert]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Guests] FOREIGN KEY([GuestID])
REFERENCES [dbo].[Guests] ([ID])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Ticket_Guests]
GO
USE [master]
GO
ALTER DATABASE [MusicClub] SET  READ_WRITE 
GO
