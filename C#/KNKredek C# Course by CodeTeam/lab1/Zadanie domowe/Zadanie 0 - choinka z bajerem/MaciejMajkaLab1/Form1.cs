﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaciejMajkaLab1
{
    public partial class Form1 : Form // partial - z kilku plikow , druga czesc w Form1.Designer.cs 
    {
        int counter = 0;
        private string loggedUser = "";
        private bool userIsLogged = false;

        NewWindow window = new NewWindow();
        

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            buttonNewWindow.Visible = false;
        }


        private void buttonClose_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Program zostanie zamknięty", "Informacja",MessageBoxButtons.OKCancel);
            if(result == DialogResult.OK)
            this.Close();
            
        }

        /// <summary>
        /// Sets apperance of this window after user logged/logged out
        /// </summary>
        private void setApperance(bool isLogged )
        {
            if(isLogged)
            {
                this.BackColor = Color.Honeydew;
                labelName.ForeColor = Color.Green;
            }
            else
            {
                this.BackColor = Color.Wheat;
                labelName.ForeColor = Color.Crimson;
                textBoxPassword.Text = "";
            }
            labelLoginInfo.Visible = !isLogged;
            buttonNewWindow.Visible = isLogged;
            pictureBoxUser.Visible = isLogged;
            labelLoggedUser.Visible = isLogged;
            textBoxLogin.Visible = !isLogged;
            textBoxPassword.Visible = !isLogged;
            labelLogin.Visible = !isLogged;
            labelPassword.Visible = !isLogged;
            buttonLeavePresent.Visible = !isLogged;
        }
        /// <summary>
        /// Funkcja logujaca
        /// </summary>
        private void login()
        {
           
                if (textBoxLogin.Text == "test" && textBoxPassword.Text == "test")
                {
                     userIsLogged = true;
                     loggedUser = textBoxLogin.Text;

                try
                {
                    pictureBoxUser.Image = Image.FromFile(@"graphics\userpic.bmp");
                }
                catch
                {
                    pictureBoxUser.BackColor = Color.White;
                }

                     labelLoggedUser.Text = "Zalogowany użytkownik: " + loggedUser;
                     setApperance(userIsLogged); // login ok, change window apperance
                     MessageBox.Show("Zaogowano użytkownika: " + loggedUser, "Powodzenie", MessageBoxButtons.OK);
                    
            }
                
                else
                {
                    DialogResult result = MessageBox.Show("Podano złe dane", "Uwaga", MessageBoxButtons.RetryCancel);
                    if (result == DialogResult.Cancel)
                        this.Close();

                    counter++;
                    if (counter == 6)
                    {
                        BackColor = Color.Red;
                        buttonLogin.Visible = false;
                        MessageBox.Show("Przekroczono liczbę dozwolonych logowań", "Uwaga", MessageBoxButtons.OK); 
                    }

                }
           
        }

        /// <summary>
        /// Logout user, set starting apperance
        /// </summary>
        private void logout()
        {
            loggedUser = "";
            userIsLogged = false;
            setApperance(userIsLogged);

        }

        /// <summary>
        /// Request window to set flag hasGift to true
        /// </summary>
        void sendGift()
        {
            if (window.getGift())
                MessageBox.Show("Już wysłałeś jeden prezent!", "Info");
            else
                MessageBox.Show("Wysłano prezent do użytkownika", "Gratulacje!");
        }


        /// <summary>
        /// Funkcja logujaca uzytkownika - obsluga przycisku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (!userIsLogged)
            {
                login();
                if(userIsLogged)
                 buttonLogin.Text = "Wyloguj";
            }

            else
            {
                logout();
                buttonLogin.Text = "Zaloguj";
            }
        }

        /// <summary>
        /// Opens new window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNewWindow_Click(object sender, EventArgs e)
        {
            window.ShowDialog();
        }

        private void labelLoginInfo_Click(object sender, EventArgs e)
        {

        }

        private void labelLoggedUser_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// On click, sets gift flag in second window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLeavePresent_Click(object sender, EventArgs e)
        {
            sendGift();
        }
    }
}
