﻿namespace ObjectCatalog
{
    partial class EditChapman
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSave = new System.Windows.Forms.Button();
            this.pictureBoxGuitarPic = new System.Windows.Forms.PictureBox();
            this.buttonChangeImage = new System.Windows.Forms.Button();
            this.buttonEditClose = new System.Windows.Forms.Button();
            this.labelAddFormTitle = new System.Windows.Forms.Label();
            this.listBoxStringCount = new System.Windows.Forms.ListBox();
            this.labelColor = new System.Windows.Forms.Label();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.labelGuitarType = new System.Windows.Forms.Label();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.textBoxManufacturer = new System.Windows.Forms.TextBox();
            this.labelStringCount = new System.Windows.Forms.Label();
            this.labelModel = new System.Windows.Forms.Label();
            this.labelManufacturer = new System.Windows.Forms.Label();
            this.listBoxMelodicStrings = new System.Windows.Forms.ListBox();
            this.listBoxBassStrings = new System.Windows.Forms.ListBox();
            this.labelMelodicStrings = new System.Windows.Forms.Label();
            this.labelBassStrings = new System.Windows.Forms.Label();
            this.listBoxType = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(288, 431);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(150, 50);
            this.buttonSave.TabIndex = 53;
            this.buttonSave.Text = "Zapisz";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // pictureBoxGuitarPic
            // 
            this.pictureBoxGuitarPic.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBoxGuitarPic.Location = new System.Drawing.Point(275, 12);
            this.pictureBoxGuitarPic.Name = "pictureBoxGuitarPic";
            this.pictureBoxGuitarPic.Size = new System.Drawing.Size(163, 350);
            this.pictureBoxGuitarPic.TabIndex = 52;
            this.pictureBoxGuitarPic.TabStop = false;
            // 
            // buttonChangeImage
            // 
            this.buttonChangeImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonChangeImage.Location = new System.Drawing.Point(275, 363);
            this.buttonChangeImage.Name = "buttonChangeImage";
            this.buttonChangeImage.Size = new System.Drawing.Size(163, 50);
            this.buttonChangeImage.TabIndex = 51;
            this.buttonChangeImage.Text = "Zmień zdjęcie";
            this.buttonChangeImage.UseVisualStyleBackColor = true;
            this.buttonChangeImage.Click += new System.EventHandler(this.buttonChangeImage_Click);
            // 
            // buttonEditClose
            // 
            this.buttonEditClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEditClose.Location = new System.Drawing.Point(288, 500);
            this.buttonEditClose.Name = "buttonEditClose";
            this.buttonEditClose.Size = new System.Drawing.Size(150, 50);
            this.buttonEditClose.TabIndex = 50;
            this.buttonEditClose.Text = "Zamknij";
            this.buttonEditClose.UseVisualStyleBackColor = true;
            this.buttonEditClose.Click += new System.EventHandler(this.buttonEditClose_Click);
            // 
            // labelAddFormTitle
            // 
            this.labelAddFormTitle.AutoSize = true;
            this.labelAddFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAddFormTitle.Location = new System.Drawing.Point(13, 12);
            this.labelAddFormTitle.Name = "labelAddFormTitle";
            this.labelAddFormTitle.Size = new System.Drawing.Size(242, 29);
            this.labelAddFormTitle.TabIndex = 49;
            this.labelAddFormTitle.Text = "Edytuj wpis w bazie";
            // 
            // listBoxStringCount
            // 
            this.listBoxStringCount.FormattingEnabled = true;
            this.listBoxStringCount.Items.AddRange(new object[] {
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.listBoxStringCount.Location = new System.Drawing.Point(10, 209);
            this.listBoxStringCount.Name = "listBoxStringCount";
            this.listBoxStringCount.Size = new System.Drawing.Size(127, 17);
            this.listBoxStringCount.TabIndex = 48;
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelColor.Location = new System.Drawing.Point(13, 315);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(54, 24);
            this.labelColor.TabIndex = 47;
            this.labelColor.Text = "Kolor";
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(10, 342);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(129, 20);
            this.textBoxColor.TabIndex = 46;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescription.Location = new System.Drawing.Point(12, 363);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(57, 25);
            this.labelDescription.TabIndex = 45;
            this.labelDescription.Text = "Opis";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(10, 397);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(212, 130);
            this.textBoxDescription.TabIndex = 44;
            // 
            // labelGuitarType
            // 
            this.labelGuitarType.AutoSize = true;
            this.labelGuitarType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGuitarType.Location = new System.Drawing.Point(13, 229);
            this.labelGuitarType.Name = "labelGuitarType";
            this.labelGuitarType.Size = new System.Drawing.Size(91, 24);
            this.labelGuitarType.TabIndex = 42;
            this.labelGuitarType.Text = "Typ gitary";
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(10, 156);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(129, 20);
            this.textBoxModel.TabIndex = 41;
            // 
            // textBoxManufacturer
            // 
            this.textBoxManufacturer.Location = new System.Drawing.Point(10, 106);
            this.textBoxManufacturer.Name = "textBoxManufacturer";
            this.textBoxManufacturer.Size = new System.Drawing.Size(129, 20);
            this.textBoxManufacturer.TabIndex = 40;
            // 
            // labelStringCount
            // 
            this.labelStringCount.AutoSize = true;
            this.labelStringCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStringCount.Location = new System.Drawing.Point(13, 179);
            this.labelStringCount.Name = "labelStringCount";
            this.labelStringCount.Size = new System.Drawing.Size(94, 24);
            this.labelStringCount.TabIndex = 39;
            this.labelStringCount.Text = "Ilość strun";
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelModel.Location = new System.Drawing.Point(13, 129);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(63, 24);
            this.labelModel.TabIndex = 38;
            this.labelModel.Text = "Model";
            // 
            // labelManufacturer
            // 
            this.labelManufacturer.AutoSize = true;
            this.labelManufacturer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelManufacturer.Location = new System.Drawing.Point(13, 75);
            this.labelManufacturer.Name = "labelManufacturer";
            this.labelManufacturer.Size = new System.Drawing.Size(97, 24);
            this.labelManufacturer.TabIndex = 37;
            this.labelManufacturer.Text = "Producent";
            // 
            // listBoxMelodicStrings
            // 
            this.listBoxMelodicStrings.FormattingEnabled = true;
            this.listBoxMelodicStrings.Items.AddRange(new object[] {
            "4",
            "5",
            "6"});
            this.listBoxMelodicStrings.Location = new System.Drawing.Point(160, 159);
            this.listBoxMelodicStrings.Name = "listBoxMelodicStrings";
            this.listBoxMelodicStrings.Size = new System.Drawing.Size(77, 17);
            this.listBoxMelodicStrings.TabIndex = 57;
            // 
            // listBoxBassStrings
            // 
            this.listBoxBassStrings.FormattingEnabled = true;
            this.listBoxBassStrings.Items.AddRange(new object[] {
            "4",
            "5",
            "6"});
            this.listBoxBassStrings.Location = new System.Drawing.Point(160, 106);
            this.listBoxBassStrings.Name = "listBoxBassStrings";
            this.listBoxBassStrings.Size = new System.Drawing.Size(77, 17);
            this.listBoxBassStrings.TabIndex = 56;
            // 
            // labelMelodicStrings
            // 
            this.labelMelodicStrings.AutoSize = true;
            this.labelMelodicStrings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMelodicStrings.Location = new System.Drawing.Point(156, 129);
            this.labelMelodicStrings.Name = "labelMelodicStrings";
            this.labelMelodicStrings.Size = new System.Drawing.Size(113, 24);
            this.labelMelodicStrings.TabIndex = 55;
            this.labelMelodicStrings.Text = "Melodyczne";
            // 
            // labelBassStrings
            // 
            this.labelBassStrings.AutoSize = true;
            this.labelBassStrings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBassStrings.Location = new System.Drawing.Point(156, 79);
            this.labelBassStrings.Name = "labelBassStrings";
            this.labelBassStrings.Size = new System.Drawing.Size(77, 24);
            this.labelBassStrings.TabIndex = 54;
            this.labelBassStrings.Text = "Basowe";
            // 
            // listBoxType
            // 
            this.listBoxType.FormattingEnabled = true;
            this.listBoxType.Items.AddRange(new object[] {
            "The stick",
            "Grand Stick",
            "Stick Bass"});
            this.listBoxType.Location = new System.Drawing.Point(10, 256);
            this.listBoxType.Name = "listBoxType";
            this.listBoxType.Size = new System.Drawing.Size(127, 56);
            this.listBoxType.TabIndex = 58;
            // 
            // editChapman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 562);
            this.Controls.Add(this.listBoxType);
            this.Controls.Add(this.listBoxMelodicStrings);
            this.Controls.Add(this.listBoxBassStrings);
            this.Controls.Add(this.labelMelodicStrings);
            this.Controls.Add(this.labelBassStrings);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.pictureBoxGuitarPic);
            this.Controls.Add(this.buttonChangeImage);
            this.Controls.Add(this.buttonEditClose);
            this.Controls.Add(this.labelAddFormTitle);
            this.Controls.Add(this.listBoxStringCount);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.textBoxColor);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.labelGuitarType);
            this.Controls.Add(this.textBoxModel);
            this.Controls.Add(this.textBoxManufacturer);
            this.Controls.Add(this.labelStringCount);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.labelManufacturer);
            this.Name = "editChapman";
            this.Text = "EditChapman";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.PictureBox pictureBoxGuitarPic;
        private System.Windows.Forms.Button buttonChangeImage;
        private System.Windows.Forms.Button buttonEditClose;
        private System.Windows.Forms.Label labelAddFormTitle;
        private System.Windows.Forms.ListBox listBoxStringCount;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label labelGuitarType;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.TextBox textBoxManufacturer;
        private System.Windows.Forms.Label labelStringCount;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Label labelManufacturer;
        private System.Windows.Forms.ListBox listBoxMelodicStrings;
        private System.Windows.Forms.ListBox listBoxBassStrings;
        private System.Windows.Forms.Label labelMelodicStrings;
        private System.Windows.Forms.Label labelBassStrings;
        private System.Windows.Forms.ListBox listBoxType;
    }
}