public class TestujPolimorfizm{
	
	public static void main(String[] args){
		
		Zwierze[] mojeZwierzeta = new Zwierze[2];
		Lew mojLew = new Lew();
		Zyrafa mojaZyrafa = new Zyrafa();
		
		mojeZwierzeta[0] = mojLew;
		mojeZwierzeta[1] = mojaZyrafa;
		
		for(int i=0;i<mojeZwierzeta.length;i++)
		{
			mojeZwierzeta[i].ryknijGlosno();  // wywolane zostana metody z klas odpowiadajacych OBIEKTOM a nie zmiennej odwo�uj�cej jak� jest tablica typu Zwierze (ktore to ma metode ryknijGlosno )
			
		}
		
		
	}
	
}