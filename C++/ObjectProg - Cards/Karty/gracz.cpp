#include "gracz.h"

Gracz::Gracz(std::string const & Imie)
    {
        this->Imie=Imie;
    }


std::string Gracz::PlayerName()
 {
     return Imie;
 }

 std::string Gracz::PlayerSurname()
{
 return "";
}

int Gracz::HandCardColorCheck(int CardIndex)
{
    return hand[CardIndex-1].CheckColor();
}


unsigned int Gracz::HandSize()
{
    return hand.size();
}

void Gracz::HandTakeCard(Karta const & k)
    {
        hand.push_back(k);
    }

void Gracz::HandSort()
    {
        sort(hand.begin(),hand.end(),SortOrderColors);
    }

Karta Gracz::HandPassCard(unsigned int HandIndex)
{
    Karta temp=hand[HandIndex-1];
    hand.erase(hand.begin()+(HandIndex-1));
    return temp;
}

void Gracz::HandShowDown()
    {
        std::cout <<  "_________________________________" << std::endl << std::endl << "Gracz: " << PlayerName() << " " << PlayerSurname() << " posiada karty: " << std::endl << "_________________________________" <<  std::endl << std::endl;
        for(unsigned int i=0;i<HandSize();i++)
        {
            std::cout << " || " <<  " Indeks karty: " << i+1 << " " << hand[i] ;
        }
        std::cout<<std::endl;
    }

 //////////////////////////////////////////////////////////////////////////////////////////////
 ////////////////////////// HUMAN PLAYER //////////////////////////////////////////////////////
 //////////////////////////////////////////////////////////////////////////////////////////////

 HumanGracz::HumanGracz(std::string const & Imie,std::string const & Nazwisko) : Gracz::Gracz(Imie)
    {
        this->Nazwisko=Nazwisko;
    }


std::string HumanGracz::PlayerSurname()
 {
     return Nazwisko;
 }

int HumanGracz::ReturnDecision()
{
    int index;
    std::cin >> index;
    return index;
}
/////////////////////////////
////////////// CPU /////////
///////////////////////////

CpuGracz::CpuGracz(std::string const & Imie) : Gracz::Gracz(Imie)
{

}


int CpuGracz::ReturnDecision() /// implementacja taktyk wymagala by dodania metod zwracajacych karty na stole (obecnie tylko wyswietlane na potrzeby gracza), liczenia prawdopodobienistwa itd...
{
    return rand()%(HandSize())+1; // wrzucaj dowolna karte az system przyjmie
}













