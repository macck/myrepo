﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace nixie
{
    public partial class MainForm : Form
    {
        //odswiezanie ikon
        private void UpdateToolStrip()
        {
            toolConnect.Image = (Uart.IsOpen) ? Properties.Resources.gtk_no : Properties.Resources.gtk_yes;
            toolConnect.Text = (Uart.IsOpen) ? "Rozłącz" : "Połącz";

            toolSet.Enabled = toolScroll.Enabled = Uart.IsOpen;
            toolSettings.Enabled = !Uart.IsOpen;
        }

        public MainForm()
        {
            InitializeComponent();

            UpdateToolStrip();
        }

        //zamykanie programu
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Uart.Close();
        }

        //otwieranie i zamykanie portu
        private void toolConnect_Click(object sender, EventArgs e)
        {
            try {
                if (Uart.IsOpen) Uart.Close();
                else Uart.Open(); 
            }
            catch (UnauthorizedAccessException) { Common.ShowError("Brak dostępu do portu: " + Config.PortName + ". Prawdopodobnie port jest używany przez inny program."); }
            catch (ArgumentException) { Common.ShowError("Nieobsługiwana nazwa portu: " + Config.PortName + "."); }
            catch (IOException) { Common.ShowError("Nieobsługiwany stan portu: " + Config.PortName + ". Sprawdź ustawienia programu."); }
            catch (InvalidOperationException) { Common.ShowError("Port: " + Config.PortName + " jest już otwarty."); }
            UpdateToolStrip();
        }

        //odtruwanie katod
        private void toolScroll_Click(object sender, EventArgs e)
        {
            Uart.Scroll();
        }

        //zamykanie programu
        private void toolExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        //otwieranie okienka informacyjnego
        private void toolAbout_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm();
            about.ShowDialog();
        }

        //otwieranie okienka ustawien
        private void toolSettings_Click(object sender, EventArgs e)
        {
            SettingsForm settings = new SettingsForm();
            settings.ShowDialog();
        }

        //otwieranie okienka nastawy
        private void toolSet_Click(object sender, EventArgs e)
        {
            SetForm set = new SetForm();
            set.ShowDialog();
        }

        

        
    }
}
