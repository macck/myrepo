﻿namespace ObjectCatalog
{
    partial class WindowGuitarAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAddFormTitle = new System.Windows.Forms.Label();
            this.buttonAddClose = new System.Windows.Forms.Button();
            this.buttonAddImage = new System.Windows.Forms.Button();
            this.pictureBoxGuitarPic = new System.Windows.Forms.PictureBox();
            this.labelManufacturer = new System.Windows.Forms.Label();
            this.labelModel = new System.Windows.Forms.Label();
            this.labelStringCount = new System.Windows.Forms.Label();
            this.textBoxManufacturer = new System.Windows.Forms.TextBox();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.labelGuitarType = new System.Windows.Forms.Label();
            this.listBoxType = new System.Windows.Forms.ListBox();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.buttonAddGuitar = new System.Windows.Forms.Button();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.labelColor = new System.Windows.Forms.Label();
            this.listBoxStringCount = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).BeginInit();
            this.SuspendLayout();
            // 
            // labelAddFormTitle
            // 
            this.labelAddFormTitle.AutoSize = true;
            this.labelAddFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAddFormTitle.Location = new System.Drawing.Point(12, 12);
            this.labelAddFormTitle.Name = "labelAddFormTitle";
            this.labelAddFormTitle.Size = new System.Drawing.Size(253, 29);
            this.labelAddFormTitle.TabIndex = 0;
            this.labelAddFormTitle.Text = "Dodaj gitarę do bazy";
            // 
            // buttonAddClose
            // 
            this.buttonAddClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddClose.Location = new System.Drawing.Point(287, 500);
            this.buttonAddClose.Name = "buttonAddClose";
            this.buttonAddClose.Size = new System.Drawing.Size(150, 50);
            this.buttonAddClose.TabIndex = 1;
            this.buttonAddClose.Text = "Zamknij";
            this.buttonAddClose.UseVisualStyleBackColor = true;
            this.buttonAddClose.Click += new System.EventHandler(this.buttonAddClose_Click);
            // 
            // buttonAddImage
            // 
            this.buttonAddImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddImage.Location = new System.Drawing.Point(274, 368);
            this.buttonAddImage.Name = "buttonAddImage";
            this.buttonAddImage.Size = new System.Drawing.Size(163, 50);
            this.buttonAddImage.TabIndex = 2;
            this.buttonAddImage.Text = "Dodaj zdjęcie";
            this.buttonAddImage.UseVisualStyleBackColor = true;
            this.buttonAddImage.Click += new System.EventHandler(this.buttonAddImage_Click);
            // 
            // pictureBoxGuitarPic
            // 
            this.pictureBoxGuitarPic.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBoxGuitarPic.Location = new System.Drawing.Point(274, 12);
            this.pictureBoxGuitarPic.Name = "pictureBoxGuitarPic";
            this.pictureBoxGuitarPic.Size = new System.Drawing.Size(163, 350);
            this.pictureBoxGuitarPic.TabIndex = 3;
            this.pictureBoxGuitarPic.TabStop = false;
            // 
            // labelManufacturer
            // 
            this.labelManufacturer.AutoSize = true;
            this.labelManufacturer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelManufacturer.Location = new System.Drawing.Point(13, 64);
            this.labelManufacturer.Name = "labelManufacturer";
            this.labelManufacturer.Size = new System.Drawing.Size(97, 24);
            this.labelManufacturer.TabIndex = 4;
            this.labelManufacturer.Text = "Producent";
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelModel.Location = new System.Drawing.Point(13, 118);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(63, 24);
            this.labelModel.TabIndex = 5;
            this.labelModel.Text = "Model";
            // 
            // labelStringCount
            // 
            this.labelStringCount.AutoSize = true;
            this.labelStringCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStringCount.Location = new System.Drawing.Point(13, 168);
            this.labelStringCount.Name = "labelStringCount";
            this.labelStringCount.Size = new System.Drawing.Size(94, 24);
            this.labelStringCount.TabIndex = 6;
            this.labelStringCount.Text = "Ilość strun";
            // 
            // textBoxManufacturer
            // 
            this.textBoxManufacturer.Location = new System.Drawing.Point(10, 95);
            this.textBoxManufacturer.Name = "textBoxManufacturer";
            this.textBoxManufacturer.Size = new System.Drawing.Size(129, 20);
            this.textBoxManufacturer.TabIndex = 7;
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(10, 145);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(129, 20);
            this.textBoxModel.TabIndex = 8;
            // 
            // labelGuitarType
            // 
            this.labelGuitarType.AutoSize = true;
            this.labelGuitarType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGuitarType.Location = new System.Drawing.Point(13, 218);
            this.labelGuitarType.Name = "labelGuitarType";
            this.labelGuitarType.Size = new System.Drawing.Size(91, 24);
            this.labelGuitarType.TabIndex = 10;
            this.labelGuitarType.Text = "Typ gitary";
            // 
            // listBoxType
            // 
            this.listBoxType.FormattingEnabled = true;
            this.listBoxType.Items.AddRange(new object[] {
            "elektryczna",
            "akustyczna",
            "klasyczna",
            "basowa"});
            this.listBoxType.Location = new System.Drawing.Point(10, 245);
            this.listBoxType.Name = "listBoxType";
            this.listBoxType.Size = new System.Drawing.Size(127, 56);
            this.listBoxType.TabIndex = 12;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(10, 386);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(212, 130);
            this.textBoxDescription.TabIndex = 13;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescription.Location = new System.Drawing.Point(12, 352);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(57, 25);
            this.labelDescription.TabIndex = 14;
            this.labelDescription.Text = "Opis";
            // 
            // buttonAddGuitar
            // 
            this.buttonAddGuitar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddGuitar.Location = new System.Drawing.Point(287, 431);
            this.buttonAddGuitar.Name = "buttonAddGuitar";
            this.buttonAddGuitar.Size = new System.Drawing.Size(150, 50);
            this.buttonAddGuitar.TabIndex = 15;
            this.buttonAddGuitar.Text = "Zapisz";
            this.buttonAddGuitar.UseVisualStyleBackColor = true;
            this.buttonAddGuitar.Click += new System.EventHandler(this.buttonAddGuitar_Click);
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(10, 331);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(129, 20);
            this.textBoxColor.TabIndex = 17;
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelColor.Location = new System.Drawing.Point(13, 304);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(54, 24);
            this.labelColor.TabIndex = 18;
            this.labelColor.Text = "Kolor";
            // 
            // listBoxStringCount
            // 
            this.listBoxStringCount.FormattingEnabled = true;
            this.listBoxStringCount.Items.AddRange(new object[] {
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.listBoxStringCount.Location = new System.Drawing.Point(10, 195);
            this.listBoxStringCount.Name = "listBoxStringCount";
            this.listBoxStringCount.Size = new System.Drawing.Size(127, 17);
            this.listBoxStringCount.TabIndex = 19;
            // 
            // windowGuitarAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(449, 562);
            this.Controls.Add(this.listBoxStringCount);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.textBoxColor);
            this.Controls.Add(this.buttonAddGuitar);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.listBoxType);
            this.Controls.Add(this.labelGuitarType);
            this.Controls.Add(this.textBoxModel);
            this.Controls.Add(this.textBoxManufacturer);
            this.Controls.Add(this.labelStringCount);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.labelManufacturer);
            this.Controls.Add(this.pictureBoxGuitarPic);
            this.Controls.Add(this.buttonAddImage);
            this.Controls.Add(this.buttonAddClose);
            this.Controls.Add(this.labelAddFormTitle);
            this.Name = "windowGuitarAdd";
            this.Text = "Dodaj gitarę";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAddFormTitle;
        private System.Windows.Forms.Button buttonAddClose;
        private System.Windows.Forms.Button buttonAddImage;
        private System.Windows.Forms.PictureBox pictureBoxGuitarPic;
        private System.Windows.Forms.Label labelManufacturer;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Label labelStringCount;
        private System.Windows.Forms.TextBox textBoxManufacturer;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.Label labelGuitarType;
        private System.Windows.Forms.ListBox listBoxType;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Button buttonAddGuitar;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.ListBox listBoxStringCount;
    }
}