﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class WorkerOfMonth
    {
        public string WorkerName { get; set; } // property duza litera, klamry!
        public int Prize { get; set; }
        public bool Handshake { get; set; }

        public WorkerOfMonth(string workerName, int prize, bool handshake)
        {
            this.WorkerName = workerName;
            this.Prize = prize;
            this.Handshake = handshake;
        }

        
    }
}
