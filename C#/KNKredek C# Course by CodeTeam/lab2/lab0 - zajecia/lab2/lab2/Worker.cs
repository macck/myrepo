﻿using System;

public class Worker
{
    private string workerName;
    private float salary;

	public Worker()
	{
	}
    public Worker(string workerName, float salary)
    {
        this.workerName = workerName;
        this.salary = salary;
    }

    public string GetName()
    {
        return workerName;
    }

    public string GetSalary()
    {
        return salary.ToString();
    }
}
