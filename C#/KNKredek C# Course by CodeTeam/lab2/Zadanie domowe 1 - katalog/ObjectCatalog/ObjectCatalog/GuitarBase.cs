﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectCatalog
{
    public partial class ObjectCatalog : Form
    {
        public ObjectCatalog()
        {
            InitializeComponent();
            pictureBoxDetails.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        List<Guitar> guitarList = new List<Guitar>();
       
        
        /* FUNC SECTION */


        /// <summary>
        /// Opens popup form to add new objects, adds new object into array
        /// 
        /// </summary>
        private void WindowAddOpen()
        {
            WindowGuitarAdd newWindow = new WindowGuitarAdd(guitarList);
            newWindow.ShowDialog();
            DataGridViewRefresh();
            dataGridViewObjectList.DataSource = guitarList;
        }

        /// <summary>
        /// Opens new window that allows to edit guitar
        /// </summary>
        private void WindowEditOpen()
        {
            if( dataGridViewObjectList.SelectedRows.Count == 1)
            {
                WindowGuitarEdit newWindow = new WindowGuitarEdit(guitarList[dataGridViewObjectList.SelectedRows[0].Index]);
                newWindow.LoadInfo();
                newWindow.ShowDialog();
            }
            DataGridViewRefresh();
            ShowObjectsInDataGView();
        }



        void DataGridViewRefresh()
        {
            dataGridViewObjectList.DataSource = null;
        }

        private void ShowObjectsInDataGView()
        {
            dataGridViewObjectList.DataSource = guitarList;
        }

        /// <summary>
        /// Show/hide detailed info about object
        /// </summary>
        /// <param name="obj"></param>
        private void ShowDetails(Guitar obj)
        {
            if(obj.GetPathToImage() != null)
            {
                pictureBoxDetails.Image = Image.FromFile(obj.GetPathToImage());
            }
            textBoxDetails.Text = obj.GetDescription();

            string show = "Pokaż informacje szczegółowe";
            string hide = "Ukryj";
            if(buttonShowInfoObject.Text == show)
            {
                buttonShowInfoObject.Text = hide;
                this.Size = new Size(950, 600);
                textBoxDetails.Visible = true;
                pictureBoxDetails.Visible = true;
                labelDescriptionL.Visible = true;
            }
            else
            {
                buttonShowInfoObject.Text = show;
                this.Size = new Size(900, 600);
                textBoxDetails.Visible = false;
                pictureBoxDetails.Visible = false;
                labelDescriptionL.Visible = false;

            }
        }

       


        /// <summary>
        /// Removes selected object(s) from guitarList
        /// </summary>
        private void RemoveCheckedObject()
        {
            try
            {
                if (dataGridViewObjectList.SelectedRows.Count > 1)
                {

                    int selectedCount = dataGridViewObjectList.SelectedRows.Count;
                    for (int index = 0; index < selectedCount; index++)
                    {
                        try
                        {
                            guitarList.RemoveAt(dataGridViewObjectList.SelectedRows[0].Index); // this gets smaller <- point to first element!!
                        }
                        catch
                        {
                            MessageBox.Show("Problem z indeksowaniem", "Informacja", MessageBoxButtons.OK);
                        }
                    }
                    DataGridViewRefresh();  // <- here because if before dataGridView would point to null!
                    ShowObjectsInDataGView();
                }
                else
                {
                    int index = dataGridViewObjectList.SelectedRows[0].Index;
                    guitarList.RemoveAt(index);
                    DataGridViewRefresh();  // <- here because if before dataGridView would point to null!
                    ShowObjectsInDataGView();
                }
            }

            catch (Exception)
            {

            }

        }

        /// <summary>
        /// serializes objects into binary file
        /// </summary>
        private void SaveObjectsToFile()
        {
            OpenFileDialog openFileToSave = new OpenFileDialog();
            DialogResult result = openFileToSave.ShowDialog();
            string fileName = "";

            try
            {
                if (result == DialogResult.OK)
                {
                    System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                    if (openFileToSave.FileName.EndsWith(".bin"))
                    {
                        fileName = openFileToSave.FileName;
                        System.IO.Stream stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Write);

                        formatter.Serialize(stream, guitarList);


                        stream.Close();
                    }

                    else
                    {
                        System.IO.Stream stream = new System.IO.FileStream(@"save\guitars.bin", System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        for (int i = 0; i < guitarList.Count; i++)
                        {
                            formatter.Serialize(stream, guitarList[i]);
                        }

                        stream.Close();

                    }
                    MessageBox.Show("Zapisano", "Informacja", MessageBoxButtons.OK);
                }
            }
            catch
            {
                MessageBox.Show("Coś poszło nie tak...", "Oj, oj...", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Creates stream, reads file into it, deserializes objecs inside and puts into guitar List
        /// </summary>
        private void GetObjectsFromFile()
        {
            OpenFileDialog openFileToLoad = new OpenFileDialog();
            DialogResult result = openFileToLoad.ShowDialog();
            string fileName = "";

            if (result == DialogResult.OK)
            {
                System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                if (openFileToLoad.FileName.EndsWith(".bin"))
                {
                    fileName = openFileToLoad.FileName;
                    System.IO.Stream stream = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read); // create stream to read file

                    try
                    {
                        guitarList = (List<Guitar>) formatter.Deserialize(stream);
                    }
                    catch
                    {
                        MessageBox.Show("Niepowodzenie", "Informacja", MessageBoxButtons.OK);
                    }
                    finally
                    {
                        MessageBox.Show("Załadowano", "Informacja", MessageBoxButtons.OK);
                        stream.Close();
                    }

                }
            }

            DataGridViewRefresh();
            ShowObjectsInDataGView();
        }

        /*ENDOF FUNC SECTION */


        private void buttonAddObject_Click(object sender, EventArgs e)
        {
            WindowAddOpen();
            
        }

        private void buttonDeleteObject_Click(object sender, EventArgs e)
        {
            RemoveCheckedObject();
        }

        private void buttonEditObject_Click(object sender, EventArgs e)
        {
            WindowEditOpen();
        }

        private void buttonShowInfoObject_Click(object sender, EventArgs e)
        {
            if (dataGridViewObjectList.SelectedRows.Count == 1)
            {
                ShowDetails(guitarList[dataGridViewObjectList.SelectedRows[0].Index]);

            }
        }

        private void buttonReadFromFile_Click(object sender, EventArgs e)
        {
            GetObjectsFromFile();

        }

        private void buttonSaveToFile_Click(object sender, EventArgs e)
        {
            SaveObjectsToFile();
        }

        private void buttonCloseProgram_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   
    }
}
