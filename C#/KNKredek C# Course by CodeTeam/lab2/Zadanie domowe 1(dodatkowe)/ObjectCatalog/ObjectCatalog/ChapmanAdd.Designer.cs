﻿namespace ObjectCatalog
{
    partial class ChapmanAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxStringCount = new System.Windows.Forms.ListBox();
            this.labelColor = new System.Windows.Forms.Label();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.buttonAddGuitar = new System.Windows.Forms.Button();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.listBoxType = new System.Windows.Forms.ListBox();
            this.labelGuitarType = new System.Windows.Forms.Label();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.textBoxManufacturer = new System.Windows.Forms.TextBox();
            this.labelStringCount = new System.Windows.Forms.Label();
            this.labelModel = new System.Windows.Forms.Label();
            this.labelManufacturer = new System.Windows.Forms.Label();
            this.pictureBoxGuitarPic = new System.Windows.Forms.PictureBox();
            this.buttonAddImage = new System.Windows.Forms.Button();
            this.buttonAddClose = new System.Windows.Forms.Button();
            this.labelAddFormTitle = new System.Windows.Forms.Label();
            this.labelBassStrings = new System.Windows.Forms.Label();
            this.labelMelodicStrings = new System.Windows.Forms.Label();
            this.listBoxBassStrings = new System.Windows.Forms.ListBox();
            this.listBoxMelodicStrings = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxStringCount
            // 
            this.listBoxStringCount.FormattingEnabled = true;
            this.listBoxStringCount.Items.AddRange(new object[] {
            "10",
            "11",
            "12"});
            this.listBoxStringCount.Location = new System.Drawing.Point(11, 195);
            this.listBoxStringCount.Name = "listBoxStringCount";
            this.listBoxStringCount.Size = new System.Drawing.Size(127, 17);
            this.listBoxStringCount.TabIndex = 36;
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelColor.Location = new System.Drawing.Point(14, 304);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(54, 24);
            this.labelColor.TabIndex = 35;
            this.labelColor.Text = "Kolor";
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(11, 331);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(129, 20);
            this.textBoxColor.TabIndex = 34;
            // 
            // buttonAddGuitar
            // 
            this.buttonAddGuitar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddGuitar.Location = new System.Drawing.Point(288, 431);
            this.buttonAddGuitar.Name = "buttonAddGuitar";
            this.buttonAddGuitar.Size = new System.Drawing.Size(150, 50);
            this.buttonAddGuitar.TabIndex = 33;
            this.buttonAddGuitar.Text = "Zapisz";
            this.buttonAddGuitar.UseVisualStyleBackColor = true;
            this.buttonAddGuitar.Click += new System.EventHandler(this.buttonAddGuitar_Click);
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescription.Location = new System.Drawing.Point(13, 352);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(57, 25);
            this.labelDescription.TabIndex = 32;
            this.labelDescription.Text = "Opis";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(11, 386);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(212, 130);
            this.textBoxDescription.TabIndex = 31;
            // 
            // listBoxType
            // 
            this.listBoxType.FormattingEnabled = true;
            this.listBoxType.Items.AddRange(new object[] {
            "The stick",
            "Grand Stick",
            "Stick Bass"});
            this.listBoxType.Location = new System.Drawing.Point(11, 245);
            this.listBoxType.Name = "listBoxType";
            this.listBoxType.Size = new System.Drawing.Size(127, 56);
            this.listBoxType.TabIndex = 30;
            // 
            // labelGuitarType
            // 
            this.labelGuitarType.AutoSize = true;
            this.labelGuitarType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGuitarType.Location = new System.Drawing.Point(14, 218);
            this.labelGuitarType.Name = "labelGuitarType";
            this.labelGuitarType.Size = new System.Drawing.Size(91, 24);
            this.labelGuitarType.TabIndex = 29;
            this.labelGuitarType.Text = "Typ gitary";
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(11, 145);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(129, 20);
            this.textBoxModel.TabIndex = 28;
            // 
            // textBoxManufacturer
            // 
            this.textBoxManufacturer.Location = new System.Drawing.Point(11, 95);
            this.textBoxManufacturer.Name = "textBoxManufacturer";
            this.textBoxManufacturer.Size = new System.Drawing.Size(129, 20);
            this.textBoxManufacturer.TabIndex = 27;
            // 
            // labelStringCount
            // 
            this.labelStringCount.AutoSize = true;
            this.labelStringCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelStringCount.Location = new System.Drawing.Point(14, 168);
            this.labelStringCount.Name = "labelStringCount";
            this.labelStringCount.Size = new System.Drawing.Size(94, 24);
            this.labelStringCount.TabIndex = 26;
            this.labelStringCount.Text = "Ilość strun";
            // 
            // labelModel
            // 
            this.labelModel.AutoSize = true;
            this.labelModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelModel.Location = new System.Drawing.Point(14, 118);
            this.labelModel.Name = "labelModel";
            this.labelModel.Size = new System.Drawing.Size(63, 24);
            this.labelModel.TabIndex = 25;
            this.labelModel.Text = "Model";
            // 
            // labelManufacturer
            // 
            this.labelManufacturer.AutoSize = true;
            this.labelManufacturer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelManufacturer.Location = new System.Drawing.Point(14, 64);
            this.labelManufacturer.Name = "labelManufacturer";
            this.labelManufacturer.Size = new System.Drawing.Size(97, 24);
            this.labelManufacturer.TabIndex = 24;
            this.labelManufacturer.Text = "Producent";
            // 
            // pictureBoxGuitarPic
            // 
            this.pictureBoxGuitarPic.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBoxGuitarPic.Location = new System.Drawing.Point(275, 12);
            this.pictureBoxGuitarPic.Name = "pictureBoxGuitarPic";
            this.pictureBoxGuitarPic.Size = new System.Drawing.Size(163, 350);
            this.pictureBoxGuitarPic.TabIndex = 23;
            this.pictureBoxGuitarPic.TabStop = false;
            // 
            // buttonAddImage
            // 
            this.buttonAddImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddImage.Location = new System.Drawing.Point(275, 368);
            this.buttonAddImage.Name = "buttonAddImage";
            this.buttonAddImage.Size = new System.Drawing.Size(163, 50);
            this.buttonAddImage.TabIndex = 22;
            this.buttonAddImage.Text = "Dodaj zdjęcie";
            this.buttonAddImage.UseVisualStyleBackColor = true;
            this.buttonAddImage.Click += new System.EventHandler(this.buttonAddImage_Click);
            // 
            // buttonAddClose
            // 
            this.buttonAddClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddClose.Location = new System.Drawing.Point(288, 500);
            this.buttonAddClose.Name = "buttonAddClose";
            this.buttonAddClose.Size = new System.Drawing.Size(150, 50);
            this.buttonAddClose.TabIndex = 21;
            this.buttonAddClose.Text = "Zamknij";
            this.buttonAddClose.UseVisualStyleBackColor = true;
            this.buttonAddClose.Click += new System.EventHandler(this.buttonAddClose_Click);
            // 
            // labelAddFormTitle
            // 
            this.labelAddFormTitle.AutoSize = true;
            this.labelAddFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAddFormTitle.Location = new System.Drawing.Point(13, 12);
            this.labelAddFormTitle.Name = "labelAddFormTitle";
            this.labelAddFormTitle.Size = new System.Drawing.Size(220, 29);
            this.labelAddFormTitle.TabIndex = 20;
            this.labelAddFormTitle.Text = "Dodaj chapman\'a ";
            // 
            // labelBassStrings
            // 
            this.labelBassStrings.AutoSize = true;
            this.labelBassStrings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBassStrings.Location = new System.Drawing.Point(156, 68);
            this.labelBassStrings.Name = "labelBassStrings";
            this.labelBassStrings.Size = new System.Drawing.Size(77, 24);
            this.labelBassStrings.TabIndex = 39;
            this.labelBassStrings.Text = "Basowe";
            // 
            // labelMelodicStrings
            // 
            this.labelMelodicStrings.AutoSize = true;
            this.labelMelodicStrings.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMelodicStrings.Location = new System.Drawing.Point(156, 118);
            this.labelMelodicStrings.Name = "labelMelodicStrings";
            this.labelMelodicStrings.Size = new System.Drawing.Size(113, 24);
            this.labelMelodicStrings.TabIndex = 40;
            this.labelMelodicStrings.Text = "Melodyczne";
            // 
            // listBoxBassStrings
            // 
            this.listBoxBassStrings.FormattingEnabled = true;
            this.listBoxBassStrings.Items.AddRange(new object[] {
            "4",
            "5",
            "6"});
            this.listBoxBassStrings.Location = new System.Drawing.Point(160, 95);
            this.listBoxBassStrings.Name = "listBoxBassStrings";
            this.listBoxBassStrings.Size = new System.Drawing.Size(77, 17);
            this.listBoxBassStrings.TabIndex = 41;
            // 
            // listBoxMelodicStrings
            // 
            this.listBoxMelodicStrings.FormattingEnabled = true;
            this.listBoxMelodicStrings.Items.AddRange(new object[] {
            "4",
            "5",
            "6"});
            this.listBoxMelodicStrings.Location = new System.Drawing.Point(160, 148);
            this.listBoxMelodicStrings.Name = "listBoxMelodicStrings";
            this.listBoxMelodicStrings.Size = new System.Drawing.Size(77, 17);
            this.listBoxMelodicStrings.TabIndex = 42;
            // 
            // ChapmanAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(449, 562);
            this.Controls.Add(this.listBoxMelodicStrings);
            this.Controls.Add(this.listBoxBassStrings);
            this.Controls.Add(this.labelMelodicStrings);
            this.Controls.Add(this.labelBassStrings);
            this.Controls.Add(this.listBoxStringCount);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.textBoxColor);
            this.Controls.Add(this.buttonAddGuitar);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.listBoxType);
            this.Controls.Add(this.labelGuitarType);
            this.Controls.Add(this.textBoxModel);
            this.Controls.Add(this.textBoxManufacturer);
            this.Controls.Add(this.labelStringCount);
            this.Controls.Add(this.labelModel);
            this.Controls.Add(this.labelManufacturer);
            this.Controls.Add(this.pictureBoxGuitarPic);
            this.Controls.Add(this.buttonAddImage);
            this.Controls.Add(this.buttonAddClose);
            this.Controls.Add(this.labelAddFormTitle);
            this.Name = "ChapmanAdd";
            this.Text = "ChapmanAdd";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxStringCount;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.Button buttonAddGuitar;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.ListBox listBoxType;
        private System.Windows.Forms.Label labelGuitarType;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.TextBox textBoxManufacturer;
        private System.Windows.Forms.Label labelStringCount;
        private System.Windows.Forms.Label labelModel;
        private System.Windows.Forms.Label labelManufacturer;
        private System.Windows.Forms.PictureBox pictureBoxGuitarPic;
        private System.Windows.Forms.Button buttonAddImage;
        private System.Windows.Forms.Button buttonAddClose;
        private System.Windows.Forms.Label labelAddFormTitle;
        private System.Windows.Forms.Label labelBassStrings;
        private System.Windows.Forms.Label labelMelodicStrings;
        private System.Windows.Forms.ListBox listBoxBassStrings;
        private System.Windows.Forms.ListBox listBoxMelodicStrings;
    }
}