import java.util.*;
class Pies implements PiesInterfejs{
	String imiePsa;
	boolean czySzczeka;
	int glosnoscSzczeku;
	static int iloscWarkniec = 0 ;
	final static int FINALNA_ZMIENNA;
	Integer i = new Integer(2);
	int j;
	
	public Pies(){
		
		this("Rex",true);
	}
	
	public Pies(String imie, boolean szczeka)
	{
		this(10);
		imiePsa = imie;
		czySzczeka = szczeka;
	}
	
	public Pies(int glosnosc)
	{
		this.glosnoscSzczeku = glosnosc;
	}

	public static Pies nowyPies(){
		
		Pies myNewDog = new Pies();
		return myNewDog;
		
		
	}
	
	public void go(){
		j = i;
		System.out.println(j);
		System.out.println(i);
	}
	
	static {
		
		FINALNA_ZMIENNA = 3;
	}
	
	public void returnStatic(){
		System.out.println(FINALNA_ZMIENNA);
	}
	
	
	public void szczekaj(int ileRazy)
	{
		for(int i = 0; i <ileRazy;i++)
		{
			if(i == ileRazy - 1)
			System.out.println("WOF! " + glosnoscSzczeku);
		else
			System.out.print("WOF! ");
		
			
		}
	}
	
	 public void szczekajMilusio(int ileRazy)
	{
		for(int i = 0; i <ileRazy;i++)
		{
			if(i == ileRazy - 1)
			System.out.println("MrauHAu! ");
		else
			System.out.print("Hauuu! ");
		
			
		}
		
	}
	
	
	public static void warknij()
	{
		System.out.println("WRRRRRRR!");
		iloscWarkniec++;
		
	}
	
	public static int ileWarkniec()
	{
		return iloscWarkniec;
	}
	
	public static void szczeknijStatycznie(Pies p)
	{
		p.szczekaj(10);
	}
	
	public String retImie()
	{
		return imiePsa;
	}
	
	public int zwrocInt(Integer x)
	{
		return x;
	}
	
	

}
public class TestPoli{

	public static void main(String[] args){
		
		Pies doge = new Pies();
		Object ob = new Object();
		Pies doggy;
		doggy = doge.nowyPies();
		//doge.szczekaj(10);
		
		
		
		for(int i = 0; i<10;i++){
			
			Pies.warknij();
			System.out.println(Pies.ileWarkniec());
			
		}
		
		ArrayList<Pies> mojaListaPsow = new ArrayList<Pies>();
		ArrayList<Object> mojaListaPsowObiektow = new ArrayList<Object>();
		mojaListaPsow.add(doge);
		Pies p = mojaListaPsow.get(0);
		p.szczekaj(2);
		mojaListaPsowObiektow.add(doge);
		ob = mojaListaPsowObiektow.get(0);
		
		Pies piesel = new Pies("Doge",false);
		piesel = (Pies) ob;
		
		piesel.szczekajMilusio(3);
		System.out.println(piesel.retImie());
		Pies.szczeknijStatycznie(doge);
		doge.returnStatic();
		System.out.println(doge.zwrocInt(150));
		doge.go();
		
		String tekst = String.format("Abecadlo %,10.4f i jeszcze drugie chuj wie co %,d i trzecie gowno %x  %tA, %<tb, %<td", 10000.0000, 83129891, 255, new Date());
		System.out.println(tekst);
		Calendar datas = Calendar.getInstance();
		System.out.println(datas.get(Calendar.HOUR_OF_DAY)+":" + datas.get(Calendar.MINUTE));
		
		
	}
}
