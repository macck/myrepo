﻿namespace Library
{
    partial class Library
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewLibrary = new System.Windows.Forms.DataGridView();
            this.buttonShowAllUsers = new System.Windows.Forms.Button();
            this.buttonAddUser = new System.Windows.Forms.Button();
            this.labelName = new System.Windows.Forms.Label();
            this.labelSurname = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.buttonShowAllBooks = new System.Windows.Forms.Button();
            this.textBoxSearchByTitle = new System.Windows.Forms.TextBox();
            this.labelSearchByTitle = new System.Windows.Forms.Label();
            this.buttonSearchByYear = new System.Windows.Forms.Button();
            this.buttonShowBorrowingsDetail = new System.Windows.Forms.Button();
            this.buttonShowBorrowings = new System.Windows.Forms.Button();
            this.comboBoxSortList = new System.Windows.Forms.ComboBox();
            this.buttonSort = new System.Windows.Forms.Button();
            this.labelSearchByYear = new System.Windows.Forms.Label();
            this.textBoxYearFrom = new System.Windows.Forms.TextBox();
            this.labelForm = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.textBoxYearTo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLibrary)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewLibrary
            // 
            this.dataGridViewLibrary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLibrary.Location = new System.Drawing.Point(154, 15);
            this.dataGridViewLibrary.Name = "dataGridViewLibrary";
            this.dataGridViewLibrary.Size = new System.Drawing.Size(649, 300);
            this.dataGridViewLibrary.TabIndex = 0;
            // 
            // buttonShowAllUsers
            // 
            this.buttonShowAllUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowAllUsers.Location = new System.Drawing.Point(3, 15);
            this.buttonShowAllUsers.Name = "buttonShowAllUsers";
            this.buttonShowAllUsers.Size = new System.Drawing.Size(145, 60);
            this.buttonShowAllUsers.TabIndex = 1;
            this.buttonShowAllUsers.Text = "Pokaż wszystkich użytkowników";
            this.buttonShowAllUsers.UseVisualStyleBackColor = true;
            this.buttonShowAllUsers.Click += new System.EventHandler(this.buttonShowAllUsers_Click);
            // 
            // buttonAddUser
            // 
            this.buttonAddUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddUser.Location = new System.Drawing.Point(164, 385);
            this.buttonAddUser.Name = "buttonAddUser";
            this.buttonAddUser.Size = new System.Drawing.Size(90, 25);
            this.buttonAddUser.TabIndex = 2;
            this.buttonAddUser.Text = "Dodaj";
            this.buttonAddUser.UseVisualStyleBackColor = true;
            this.buttonAddUser.Click += new System.EventHandler(this.buttonAddUser_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(119, 333);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(29, 13);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "Imię:";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Location = new System.Drawing.Point(92, 359);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(56, 13);
            this.labelSurname.TabIndex = 4;
            this.labelSurname.Text = "Nazwisko:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(154, 330);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 5;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Location = new System.Drawing.Point(154, 356);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(100, 20);
            this.textBoxSurname.TabIndex = 6;
            // 
            // buttonShowAllBooks
            // 
            this.buttonShowAllBooks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowAllBooks.Location = new System.Drawing.Point(3, 81);
            this.buttonShowAllBooks.Name = "buttonShowAllBooks";
            this.buttonShowAllBooks.Size = new System.Drawing.Size(145, 60);
            this.buttonShowAllBooks.TabIndex = 7;
            this.buttonShowAllBooks.Text = "Pokaż wszystkie książki";
            this.buttonShowAllBooks.UseVisualStyleBackColor = true;
            this.buttonShowAllBooks.Click += new System.EventHandler(this.buttonShowAllBooks_Click);
            // 
            // textBoxSearchByTitle
            // 
            this.textBoxSearchByTitle.Location = new System.Drawing.Point(368, 330);
            this.textBoxSearchByTitle.Name = "textBoxSearchByTitle";
            this.textBoxSearchByTitle.Size = new System.Drawing.Size(100, 20);
            this.textBoxSearchByTitle.TabIndex = 8;
            this.textBoxSearchByTitle.TextChanged += new System.EventHandler(this.textBoxSearchByTitle_TextChanged);
            // 
            // labelSearchByTitle
            // 
            this.labelSearchByTitle.AutoSize = true;
            this.labelSearchByTitle.Location = new System.Drawing.Point(266, 333);
            this.labelSearchByTitle.Name = "labelSearchByTitle";
            this.labelSearchByTitle.Size = new System.Drawing.Size(96, 13);
            this.labelSearchByTitle.TabIndex = 9;
            this.labelSearchByTitle.Text = "Wyszukaj po tytule";
            // 
            // buttonSearchByYear
            // 
            this.buttonSearchByYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSearchByYear.Location = new System.Drawing.Point(437, 372);
            this.buttonSearchByYear.Name = "buttonSearchByYear";
            this.buttonSearchByYear.Size = new System.Drawing.Size(90, 20);
            this.buttonSearchByYear.TabIndex = 10;
            this.buttonSearchByYear.Text = "Szukaj";
            this.buttonSearchByYear.UseVisualStyleBackColor = true;
            this.buttonSearchByYear.Click += new System.EventHandler(this.buttonSearchByYear_Click);
            // 
            // buttonShowBorrowingsDetail
            // 
            this.buttonShowBorrowingsDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowBorrowingsDetail.Location = new System.Drawing.Point(3, 147);
            this.buttonShowBorrowingsDetail.Name = "buttonShowBorrowingsDetail";
            this.buttonShowBorrowingsDetail.Size = new System.Drawing.Size(145, 60);
            this.buttonShowBorrowingsDetail.TabIndex = 11;
            this.buttonShowBorrowingsDetail.Text = "Pokaż szczegóły wypożyczeń";
            this.buttonShowBorrowingsDetail.UseVisualStyleBackColor = true;
            this.buttonShowBorrowingsDetail.Click += new System.EventHandler(this.buttonShowBorrowingsDetail_Click);
            // 
            // buttonShowBorrowings
            // 
            this.buttonShowBorrowings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowBorrowings.Location = new System.Drawing.Point(3, 213);
            this.buttonShowBorrowings.Name = "buttonShowBorrowings";
            this.buttonShowBorrowings.Size = new System.Drawing.Size(145, 60);
            this.buttonShowBorrowings.TabIndex = 12;
            this.buttonShowBorrowings.Text = "Pokaż wypożyczone książki";
            this.buttonShowBorrowings.UseVisualStyleBackColor = true;
            this.buttonShowBorrowings.Click += new System.EventHandler(this.buttonShowBorrowings_Click);
            // 
            // comboBoxSortList
            // 
            this.comboBoxSortList.FormattingEnabled = true;
            this.comboBoxSortList.Items.AddRange(new object[] {
            "Author",
            "Title",
            "Name",
            "Surname",
            "DateOfBorrowing",
            "DateOfReturn"});
            this.comboBoxSortList.Location = new System.Drawing.Point(534, 329);
            this.comboBoxSortList.Name = "comboBoxSortList";
            this.comboBoxSortList.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSortList.TabIndex = 13;
            // 
            // buttonSort
            // 
            this.buttonSort.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSort.Location = new System.Drawing.Point(565, 367);
            this.buttonSort.Name = "buttonSort";
            this.buttonSort.Size = new System.Drawing.Size(90, 25);
            this.buttonSort.TabIndex = 14;
            this.buttonSort.Text = "Sortuj";
            this.buttonSort.UseVisualStyleBackColor = true;
            this.buttonSort.Click += new System.EventHandler(this.buttonSort_Click);
            // 
            // labelSearchByYear
            // 
            this.labelSearchByYear.AutoSize = true;
            this.labelSearchByYear.Location = new System.Drawing.Point(266, 356);
            this.labelSearchByYear.Name = "labelSearchByYear";
            this.labelSearchByYear.Size = new System.Drawing.Size(165, 13);
            this.labelSearchByYear.TabIndex = 15;
            this.labelSearchByYear.Text = "Wyszukaj książki z przedziału lat:";
            // 
            // textBoxYearFrom
            // 
            this.textBoxYearFrom.Location = new System.Drawing.Point(298, 372);
            this.textBoxYearFrom.Name = "textBoxYearFrom";
            this.textBoxYearFrom.Size = new System.Drawing.Size(48, 20);
            this.textBoxYearFrom.TabIndex = 16;
            // 
            // labelForm
            // 
            this.labelForm.AutoSize = true;
            this.labelForm.Location = new System.Drawing.Point(266, 375);
            this.labelForm.Name = "labelForm";
            this.labelForm.Size = new System.Drawing.Size(26, 13);
            this.labelForm.TabIndex = 17;
            this.labelForm.Text = "OD:";
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Location = new System.Drawing.Point(352, 375);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(26, 13);
            this.labelTo.TabIndex = 18;
            this.labelTo.Text = "DO:";
            // 
            // textBoxYearTo
            // 
            this.textBoxYearTo.Location = new System.Drawing.Point(383, 372);
            this.textBoxYearTo.Name = "textBoxYearTo";
            this.textBoxYearTo.Size = new System.Drawing.Size(48, 20);
            this.textBoxYearTo.TabIndex = 19;
            // 
            // Library
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(807, 411);
            this.Controls.Add(this.textBoxYearTo);
            this.Controls.Add(this.labelTo);
            this.Controls.Add(this.labelForm);
            this.Controls.Add(this.textBoxYearFrom);
            this.Controls.Add(this.labelSearchByYear);
            this.Controls.Add(this.buttonSort);
            this.Controls.Add(this.comboBoxSortList);
            this.Controls.Add(this.buttonShowBorrowings);
            this.Controls.Add(this.buttonShowBorrowingsDetail);
            this.Controls.Add(this.buttonSearchByYear);
            this.Controls.Add(this.labelSearchByTitle);
            this.Controls.Add(this.textBoxSearchByTitle);
            this.Controls.Add(this.buttonShowAllBooks);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.buttonAddUser);
            this.Controls.Add(this.buttonShowAllUsers);
            this.Controls.Add(this.dataGridViewLibrary);
            this.Name = "Library";
            this.Text = "Library";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLibrary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewLibrary;
        private System.Windows.Forms.Button buttonShowAllUsers;
        private System.Windows.Forms.Button buttonAddUser;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.Button buttonShowAllBooks;
        private System.Windows.Forms.TextBox textBoxSearchByTitle;
        private System.Windows.Forms.Label labelSearchByTitle;
        private System.Windows.Forms.Button buttonSearchByYear;
        private System.Windows.Forms.Button buttonShowBorrowingsDetail;
        private System.Windows.Forms.Button buttonShowBorrowings;
        private System.Windows.Forms.ComboBox comboBoxSortList;
        private System.Windows.Forms.Button buttonSort;
        private System.Windows.Forms.Label labelSearchByYear;
        private System.Windows.Forms.TextBox textBoxYearFrom;
        private System.Windows.Forms.Label labelForm;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.TextBox textBoxYearTo;
    }
}

