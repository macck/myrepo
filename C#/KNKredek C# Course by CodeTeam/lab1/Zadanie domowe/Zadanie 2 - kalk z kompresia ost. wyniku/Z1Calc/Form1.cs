﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Z1Calc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            axKappaPlayer.Visible = false;
            webBrowserEmbeded.Visible = false;
            this.Size = new Size(500, 600);

        }


        private double[] argTab = new double[2]; // to keep operands 
        private string argToParse = "0"; // string input from buttons, parsed to get args and operator
        private string calcOperator; // what operator is being used during calculation
        private bool setOperator = false; // any operation choosen
        private bool setSpecial = false; // if any coma appears in single operand this sets to true
        private bool equalized = false; // flag to indicate that result is ready, to refresh textBox
        private string toFile = "";



        /// <summary>
        /// writes to file (wynik.txt, wynik.gz) last equation executed
        /// </summary>
        private void writeToFile()
        {
            // try catch here cuz exceptions much :pepe: just to show that its probably working :D
            using(System.IO.FileStream newFile = System.IO.File.Create("wynik.txt"))
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(@"files\wynik.txt"))
                {
                    writer.WriteLine(toFile);
                }
            }

            using(System.IO.FileStream fileOriginal = System.IO.File.OpenRead(@"files\wynik.txt"))
            {         
                   using (System.IO.FileStream compressed = System.IO.File.Create(@"files\wynik.txt.gz"))
                    {
                        using (System.IO.Compression.GZipStream archive = new System.IO.Compression.GZipStream(compressed, System.IO.Compression.CompressionMode.Compress))
                        {
                            fileOriginal.CopyTo(archive);
                        }
                    }                
            }            
        }

        /// <summary>
        /// Handles numbers with commas
        /// </summary>
        /// <param name="butt"></param>
        private void specialComa(string valComma)
        {
            if (!setSpecial)
            {
                string value = valComma;
                setSpecial = true;
                writeArg(value);
            }
        }

        /// <summary>
        /// Handles button object input, sets flag, passes arguments to writeArg
        /// </summary>
        /// <param name="butt"></param>
        private void operatorWrite(Button butt)
        {
            if (!setOperator)
            {
                string value = butt.Text;
                setOperator = true;
                calcOperator = value;
                setSpecial = false;
                writeArg(value);
            }
        }


        /// <summary>
        /// Assign operands to correct positions 
        /// </summary>
        /// <param name="arg"></param>
        private void writeArg(string arg)
        {
            if(equalized && !setOperator) // if there is number input after calculate(), set calc to default, otherwise use result as 1st operand
            {
                equalized = false;
                richTextBoxCalcOutput.Text = "";
                argToParse = "";
                argTab[0] = 0;
                argTab[1] = 0;
            }
                argToParse += arg;
                toFile += arg;
            richTextBoxCalcOutput.Text = argToParse;     
        }



        private void buttonEquals_Click(object sender, EventArgs e)
        {
                calculate();
                  DialogResult dialogRes = MessageBox.Show(@"Prawdopodobnie zapisano działanie w pliku files\wynik.txt oraz wynik.txt.gz", "File probably saved", MessageBoxButtons.OK);
                if (dialogRes == DialogResult.OK)
                {
                    System.Media.SystemSounds.Asterisk.Play();
                }

            /* to powinno być weryfikowane i zrobione w innym miejscu ale czas goni*/
        }
    
        /// <summary>
        /// Logic unit, exec arythmetics
        /// </summary>
        private void calculate()
        {
            
            stringParse(argToParse, calcOperator);
            bool dividedByZero = false;
            
            switch (calcOperator)
            {
                case "+":
                    argTab[0] = argTab[0] + argTab[1];
                   
                    break;

                case "-":
                    argTab[0] = argTab[0] - argTab[1];
                    break;

                case "*":
                    argTab[0] = argTab[0] * argTab[1];
                    break;

                case "s": // if x sqrt y multiply x*sqrt(y) else output sqrt of y
                        if(argTab[0] == 0)
                        argTab[0] =  Math.Sqrt(argTab[1]);

                        else
                        argTab[0] = argTab[0] * Math.Sqrt(argTab[1]);

                    break;

                case "p":

                    argTab[0] = Math.Pow(argTab[0],argTab[1]);
                    break;

                case "/":
                    if (argTab[1] != 0)
                     argTab[0] = argTab[0] / argTab[1];
                    else
                    {
                        if(setOperator == false)
                        {
                            printResult( argTab[0].ToString() );
                        }
                        else
                        {
                            printResult("Dzielenie przez zero!");
                            dividedByZero = true;
                        }
                    }

                    break;
             }
                    if(!dividedByZero) 
                    printResult(argTab[0].ToString());
                    argToParse = argTab[0].ToString();

                     toFile += " = " + argToParse;
                    writeToFile();
                     toFile = "";

                    equalized = true;
         }
 
        /// <summary>
        /// Prints outputs to textBox, re-enables input to calc.
        /// </summary>
        /// <param name="result"></param>
        private void printResult(string result)
        {
            richTextBoxCalcOutput.Text = result;
            setOperator = false;
            setSpecial = false;
        }

        private void richTextBoxCalcOutput_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Original state of calculator inputs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            argToParse = "";
            setOperator = false;
            argTab[0] = 0;
            argTab[1] = 0;
            richTextBoxCalcOutput.Text = "";
        }

        /// <summary>
        /// Parses string into double delimiting by given operator, put results into argTab fields
        /// </summary>
        /// <param name="stringToParse"></param>
        /// <param name="delim"></param>
        private void stringParse(string stringToParse,string delim)
        {
            int tempIndex = 0; // to store index of separator
            int stringLength = stringToParse.Length; // obtain length of parsed string
            string tempBuffer; // to keep slice of string

            if(stringToParse.StartsWith("s"))  // in case user wants just to sqrt single argument and starts with sqrt operator
            {
                    tempBuffer = stringToParse.Substring(1, stringLength - 1);
                     double.TryParse(tempBuffer, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out argTab[1]);
            }

            else if (setOperator == true)
            {
                for (int i = 0; i < stringLength; i++) // loop on each element of string array
                {
                    if (stringToParse[i] == delim[0]) // find position of character other than defined separator
                    {
                        tempIndex = i; // store position of separator that begins array of characters

                            tempBuffer = stringToParse.Substring(0, i);
                            double.TryParse(tempBuffer, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out argTab[0]);
                            tempBuffer = stringToParse.Substring(i + 1, stringLength - (i + 1));
                            double.TryParse(tempBuffer, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out argTab[1]);
                            break;
                                             
                    }
                }

                richTextBoxCalcOutput.Text = (argTab[0].ToString() + delim + argTab[1].ToString());
            }

            else
            {
                double.TryParse(stringToParse, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, out argTab[0]);
            }

         } // end stringParse




        /* INPUT BUTTONS */

        private void buttonInput0_Click(object sender, EventArgs e)
        {
            string value = "0";
            writeArg(value);
        }


        private void buttonInput1_Click(object sender, EventArgs e)
        {
            string value = "1";
            writeArg(value);
        }


        private void buttonInput2_Click(object sender, EventArgs e)
        {
            string value = "2";
            writeArg(value);
        }


        private void buttonInput3_Click(object sender, EventArgs e)
        {
            string value = "3";
            writeArg(value);
        }


        private void buttonInput4_Click(object sender, EventArgs e)
        {
            string value = "4";
            writeArg(value);
        }


        private void buttonInput5_Click(object sender, EventArgs e)
        {
            string value = "5";
            writeArg(value);
        }


        private void buttonInput6_Click(object sender, EventArgs e)
        {
            string value = "6";
            writeArg(value);
        }


        private void buttonInput7_Click(object sender, EventArgs e)
        {
            string value = "7";
            writeArg(value);
        }


        private void buttonInput8_Click(object sender, EventArgs e)
        {
            string value = "8";
            writeArg(value);
        }


        private void buttonInput9_Click(object sender, EventArgs e)
        {
            string value = "9";
            writeArg(value);
        }


        private void buttonAdd_Click(object sender, EventArgs e)
        {
            operatorWrite(buttonAdd);
        }


        private void buttonSubtract_Click(object sender, EventArgs e)
        {
            operatorWrite(buttonSubtract);
        }


        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            operatorWrite(buttonMultiply);
        }


        private void buttonDivide_Click(object sender, EventArgs e)
        {
            operatorWrite(buttonDivide);
        }


        private void buttonNatBase_Click(object sender, EventArgs e)
        {
            specialComa("2,71828");
        }


        private void buttonNumPi_Click(object sender, EventArgs e)
        {
            specialComa("3,14159");
        }


        private void buttonComa_Click(object sender, EventArgs e)
        {
            specialComa(",");
        }


        private void buttonSqrt_Click(object sender, EventArgs e)
        {
            if (!setOperator)
            {
                string value = "s";
                setOperator = true;
                calcOperator = "s";
                writeArg(value);
            }
        }

        private void buttonSquared_Click(object sender, EventArgs e)
        {
            if (!setOperator)
            {
                string value = "p";
                setOperator = true;
                calcOperator = "p";
                writeArg(value);
            }
        }


        /*OTHER BUTTONS */

        private void buttonSwitchKappa_Click(object sender, EventArgs e)
        {
            enableKappa();
        }

        /// <summary>
        /// Resize window, change background, play video
        /// </summary>
        private void enableKappa()
        {
            if (this.Size.Height < 900)
            {

                axKappaPlayer.URL = @"videos\kappa.avi";

                try
                {
                    BackgroundImage = Image.FromFile(@"graphics\kappabground.png");
                    
                }
                catch(System.IO.FileNotFoundException)
                {
                    MessageBox.Show("Nie znaleziono kappy", "Kappa");
                }

                axKappaPlayer.Visible = true;
                this.Size = new Size(this.Size.Width, 900);
            }
            else
            {
                BackgroundImage = null;
                this.Size = new Size(this.Size.Width, 600);
                axKappaPlayer.Ctlcontrols.stop();
                axKappaPlayer.close(); // Memory relase !!!
                axKappaPlayer.Visible = false;
            }
        }



        private void buttonUltimate_Click(object sender, EventArgs e)
        {
            toggleBrowser();
        }

        /// <summary>
        /// Expands window, shows browser
        /// </summary>
        private void toggleBrowser()
        {
            if (this.Size.Width < 1200)
            {
                webBrowserEmbeded.Visible = true;
                this.Size = new Size(1200, this.Size.Height);
                buttonUltimate.Text = "Ukryj";
            }
            else
            {
                this.Size = new Size(500, this.Size.Height);
                webBrowserEmbeded.Visible = false;
                buttonUltimate.Text = "Ultimate Answers";

            }

        }

        /* BELOW UNUSED EVENTS */

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
           
        }

        private void axKappaPlayer_Enter(object sender, EventArgs e)
        {

        }


    }
}
