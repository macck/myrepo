class GitaraElektryczna{
	String rodzaj;
	int iloscKonwerterow;
	boolean uzywanaPrzezGwiazde;
	
	void setRodzaj(String rodzajGitary)
	{
		rodzaj = rodzajGitary;
	}
	
	void setIloscKonwerterow(int ilosc)
	{
		iloscKonwerterow = ilosc;
	}
	
	void setUzywanaPrzezGwiazde(boolean used){
		
		uzywanaPrzezGwiazde = used;
	}
	
	String getRodzaj()
	{
		return rodzaj;
	}
	
	int getIloscKonwerterow()
	{
		return iloscKonwerterow;
	}
	
	boolean getUzywanaPrzezGwiazde()
	{
		return uzywanaPrzezGwiazde;
	}
	
}

class Metody{
	public static void main(String[] args){
		
		GitaraElektryczna gibson = new GitaraElektryczna();
		
		gibson.setRodzaj("Les Paul");
		gibson.setIloscKonwerterow (2);
		gibson.setUzywanaPrzezGwiazde(true);
		
		System.out.println("Gitara " + gibson.getRodzaj() + " ma " + gibson.getIloscKonwerterow() + " przetwornikow i jest uzywana przez gwiazde: " + gibson.getUzywanaPrzezGwiazde());
		
		
	}
	
}