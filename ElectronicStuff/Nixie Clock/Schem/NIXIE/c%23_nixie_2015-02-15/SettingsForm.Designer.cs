﻿namespace nixie
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Baud = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.RefreshBtn = new System.Windows.Forms.Button();
            this.PortName = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BootloaderBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SelectBootloaderBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AvrCombo = new System.Windows.Forms.ComboBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ApplyBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Baud)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Baud);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.RefreshBtn);
            this.groupBox2.Controls.Add(this.PortName);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(215, 80);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ustawienia portu";
            // 
            // Baud
            // 
            this.Baud.Location = new System.Drawing.Point(129, 47);
            this.Baud.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.Baud.Name = "Baud";
            this.Baud.Size = new System.Drawing.Size(80, 20);
            this.Baud.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Szybkość transmisji:";
            // 
            // RefreshBtn
            // 
            this.RefreshBtn.Location = new System.Drawing.Point(129, 15);
            this.RefreshBtn.Name = "RefreshBtn";
            this.RefreshBtn.Size = new System.Drawing.Size(80, 23);
            this.RefreshBtn.TabIndex = 2;
            this.RefreshBtn.Text = "Odśwież listę";
            this.RefreshBtn.UseVisualStyleBackColor = true;
            this.RefreshBtn.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // PortName
            // 
            this.PortName.FormattingEnabled = true;
            this.PortName.Location = new System.Drawing.Point(48, 17);
            this.PortName.Name = "PortName";
            this.PortName.Size = new System.Drawing.Size(75, 21);
            this.PortName.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Port:";
            // 
            // BootloaderBtn
            // 
            this.BootloaderBtn.Location = new System.Drawing.Point(6, 79);
            this.BootloaderBtn.Name = "BootloaderBtn";
            this.BootloaderBtn.Size = new System.Drawing.Size(203, 27);
            this.BootloaderBtn.TabIndex = 10;
            this.BootloaderBtn.Text = "Otwórz bootloader";
            this.BootloaderBtn.UseVisualStyleBackColor = true;
            this.BootloaderBtn.Click += new System.EventHandler(this.BootloaderButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SelectBootloaderBtn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.AvrCombo);
            this.groupBox1.Controls.Add(this.BootloaderBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 115);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Aktualizacja firmware\'u";
            // 
            // SelectBootloaderBtn
            // 
            this.SelectBootloaderBtn.Image = global::nixie.Properties.Resources.document_open;
            this.SelectBootloaderBtn.Location = new System.Drawing.Point(129, 46);
            this.SelectBootloaderBtn.Name = "SelectBootloaderBtn";
            this.SelectBootloaderBtn.Size = new System.Drawing.Size(80, 27);
            this.SelectBootloaderBtn.TabIndex = 14;
            this.SelectBootloaderBtn.Text = "Wybierz";
            this.SelectBootloaderBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SelectBootloaderBtn.UseVisualStyleBackColor = true;
            this.SelectBootloaderBtn.Click += new System.EventHandler(this.SelectBootloaderBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Lokalizacja bootloadera:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Urządzenie:";
            // 
            // AvrCombo
            // 
            this.AvrCombo.FormattingEnabled = true;
            this.AvrCombo.Location = new System.Drawing.Point(109, 19);
            this.AvrCombo.Name = "AvrCombo";
            this.AvrCombo.Size = new System.Drawing.Size(100, 21);
            this.AvrCombo.TabIndex = 11;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Pliki .exe|*.exe|Pliki .py|*.py";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // ApplyBtn
            // 
            this.ApplyBtn.Image = global::nixie.Properties.Resources.gtk_apply;
            this.ApplyBtn.Location = new System.Drawing.Point(152, 220);
            this.ApplyBtn.Name = "ApplyBtn";
            this.ApplyBtn.Size = new System.Drawing.Size(75, 27);
            this.ApplyBtn.TabIndex = 9;
            this.ApplyBtn.Text = "Zapisz";
            this.ApplyBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ApplyBtn.UseVisualStyleBackColor = true;
            this.ApplyBtn.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Image = global::nixie.Properties.Resources.gtk_cancel;
            this.CancelBtn.Location = new System.Drawing.Point(12, 220);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(75, 27);
            this.CancelBtn.TabIndex = 8;
            this.CancelBtn.Text = "Wyjdź";
            this.CancelBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 257);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ApplyBtn);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ustawienia";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Baud)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button RefreshBtn;
        private System.Windows.Forms.ComboBox PortName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown Baud;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button ApplyBtn;
        private System.Windows.Forms.Button BootloaderBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox AvrCombo;
        private System.Windows.Forms.Button SelectBootloaderBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}