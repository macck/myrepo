﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MusicClub
{
    class Supplies : DataBaseTable
    {

        public override SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            databaseQuery = "SELECT Drinks.Name as 'Drink', Drinks.Description AS 'OPIS', Drinks.Volume AS 'Objętość', Drinks.Price AS 'Cena' , Supplies.Amount AS 'Dostępna ilość' FROM Supplies JOIN Drinks on DrinkID=Drinks.ID";
            return base.ShowRecords(sqlCon, dataGridView, databaseQuery);
        }

        public override void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            string databaseQuery = "SELECT * FROM Supplies WHERE Amount LIKE" + "'" + text + "%'";
            base.SearchBy(sqlCon, dataGridViewDatabase, databaseQuery);
        }
    }
}
