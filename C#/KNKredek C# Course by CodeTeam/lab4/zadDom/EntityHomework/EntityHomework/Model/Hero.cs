﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityHomework.Model
{
    public class Hero : Entity
    {
        public int UserId { get; set; }
        public string HeroName { get; set; }
        public int Attack { get; set; }
        public int Defence { get; set; }
        public int Level { get; set; }
        public float Coeff { get; set; }
    }
}
