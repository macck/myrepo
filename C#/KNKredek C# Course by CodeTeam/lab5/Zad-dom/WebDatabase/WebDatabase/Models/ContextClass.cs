﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebDatabase.Models;

namespace ApiDatabase.Models
{
    public class ContextClass : DbContext
    {
        public virtual IDbSet<User> Users { get; set; }
        public virtual IDbSet<Order> Orders { get; set; }
        public virtual IDbSet<Product> Products { get; set; }
        public virtual IDbSet<ProductOrder> OrderedProducts { get; set; }
    }
}