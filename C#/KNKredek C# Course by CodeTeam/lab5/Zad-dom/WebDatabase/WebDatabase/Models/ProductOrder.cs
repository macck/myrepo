﻿using ApiDatabase.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebDatabase.Models
{
    
    public class ProductOrder : EntityObjModel
    {    
        public int Product_Id { get; set; }
        public int Quantity { get;  set;}
    }
}