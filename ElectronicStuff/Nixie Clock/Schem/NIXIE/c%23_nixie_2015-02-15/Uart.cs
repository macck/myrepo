﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace nixie
{
    //obsluga portu COM, generowanie ramek wedlug specyfikacji
    public static partial class Uart
    {
        private static byte frameStartByte;
        private static byte frameStopByte;
        private static SerialPort port = new System.IO.Ports.SerialPort();

        public static bool IsOpen
        {
            get { return port.IsOpen; }
        }

        private static string ByteToHex(byte value) 
        { 
            return value.ToString("X2"); 
        }

        public static void Open()
        {
            Close();
            frameStartByte = Config.FrameStartByte;
            frameStopByte = Config.FrameStopByte;
            port = new System.IO.Ports.SerialPort(Config.PortName, Config.PortBaud);
            port.Open();
        }

        public static void Close()
        {
            if (IsOpen) port.Close();
        }

        public static string SendCommand(byte commandCode, byte[] arguments = null)
        {
            if (!IsOpen)
                return "";

            arguments = arguments ?? new byte[0];

            //struktura ramki: [0x00] [dlugosc] [kod] [arg1] .. [argn] [crc] [0x0A]
            //kazdy bajt poza poczatkiem i koncem (0x00 i 0x0A) jest wysylany w postaci tekstowo-heksadecymalnej (74 -> "4A")
            byte frameLength = (byte)(6 + 2 * arguments.Length);
            byte crc = (byte)(frameLength ^ commandCode);
            string command = ByteToHex(frameLength) + ByteToHex(commandCode);
            foreach (byte arg in arguments)
            {
                command += ByteToHex(arg);
                crc ^= arg;
            }
            command = Convert.ToChar(frameStartByte) + command + ByteToHex(crc) + Convert.ToChar(frameStopByte);

            port.Write(command);
            return "\\x" + ByteToHex(frameStartByte) + " "
                         + command.Substring(1, command.Length - 2)
                         + " \\x" + ByteToHex(frameStopByte);
        }
    }
}
