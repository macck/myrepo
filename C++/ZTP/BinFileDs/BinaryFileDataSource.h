/*
in * BinaryFileDataSource.h
 *
 *  Created on: 09.11.2016
 *      Author: c
 */

#ifndef BINARYFILEDATASOURCE_H_
#define BINARYFILEDATASOURCE_H_

#include <vector>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include "inttypes.h"
#include <iostream>
#include "DataSource.h"

class BinaryFileDataSource : public DataSource { // 16 bitow, 8 kanalow little-endian U2  2-bajty 1 pr�bka

private:

		bool hasDataLeft; // to indicate if there is anything to read in input str
		bool isFileOpen;
		bool dataUsed; // flag to test if string have been already parsed into vector of ints
		std::fstream inputStream;
		uint8_t dataSize; // how many bits represent one sample
		char upDataRead[16]; // unparsed data read from input stream to be processed 16 chars rep 16 bytes (8x2byte data)

		void charParse(std::vector<int> & dataVect);


public:
		BinaryFileDataSource(); // no member init
		BinaryFileDataSource(const std::string & path, uint8_t dataSize); // get file, and how many bits required to represent one value
		std::vector<int> getData();
		bool hasData();
		virtual ~BinaryFileDataSource();


};

#endif /* BINARYFILEDATASOURCE_H_ */
