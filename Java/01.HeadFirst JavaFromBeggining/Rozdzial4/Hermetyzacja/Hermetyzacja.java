class DobryPiesek {
	
	private int wielkosc;
	
	public void setWielkosc(int size){
		if(size >0 && size < 100)
		{
			wielkosc = size;
		}
		else
		{
			System.out.println("Nieprawidlowy argument.");  // raczej zla metoda, w zasadzie nalezalo by pewnie rzucic wyjatkiem 
		}
	}
	
	public int getWielkosc()
	{
		return wielkosc;
	}
	
	
	
}

class Hermetyzacja{
	public static void main(String[] args){
		
		DobryPiesek pies = new DobryPiesek();
		
		pies.setWielkosc(125);
		pies.getWielkosc();
		pies.setWielkosc(10);
		System.out.println(pies.getWielkosc());
		
		
	}
}