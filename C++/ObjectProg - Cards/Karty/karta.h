#ifndef KARTA_H_INCLUDED
#define KARTA_H_INCLUDED
#include <iostream>
#include <string>


class Karta{

int Kolor;
int Wartosc;

public:

    Karta();
    Karta(const int & Kolor, const int & Wartosc);
    int CheckColor();
    int CheckValue();


};
    std::ostream & operator << (std::ostream & str, Karta k);

     bool SortOrderColors(Karta k1, Karta k2);
     bool SortOrderValues(Karta k1, Karta k2);

#endif // KARTA_H_INCLUDED
