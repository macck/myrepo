﻿namespace MusicClub
{
    partial class MusicClub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusicClub));
            this.dataGridViewDatabase = new System.Windows.Forms.DataGridView();
            this.buttonShowAllRecords = new System.Windows.Forms.Button();
            this.comboBoxSelectTable = new System.Windows.Forms.ComboBox();
            this.labelInformation = new System.Windows.Forms.Label();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.Edit = new System.Windows.Forms.Button();
            this.labelSearch = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.labelInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDatabase)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewDatabase
            // 
            this.dataGridViewDatabase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDatabase.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewDatabase.Name = "dataGridViewDatabase";
            this.dataGridViewDatabase.Size = new System.Drawing.Size(769, 347);
            this.dataGridViewDatabase.TabIndex = 0;
            // 
            // buttonShowAllRecords
            // 
            this.buttonShowAllRecords.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowAllRecords.Location = new System.Drawing.Point(12, 471);
            this.buttonShowAllRecords.Name = "buttonShowAllRecords";
            this.buttonShowAllRecords.Size = new System.Drawing.Size(129, 41);
            this.buttonShowAllRecords.TabIndex = 1;
            this.buttonShowAllRecords.Text = "Wyświetl";
            this.buttonShowAllRecords.UseVisualStyleBackColor = true;
            this.buttonShowAllRecords.Click += new System.EventHandler(this.buttonShowAllRecords_Click);
            // 
            // comboBoxSelectTable
            // 
            this.comboBoxSelectTable.FormattingEnabled = true;
            this.comboBoxSelectTable.Items.AddRange(new object[] {
            "Concerts",
            "Drinks",
            "Employees",
            "Equipment",
            "Guests",
            "Inventory",
            "Jobs",
            "Receipts",
            "Schedules",
            "Supplies",
            "Tickets"});
            this.comboBoxSelectTable.Location = new System.Drawing.Point(12, 397);
            this.comboBoxSelectTable.Name = "comboBoxSelectTable";
            this.comboBoxSelectTable.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSelectTable.TabIndex = 2;
            // 
            // labelInformation
            // 
            this.labelInformation.AutoSize = true;
            this.labelInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelInformation.Location = new System.Drawing.Point(12, 374);
            this.labelInformation.Name = "labelInformation";
            this.labelInformation.Size = new System.Drawing.Size(127, 20);
            this.labelInformation.TabIndex = 3;
            this.labelInformation.Text = "Wybierz tabelę";
            // 
            // buttonDelete
            // 
            this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDelete.Location = new System.Drawing.Point(652, 377);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(129, 41);
            this.buttonDelete.TabIndex = 5;
            this.buttonDelete.Text = "Usuń";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // Edit
            // 
            this.Edit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Edit.Location = new System.Drawing.Point(517, 377);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(129, 41);
            this.Edit.TabIndex = 6;
            this.Edit.Text = "Edytuj/Dodaj";
            this.Edit.UseVisualStyleBackColor = true;
            this.Edit.Click += new System.EventHandler(this.Edit_Click);
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSearch.Location = new System.Drawing.Point(203, 366);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(63, 20);
            this.labelSearch.TabIndex = 7;
            this.labelSearch.Text = "Szukaj";
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(207, 389);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(100, 20);
            this.textBoxSearch.TabIndex = 8;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearch_TextChanged);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelInfo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelInfo.Location = new System.Drawing.Point(325, 362);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(186, 180);
            this.labelInfo.TabIndex = 9;
            this.labelInfo.Text = resources.GetString("labelInfo.Text");
            this.labelInfo.Visible = false;
            // 
            // MusicClub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(793, 548);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.labelSearch);
            this.Controls.Add(this.Edit);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.labelInformation);
            this.Controls.Add(this.comboBoxSelectTable);
            this.Controls.Add(this.buttonShowAllRecords);
            this.Controls.Add(this.dataGridViewDatabase);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "MusicClub";
            this.Text = "MusicClub";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDatabase)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDatabase;
        private System.Windows.Forms.Button buttonShowAllRecords;
        private System.Windows.Forms.ComboBox comboBoxSelectTable;
        private System.Windows.Forms.Label labelInformation;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button Edit;
        private System.Windows.Forms.Label labelSearch;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Label labelInfo;
    }
}

