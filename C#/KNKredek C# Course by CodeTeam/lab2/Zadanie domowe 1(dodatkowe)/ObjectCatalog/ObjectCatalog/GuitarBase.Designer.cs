﻿namespace ObjectCatalog
{
    partial class ObjectCatalog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewObjectList = new System.Windows.Forms.DataGridView();
            this.buttonAddObject = new System.Windows.Forms.Button();
            this.buttonDeleteObject = new System.Windows.Forms.Button();
            this.buttonEditObject = new System.Windows.Forms.Button();
            this.buttonShowInfoObject = new System.Windows.Forms.Button();
            this.buttonReadFromFile = new System.Windows.Forms.Button();
            this.buttonSaveToFile = new System.Windows.Forms.Button();
            this.buttonCloseProgram = new System.Windows.Forms.Button();
            this.labelFormTitle = new System.Windows.Forms.Label();
            this.pictureBoxDetails = new System.Windows.Forms.PictureBox();
            this.textBoxDetails = new System.Windows.Forms.TextBox();
            this.labelDescriptionL = new System.Windows.Forms.Label();
            this.labelBase = new System.Windows.Forms.Label();
            this.labelDer = new System.Windows.Forms.Label();
            this.buttonDetailsChapman = new System.Windows.Forms.Button();
            this.buttonEditChapman = new System.Windows.Forms.Button();
            this.buttonRemoveChapman = new System.Windows.Forms.Button();
            this.buttonAddChapman = new System.Windows.Forms.Button();
            this.buttonShowBaseDer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewObjectList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewObjectList
            // 
            this.dataGridViewObjectList.BackgroundColor = System.Drawing.Color.Linen;
            this.dataGridViewObjectList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewObjectList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewObjectList.Location = new System.Drawing.Point(168, 75);
            this.dataGridViewObjectList.Name = "dataGridViewObjectList";
            this.dataGridViewObjectList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewObjectList.Size = new System.Drawing.Size(550, 400);
            this.dataGridViewObjectList.TabIndex = 0;
            // 
            // buttonAddObject
            // 
            this.buttonAddObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddObject.Location = new System.Drawing.Point(12, 75);
            this.buttonAddObject.Name = "buttonAddObject";
            this.buttonAddObject.Size = new System.Drawing.Size(150, 45);
            this.buttonAddObject.TabIndex = 1;
            this.buttonAddObject.Text = "Dodaj obiekt";
            this.buttonAddObject.UseVisualStyleBackColor = true;
            this.buttonAddObject.Click += new System.EventHandler(this.buttonAddObject_Click);
            // 
            // buttonDeleteObject
            // 
            this.buttonDeleteObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDeleteObject.Location = new System.Drawing.Point(12, 126);
            this.buttonDeleteObject.Name = "buttonDeleteObject";
            this.buttonDeleteObject.Size = new System.Drawing.Size(150, 45);
            this.buttonDeleteObject.TabIndex = 2;
            this.buttonDeleteObject.Text = "Usuń objekt";
            this.buttonDeleteObject.UseVisualStyleBackColor = true;
            this.buttonDeleteObject.Click += new System.EventHandler(this.buttonDeleteObject_Click);
            // 
            // buttonEditObject
            // 
            this.buttonEditObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEditObject.Location = new System.Drawing.Point(12, 177);
            this.buttonEditObject.Name = "buttonEditObject";
            this.buttonEditObject.Size = new System.Drawing.Size(150, 45);
            this.buttonEditObject.TabIndex = 3;
            this.buttonEditObject.Text = "Edytuj objekt";
            this.buttonEditObject.UseVisualStyleBackColor = true;
            this.buttonEditObject.Click += new System.EventHandler(this.buttonEditObject_Click);
            // 
            // buttonShowInfoObject
            // 
            this.buttonShowInfoObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowInfoObject.Location = new System.Drawing.Point(12, 228);
            this.buttonShowInfoObject.Name = "buttonShowInfoObject";
            this.buttonShowInfoObject.Size = new System.Drawing.Size(150, 45);
            this.buttonShowInfoObject.TabIndex = 4;
            this.buttonShowInfoObject.Text = "Pokaż informacje szczegółowe";
            this.buttonShowInfoObject.UseVisualStyleBackColor = true;
            this.buttonShowInfoObject.Click += new System.EventHandler(this.buttonShowInfoObject_Click);
            // 
            // buttonReadFromFile
            // 
            this.buttonReadFromFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonReadFromFile.Location = new System.Drawing.Point(168, 505);
            this.buttonReadFromFile.Name = "buttonReadFromFile";
            this.buttonReadFromFile.Size = new System.Drawing.Size(150, 45);
            this.buttonReadFromFile.TabIndex = 5;
            this.buttonReadFromFile.Text = "Wczytaj z pliku";
            this.buttonReadFromFile.UseVisualStyleBackColor = true;
            this.buttonReadFromFile.Click += new System.EventHandler(this.buttonReadFromFile_Click);
            // 
            // buttonSaveToFile
            // 
            this.buttonSaveToFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSaveToFile.Location = new System.Drawing.Point(324, 505);
            this.buttonSaveToFile.Name = "buttonSaveToFile";
            this.buttonSaveToFile.Size = new System.Drawing.Size(150, 45);
            this.buttonSaveToFile.TabIndex = 6;
            this.buttonSaveToFile.Text = "Zapisz do pliku";
            this.buttonSaveToFile.UseVisualStyleBackColor = true;
            this.buttonSaveToFile.Click += new System.EventHandler(this.buttonSaveToFile_Click);
            // 
            // buttonCloseProgram
            // 
            this.buttonCloseProgram.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCloseProgram.ForeColor = System.Drawing.Color.DarkRed;
            this.buttonCloseProgram.Location = new System.Drawing.Point(722, 505);
            this.buttonCloseProgram.Name = "buttonCloseProgram";
            this.buttonCloseProgram.Size = new System.Drawing.Size(150, 45);
            this.buttonCloseProgram.TabIndex = 7;
            this.buttonCloseProgram.Text = "Zakończ";
            this.buttonCloseProgram.UseVisualStyleBackColor = true;
            this.buttonCloseProgram.Click += new System.EventHandler(this.buttonCloseProgram_Click);
            // 
            // labelFormTitle
            // 
            this.labelFormTitle.AutoSize = true;
            this.labelFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelFormTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelFormTitle.Location = new System.Drawing.Point(285, 9);
            this.labelFormTitle.Name = "labelFormTitle";
            this.labelFormTitle.Size = new System.Drawing.Size(303, 46);
            this.labelFormTitle.TabIndex = 8;
            this.labelFormTitle.Text = "Katalog - gitary";
            // 
            // pictureBoxDetails
            // 
            this.pictureBoxDetails.Location = new System.Drawing.Point(737, 75);
            this.pictureBoxDetails.Name = "pictureBoxDetails";
            this.pictureBoxDetails.Size = new System.Drawing.Size(125, 267);
            this.pictureBoxDetails.TabIndex = 9;
            this.pictureBoxDetails.TabStop = false;
            this.pictureBoxDetails.Visible = false;
            // 
            // textBoxDetails
            // 
            this.textBoxDetails.Location = new System.Drawing.Point(724, 373);
            this.textBoxDetails.Multiline = true;
            this.textBoxDetails.Name = "textBoxDetails";
            this.textBoxDetails.Size = new System.Drawing.Size(166, 108);
            this.textBoxDetails.TabIndex = 10;
            this.textBoxDetails.Visible = false;
            // 
            // labelDescriptionL
            // 
            this.labelDescriptionL.AutoSize = true;
            this.labelDescriptionL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDescriptionL.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelDescriptionL.Location = new System.Drawing.Point(724, 345);
            this.labelDescriptionL.Name = "labelDescriptionL";
            this.labelDescriptionL.Size = new System.Drawing.Size(70, 25);
            this.labelDescriptionL.TabIndex = 11;
            this.labelDescriptionL.Text = "Opis: ";
            this.labelDescriptionL.Visible = false;
            // 
            // labelBase
            // 
            this.labelBase.AutoSize = true;
            this.labelBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBase.Location = new System.Drawing.Point(12, 48);
            this.labelBase.Name = "labelBase";
            this.labelBase.Size = new System.Drawing.Size(139, 24);
            this.labelBase.TabIndex = 12;
            this.labelBase.Text = "Gitara - bazowa";
            // 
            // labelDer
            // 
            this.labelDer.AutoSize = true;
            this.labelDer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDer.Location = new System.Drawing.Point(12, 289);
            this.labelDer.Name = "labelDer";
            this.labelDer.Size = new System.Drawing.Size(133, 24);
            this.labelDer.TabIndex = 17;
            this.labelDer.Text = "Chapman stick";
            // 
            // buttonDetailsChapman
            // 
            this.buttonDetailsChapman.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonDetailsChapman.Location = new System.Drawing.Point(12, 491);
            this.buttonDetailsChapman.Name = "buttonDetailsChapman";
            this.buttonDetailsChapman.Size = new System.Drawing.Size(150, 45);
            this.buttonDetailsChapman.TabIndex = 16;
            this.buttonDetailsChapman.Text = "Pokaż informacje szczegółowe";
            this.buttonDetailsChapman.UseVisualStyleBackColor = true;
            this.buttonDetailsChapman.Click += new System.EventHandler(this.buttonDetailsChapman_Click);
            // 
            // buttonEditChapman
            // 
            this.buttonEditChapman.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEditChapman.Location = new System.Drawing.Point(12, 440);
            this.buttonEditChapman.Name = "buttonEditChapman";
            this.buttonEditChapman.Size = new System.Drawing.Size(150, 45);
            this.buttonEditChapman.TabIndex = 15;
            this.buttonEditChapman.Text = "Edytuj objekt";
            this.buttonEditChapman.UseVisualStyleBackColor = true;
            this.buttonEditChapman.Click += new System.EventHandler(this.buttonEditChapman_Click);
            // 
            // buttonRemoveChapman
            // 
            this.buttonRemoveChapman.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRemoveChapman.Location = new System.Drawing.Point(12, 389);
            this.buttonRemoveChapman.Name = "buttonRemoveChapman";
            this.buttonRemoveChapman.Size = new System.Drawing.Size(150, 45);
            this.buttonRemoveChapman.TabIndex = 14;
            this.buttonRemoveChapman.Text = "Usuń objekt";
            this.buttonRemoveChapman.UseVisualStyleBackColor = true;
            this.buttonRemoveChapman.Click += new System.EventHandler(this.buttonRemoveChapman_Click);
            // 
            // buttonAddChapman
            // 
            this.buttonAddChapman.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonAddChapman.Location = new System.Drawing.Point(12, 338);
            this.buttonAddChapman.Name = "buttonAddChapman";
            this.buttonAddChapman.Size = new System.Drawing.Size(150, 45);
            this.buttonAddChapman.TabIndex = 13;
            this.buttonAddChapman.Text = "Dodaj obiekt";
            this.buttonAddChapman.UseVisualStyleBackColor = true;
            this.buttonAddChapman.Click += new System.EventHandler(this.buttonAddChapman_Click);
            // 
            // buttonShowBaseDer
            // 
            this.buttonShowBaseDer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonShowBaseDer.Location = new System.Drawing.Point(522, 505);
            this.buttonShowBaseDer.Name = "buttonShowBaseDer";
            this.buttonShowBaseDer.Size = new System.Drawing.Size(150, 45);
            this.buttonShowBaseDer.TabIndex = 18;
            this.buttonShowBaseDer.Text = "Pokaż pochodne";
            this.buttonShowBaseDer.UseVisualStyleBackColor = true;
            this.buttonShowBaseDer.Click += new System.EventHandler(this.buttonShowBaseDer_Click);
            // 
            // ObjectCatalog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.ClientSize = new System.Drawing.Size(934, 562);
            this.Controls.Add(this.buttonShowBaseDer);
            this.Controls.Add(this.labelDer);
            this.Controls.Add(this.buttonDetailsChapman);
            this.Controls.Add(this.buttonEditChapman);
            this.Controls.Add(this.buttonRemoveChapman);
            this.Controls.Add(this.buttonAddChapman);
            this.Controls.Add(this.labelBase);
            this.Controls.Add(this.labelDescriptionL);
            this.Controls.Add(this.textBoxDetails);
            this.Controls.Add(this.pictureBoxDetails);
            this.Controls.Add(this.labelFormTitle);
            this.Controls.Add(this.buttonCloseProgram);
            this.Controls.Add(this.buttonSaveToFile);
            this.Controls.Add(this.buttonReadFromFile);
            this.Controls.Add(this.buttonShowInfoObject);
            this.Controls.Add(this.buttonEditObject);
            this.Controls.Add(this.buttonDeleteObject);
            this.Controls.Add(this.buttonAddObject);
            this.Controls.Add(this.dataGridViewObjectList);
            this.Name = "ObjectCatalog";
            this.Text = "Katalog Obiektów";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewObjectList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewObjectList;
        private System.Windows.Forms.Button buttonAddObject;
        private System.Windows.Forms.Button buttonDeleteObject;
        private System.Windows.Forms.Button buttonEditObject;
        private System.Windows.Forms.Button buttonShowInfoObject;
        private System.Windows.Forms.Button buttonReadFromFile;
        private System.Windows.Forms.Button buttonSaveToFile;
        private System.Windows.Forms.Button buttonCloseProgram;
        private System.Windows.Forms.Label labelFormTitle;
        private System.Windows.Forms.PictureBox pictureBoxDetails;
        private System.Windows.Forms.TextBox textBoxDetails;
        private System.Windows.Forms.Label labelDescriptionL;
        private System.Windows.Forms.Label labelBase;
        private System.Windows.Forms.Label labelDer;
        private System.Windows.Forms.Button buttonDetailsChapman;
        private System.Windows.Forms.Button buttonEditChapman;
        private System.Windows.Forms.Button buttonRemoveChapman;
        private System.Windows.Forms.Button buttonAddChapman;
        private System.Windows.Forms.Button buttonShowBaseDer;
    }
}

