﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2
{
    public partial class Pracownicy : Form
    {

        List<Worker> listOfWorkers;
        List<WorkerOfMonth> listOfWorkersOfMonth;

        public Pracownicy()
        {
            InitializeComponent();
            listOfWorkers = new List<Worker>();
            listOfWorkersOfMonth = new List<WorkerOfMonth>();
        }

        /// <summary>
        /// Creates new worker and adds to array of workers
        /// </summary>
        /// <param name="name"></param>
        /// <param name="salary"></param>
        private void CreateWorker(string name, string salary)
        {
            if (name.Length > 0 && salary.Length > 0)
            {
                float salaryFloat = 0;
                float.TryParse(salary, out salaryFloat);
                Worker worker = new Worker(name, salaryFloat);
                listOfWorkers.Add(worker);
            }
        }

        /// <summary>
        ///  Creates object superWorker, displays into textbox - this fun does not store created objects
        /// </summary>
        /// <param name="name"></param>
        /// <param name="salary"></param>
        /// <param name="handAmount"></param>
        private void CreateSuperWorker(string name, string salary,string handAmount)
        {
            if (name.Length > 0 && salary.Length > 0  && handAmount.Length > 0)
            {
                int handInt = 0;
                int.TryParse(handAmount, out handInt);
                float salaryFloat;
                float.TryParse(salary, out salaryFloat);
                SuperWorker superWorker = new SuperWorker(name, salaryFloat, handInt);
                ShowSuperworker(superWorker);
            }

        }

        /// <summary>
        /// Create workerOfMonth and store into list
        /// </summary>
        /// <param name="name"></param>
        /// <param name="prize"></param>
        /// <param name="handShake"></param>
        private void CreateWorkerOfMonth(string name, string prize, bool handShake)
        {
            if(name.Length > 0 && prize.Length > 0 )
            {
                int prizeInt = 0;
                int.TryParse(prize, out prizeInt);
                WorkerOfMonth workerOfMonth = new WorkerOfMonth(name, prizeInt, handShake);
                listOfWorkersOfMonth.Add(workerOfMonth);
            }

        }

        /// <summary>
        /// Prints list of workers into textbox
        /// </summary>
        private void ShowWorkers()
        {
            textBoxListOfWorkers.Text = "";
            for(int i = 0;  i < listOfWorkers.Count(); i++)
            {
                textBoxListOfWorkers.AppendText(listOfWorkers[i].GetName()+ " zarabia: " + listOfWorkers[i].GetSalary() + Environment.NewLine);
            }
        }

        /// <summary>
        /// displays info from forwarded object superWorker
        /// </summary>
        /// <param name="worker"></param>
        private void ShowSuperworker(SuperWorker worker)
        {
            textBoxListOfWorkers.Text = "";

                textBoxListOfWorkers.AppendText(worker.GetName() + " zarabia: " + worker.GetSalary() + " i ma: " + worker.GetHandAmount() + " rąk." + Environment.NewLine);
        }

        /// <summary>
        /// Displays list of objects workerOfMonth in dataGridView
        /// </summary>
        private void ShowWorkerOfMontInDataGView()
        {
            if(listOfWorkersOfMonth.Count > 0) // avoid updating gridView if list is empty!
            {
                RefreshDataGridView();
                dataGridViewWorkerOfMonth.DataSource = listOfWorkersOfMonth;
            }     
        }

        /// <summary>
        /// Point existing dataGridView to null so new data can be loaded # DO NOT use before processing this array!!!
        /// </summary>
        private void RefreshDataGridView()
        {
            dataGridViewWorkerOfMonth.DataSource = null;
        }

        private void RemoveCheckedWorkerOfMonth()
        {

            
            try
            {
                if (dataGridViewWorkerOfMonth.SelectedRows.Count > 1)
                {
              
                    int selectedCount = dataGridViewWorkerOfMonth.SelectedRows.Count;
                    for(int index = 0; index < selectedCount; index++)
                    {
                        listOfWorkersOfMonth.RemoveAt(dataGridViewWorkerOfMonth.SelectedRows[0].Index); // this gets smaller <- point to first element!!
                    }
                    RefreshDataGridView();  // <- here because if before dataGridView would point to null!
                    ShowWorkerOfMontInDataGView();
                }
                else
                {
                    int index = dataGridViewWorkerOfMonth.SelectedRows[0].Index;
                    listOfWorkersOfMonth.RemoveAt(index);
                    dataGridViewWorkerOfMonth.Update();
                    RefreshDataGridView();  // <- here because if before dataGridView would point to null!
                    ShowWorkerOfMontInDataGView();
                }

            }
            catch (Exception)
            {

            }

            
        }

        /// <summary>
        /// Take all workersOfMonth and put sum of prizes into labelSumOfPrizes
        /// </summary>
        private void PrintSumOfPrizes()
        {
            float tempPrizes = 0;
            for (int i = 0; i < listOfWorkersOfMonth.Count; i++)
            {
                tempPrizes += listOfWorkersOfMonth[i].Prize;
            }
            labelSumOfPrizes.Text = "Suma nagród: " + tempPrizes.ToString();
        }

        private void buttonCreateWorker_Click(object sender, EventArgs e)
        {
            CreateWorker(textBoxName.Text, textBoxSalary.Text);
        }

        private void buttonShowWorkers_Click(object sender, EventArgs e)
        {
            ShowWorkers();
        }

        private void buttonSuperWorkerCreate_Click(object sender, EventArgs e)
        {
            CreateSuperWorker(textBoxName.Text, textBoxSalary.Text, textBoxHandAmount.Text);
            
        }

        private void buttonAddWorkerOfMonth_Click(object sender, EventArgs e)
        {
            
            CreateWorkerOfMonth(textBoxName.Text, textBoxPrize.Text, checkBoxHandshake.Checked);
            ShowWorkerOfMontInDataGView();

        }

        private void buttonRemoveWorkerOfMonth_Click(object sender, EventArgs e)
        {
            RemoveCheckedWorkerOfMonth();
        }


        private void buttonSummarizePrizes_Click(object sender, EventArgs e)
        {
            PrintSumOfPrizes();
        }

    }

    



}
