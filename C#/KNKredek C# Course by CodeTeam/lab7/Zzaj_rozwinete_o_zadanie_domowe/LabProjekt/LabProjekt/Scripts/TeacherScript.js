﻿var appNd = angular.module("myTeacherApp", [])

appNd.controller("teacherController",function($scope)
{
    $scope.display = true;

    $scope.goToIndex = function () {
        window.location.replace("/Home/Index/")
    }

    var Teacher1 = {
        name: 'Ryszard',
        surname: 'Kalisz',
        degree: 'PhD'
    }

    var Teacher2 = {
        name: 'Jan',
        surname: 'Miodek',
        degree: 'Professor'
    }

    var Teacher3 = {
        name: 'Jerzy',
        surname: 'Przyjacielmlodziezy',
        degree: 'MAGISTER'
    }

    $scope.teachers = [Teacher1,Teacher2,Teacher3]

})