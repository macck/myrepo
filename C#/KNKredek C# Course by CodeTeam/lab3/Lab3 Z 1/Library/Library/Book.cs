﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    class Book
    {
        private static SqlDataAdapter sqlDataAdapter;


        // Func filling dataGridView (commonly used)
        private static void FillDataGridViewLib(SqlDataAdapter dataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null; // clear
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;

        }


        public static void ShowAllBooks(SqlConnection sqlCon, DataGridView dataGridViewLib)
        {
            sqlDataAdapter = new SqlDataAdapter("SELECT * FROM Books", sqlCon);
            FillDataGridViewLib(sqlDataAdapter, dataGridViewLib);
        }

        public static void SearchByTitle(SqlConnection sqlCon, DataGridView dataGridViewLib, string title)
        {
            sqlDataAdapter = new SqlDataAdapter("SELECT * FROM Books WHERE Title  LIKE" + "'" + title + "%'", sqlCon);
            FillDataGridViewLib(sqlDataAdapter, dataGridViewLib);
        }

        public static void SearchByYear(SqlConnection sqlCon, DataGridView dataGridViewLib, string dateFrom, string dateTo)
        {
            int tmp;
            if(dateFrom != "" && dateTo != "" && int.TryParse(dateFrom, out tmp) && int.TryParse(dateTo, out tmp)) // to prevent from entering char's other than numbers
            {
                sqlDataAdapter = new SqlDataAdapter("SELECT * FROM Books WHERE Year Between " + dateFrom + " AND " + dateTo, sqlCon);
                FillDataGridViewLib(sqlDataAdapter, dataGridViewLib);
            }

        }
    }
}
