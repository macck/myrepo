﻿using ApiDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;

namespace ApiDatabase.Controllers
{
    [RoutePrefix("api/users")]
    public class UserController : ApiController
    {

        [HttpGet,Route("")]
        [ResponseType(typeof(IEnumerable<User>))]
        public IHttpActionResult Get()
        {
            IEnumerable<User> users;

            using (var ctx = new ContextClass())
            {
                users = ctx.Users.Include(x => x.Orders).ToList();
            }

            return Ok(users);

        }



        // GET: api/User/5
        [HttpGet,Route("{id:int}", Name = "GetUser")]
        [ResponseType(typeof(User))]
        public IHttpActionResult Get(int id)
        {
            User user;
            using (var ctx = new ContextClass())
            {
                user = ctx.Users.Include(x => x.Orders).SingleOrDefault(s => s.Id == id);

            }
            return Ok(user);
        }

        // POST: api/User
        [HttpPost, Route("")]
        public IHttpActionResult Post([FromBody]User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var ctx = new ContextClass())
            {
                ctx.Users.Add(user);
                ctx.SaveChanges();
            }

            return CreatedAtRoute("GetUser", new { id = user.Id }, user);
        }

        [HttpPut, Route("{id:int}")]
        public IHttpActionResult Update(int id, [FromBody]User user)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var ctx = new ContextClass())
            {
                user.Id = id;
                ctx.Users.Attach(user);
                var dbEntry = ctx.Entry(user);
                dbEntry.State = EntityState.Modified;

                if(user.Orders != null)
                {
                    foreach (var order in user.Orders)
                    {
                        ctx.Orders.Attach(order);
                        var entry = ctx.Entry(order);
                        entry.State = order.Id == 0 ? EntityState.Added :
                        EntityState.Modified;
                    }
                }

                ctx.SaveChanges();
            }
            return Ok(user);
        }

        [HttpDelete, Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            using (var ctx = new ContextClass())
            {
                var dbEntry = ctx.Users.Find(id);
                if(dbEntry == null)
                {
                    return NotFound();
                }
                else
                {

                    ctx.Users.Remove(dbEntry);
                    ctx.SaveChanges();
                }
                return Ok();
            }
        }
    }
}
