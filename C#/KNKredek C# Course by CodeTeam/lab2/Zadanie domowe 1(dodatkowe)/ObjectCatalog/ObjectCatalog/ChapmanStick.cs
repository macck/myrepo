﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectCatalog
{
    [Serializable]
    public class ChapmanStick : Guitar
    {
        public int StrunyBasowe { get; set; }
        public int StrunyMelodyczne { get; set; }

        public ChapmanStick(string manufacturer, string model, int strings, string typeOfGuitar, string color, int basStrings, int melodicStrings) : base(manufacturer,  model, strings,  typeOfGuitar,  color)
        {
            this.StrunyBasowe = basStrings;
            this.StrunyMelodyczne = melodicStrings;
        }
    }
}
