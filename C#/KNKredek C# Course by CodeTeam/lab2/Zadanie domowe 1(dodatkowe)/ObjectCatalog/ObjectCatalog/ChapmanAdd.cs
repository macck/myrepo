﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectCatalog
{
    public partial class ChapmanAdd : Form
    {
        public ChapmanAdd()
        {
            InitializeComponent();
            pictureBoxGuitarPic.SizeMode = PictureBoxSizeMode.StretchImage;
            result = DialogResult.None;
        }

        public ChapmanAdd(List<ChapmanStick> lista)
        {
            InitializeComponent();
            pictureBoxGuitarPic.SizeMode = PictureBoxSizeMode.StretchImage;
            result = DialogResult.None;
            this.lista = lista;
        }
        private List<ChapmanStick> lista;
        private ChapmanStick retObjChapman;
        public DialogResult result;
        private string pathToImage = "";



        /// <summary>
        /// Returns path to opened file, if failed returns empty string ("")
        /// </summary>
        private string OpenFile()
        {
            string fileName = "";
            OpenFileDialog openFileGuitars = new OpenFileDialog();
            DialogResult result = openFileGuitars.ShowDialog();
            if (result == DialogResult.OK)
            {
                if (openFileGuitars.FileName.EndsWith(".png") || openFileGuitars.FileName.EndsWith(".jpg") || openFileGuitars.FileName.EndsWith(".jpeg"))
                {
                    pictureBoxGuitarPic.Image = Image.FromFile(openFileGuitars.FileName);
                    fileName = openFileGuitars.FileName;
                }

            }

            return fileName;
        }


        /// <summary>
        /// creates new obj. guitar and pushes it into list
        /// </summary>
        private void AddChapman()
        {
            if (textBoxManufacturer.Text != "" && textBoxModel.Text != "" && textBoxColor.Text != "")
            {
                int stringCount = 0;
                int.TryParse(listBoxStringCount.Text, out stringCount);
                int bassStringCount = 0;
                int melodicStrinCount = 0;
                int.TryParse(listBoxBassStrings.Text, out bassStringCount);
                int.TryParse(listBoxMelodicStrings.Text, out melodicStrinCount);

                retObjChapman = new ChapmanStick(textBoxManufacturer.Text, textBoxModel.Text, stringCount, listBoxType.Text, textBoxColor.Text, bassStringCount,melodicStrinCount);
                if (pathToImage != "")
                {
                    retObjChapman.SetPathToImage(pathToImage);
                }
                retObjChapman.SetDescription(textBoxDescription.Text);
                result = DialogResult.OK;
            }

            lista.Add(retObjChapman);
        }


        private void buttonAddImage_Click(object sender, EventArgs e)
        {
            pathToImage = OpenFile();
        }

        private void buttonAddGuitar_Click(object sender, EventArgs e)
        {
            AddChapman();
            this.Close();
        }

        private void buttonAddClose_Click(object sender, EventArgs e)
        {
           this.Close();
        }
    }
}
