USE [master]
GO
/****** Object:  Database [Library]    Script Date: 2016-12-11 18:49:26 ******/
CREATE DATABASE [Library]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Library', FILENAME = N'I:\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Library.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Library_log', FILENAME = N'I:\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Library_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Library] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Library].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Library] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Library] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Library] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Library] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Library] SET ARITHABORT OFF 
GO
ALTER DATABASE [Library] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Library] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Library] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Library] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Library] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Library] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Library] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Library] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Library] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Library] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Library] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Library] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Library] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Library] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Library] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Library] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Library] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Library] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Library] SET  MULTI_USER 
GO
ALTER DATABASE [Library] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Library] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Library] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Library] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Library] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Library]
GO
/****** Object:  Table [dbo].[Books]    Script Date: 2016-12-11 18:49:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Books](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Author] [nvarchar](50) NOT NULL,
	[Year] [int] NOT NULL,
 CONSTRAINT [PK_Books] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Borrowings]    Script Date: 2016-12-11 18:49:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Borrowings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[BookID] [int] NOT NULL,
	[DateOfBorrowing] [date] NOT NULL,
	[DateOfReturn] [date] NULL,
 CONSTRAINT [PK_Borrowings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 2016-12-11 18:49:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Books] ON 

INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (1, N'C++', N'Bjarne Straustrup', 2009)
INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (3, N'Teoria sygnałów', N'Jerzy Szabatin', 2000)
INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (4, N'Czas pogardy', N'Andrzej Sapkowski ', 1995)
INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (7, N'Władca pierścieni', N'J.R.R Tolkien', 1955)
INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (8, N'Dziady', N'Adam Mickiewicz', 1860)
INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (9, N'Kordian', N'Juliusz Słowacki', 1834)
INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (10, N'Dzień Gniewu', N'Dies Irae', 1754)
INSERT [dbo].[Books] ([ID], [Title], [Author], [Year]) VALUES (11, N'Testament mój', N'Juliusz Słowacki ', 1840)
SET IDENTITY_INSERT [dbo].[Books] OFF
SET IDENTITY_INSERT [dbo].[Borrowings] ON 

INSERT [dbo].[Borrowings] ([ID], [UserID], [BookID], [DateOfBorrowing], [DateOfReturn]) VALUES (1, 2, 1, CAST(N'2016-12-10' AS Date), NULL)
INSERT [dbo].[Borrowings] ([ID], [UserID], [BookID], [DateOfBorrowing], [DateOfReturn]) VALUES (2, 3, 4, CAST(N'2016-10-11' AS Date), CAST(N'2016-11-13' AS Date))
INSERT [dbo].[Borrowings] ([ID], [UserID], [BookID], [DateOfBorrowing], [DateOfReturn]) VALUES (3, 1, 3, CAST(N'2016-11-11' AS Date), CAST(N'2016-12-05' AS Date))
INSERT [dbo].[Borrowings] ([ID], [UserID], [BookID], [DateOfBorrowing], [DateOfReturn]) VALUES (9, 1, 7, CAST(N'2016-07-13' AS Date), CAST(N'2016-08-14' AS Date))
SET IDENTITY_INSERT [dbo].[Borrowings] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (1, N'Maciej', N'Majka')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (2, N'Jan', N'Kowalski')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (3, N'Grzegorz', N'Brzęczyszczykiewicz')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (4, N'Pai', N'Mei')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (5, N'Beatrix', N'Kiddo')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (6, N'Joe', N'Satriani')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (7, N'John ', N'Petrucci')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (8, N'Bartosz', N'Mąderek')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (9, N'Stanisław', N'Mąderek')
INSERT [dbo].[Users] ([ID], [Name], [Surname]) VALUES (10, N'Jan', N'Twardowski')
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[Borrowings]  WITH CHECK ADD  CONSTRAINT [FK_Borrowings_Books] FOREIGN KEY([BookID])
REFERENCES [dbo].[Books] ([ID])
GO
ALTER TABLE [dbo].[Borrowings] CHECK CONSTRAINT [FK_Borrowings_Books]
GO
ALTER TABLE [dbo].[Borrowings]  WITH CHECK ADD  CONSTRAINT [FK_Borrowings_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Borrowings] CHECK CONSTRAINT [FK_Borrowings_User]
GO
USE [master]
GO
ALTER DATABASE [Library] SET  READ_WRITE 
GO
