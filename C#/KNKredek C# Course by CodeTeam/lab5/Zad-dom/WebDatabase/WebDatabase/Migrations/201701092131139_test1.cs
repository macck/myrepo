namespace WebDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductOrders", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.ProductOrders", "Order_Id", "dbo.Orders");
            DropIndex("dbo.ProductOrders", new[] { "Product_Id" });
            DropIndex("dbo.ProductOrders", new[] { "Order_Id" });
            DropPrimaryKey("dbo.ProductOrders");
            AddColumn("dbo.ProductOrders", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.ProductOrders", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.ProductOrders", "Order_Id", c => c.Int());
            AddPrimaryKey("dbo.ProductOrders", "Id");
            CreateIndex("dbo.ProductOrders", "Order_Id");
            AddForeignKey("dbo.ProductOrders", "Order_Id", "dbo.Orders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductOrders", "Order_Id", "dbo.Orders");
            DropIndex("dbo.ProductOrders", new[] { "Order_Id" });
            DropPrimaryKey("dbo.ProductOrders");
            AlterColumn("dbo.ProductOrders", "Order_Id", c => c.Int(nullable: false));
            DropColumn("dbo.ProductOrders", "Quantity");
            DropColumn("dbo.ProductOrders", "Id");
            AddPrimaryKey("dbo.ProductOrders", new[] { "Product_Id", "Order_Id" });
            CreateIndex("dbo.ProductOrders", "Order_Id");
            CreateIndex("dbo.ProductOrders", "Product_Id");
            AddForeignKey("dbo.ProductOrders", "Order_Id", "dbo.Orders", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ProductOrders", "Product_Id", "dbo.Products", "Id", cascadeDelete: true);
        }
    }
}
