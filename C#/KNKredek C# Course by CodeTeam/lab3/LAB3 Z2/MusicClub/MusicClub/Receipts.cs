﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace MusicClub
{
    // To akurat (baza paragonów) nie jest zbyt mądre ale na potrzeby zadania będzie okej
    class Receipts : DataBaseTable 
    {
  
        public override SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            databaseQuery = "SELECT Receipts.ID, Guests.Name AS 'Imię', Guests.Surname AS 'Nazwisko', Drinks.Name AS 'Drink', Drinks.Price AS 'Cena', Receipts.Amount AS 'Ilość'  FROM Receipts JOIN Guests on GuestID=Guests.ID JOIN Drinks on DrinkID=Drinks.ID";
            return base.ShowRecords(sqlCon, dataGridView, databaseQuery);
        }

        public override void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            string databaseQuery = "SELECT * FROM Receipts WHERE Amount LIKE" + "'" + text + "%'";
            base.SearchBy(sqlCon, dataGridViewDatabase, databaseQuery);
        }
    }
}
