#ifndef PROWADZACY_H_INCLUDED
#define PROWADZACY_H_INCLUDED
#include <vector>
#include "karta.h"
#include "gracz.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <algorithm>

class Prowadzacy{


    Gracz & g1;
    Gracz & g2;
    Gracz & g3;
    Gracz & g4;
    int AtutCardColor;

std::vector<Karta> Talia;

public:

    Prowadzacy(Gracz & G1,Gracz & G2,Gracz & G3, Gracz & G4);
    void ShuffleCards();
    void CreateHands(); // tworzy pelna talie kart
    void PassToPlayers(); // rozdziela talie miedzy graczy
    void RaffleAtutColor(); // losuje kolor atutowy
    std::string DisplayAtut();
    void PlayerAskForCard(int PlayerIndex,int StColor,int AtutColor);
    void MatchPlay(); // przeprowadza rozgrywk� trwaj�c� 13 rund
    void WhatsInVector();



};




#endif // PROWADZACY_H_INCLUDED
