﻿namespace lab2
{
    partial class Pracownicy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNameSurname = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSalary = new System.Windows.Forms.TextBox();
            this.buttonCreateWorker = new System.Windows.Forms.Button();
            this.textBoxListOfWorkers = new System.Windows.Forms.TextBox();
            this.labelPosition = new System.Windows.Forms.Label();
            this.labelSalary = new System.Windows.Forms.Label();
            this.buttonShowWorkers = new System.Windows.Forms.Button();
            this.textBoxHandAmount = new System.Windows.Forms.TextBox();
            this.buttonSuperWorkerCreate = new System.Windows.Forms.Button();
            this.labelHandAmount = new System.Windows.Forms.Label();
            this.dataGridViewWorkerOfMonth = new System.Windows.Forms.DataGridView();
            this.buttonAddWorkerOfMonth = new System.Windows.Forms.Button();
            this.textBoxPrize = new System.Windows.Forms.TextBox();
            this.labelPrize = new System.Windows.Forms.Label();
            this.checkBoxHandshake = new System.Windows.Forms.CheckBox();
            this.buttonRemoveWorkerOfMonth = new System.Windows.Forms.Button();
            this.buttonSummarizePrizes = new System.Windows.Forms.Button();
            this.labelSumOfPrizes = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkerOfMonth)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNameSurname
            // 
            this.labelNameSurname.AutoSize = true;
            this.labelNameSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNameSurname.Location = new System.Drawing.Point(0, 0);
            this.labelNameSurname.Name = "labelNameSurname";
            this.labelNameSurname.Size = new System.Drawing.Size(171, 31);
            this.labelNameSurname.TabIndex = 0;
            this.labelNameSurname.Text = "Maciej Majka";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(5, 34);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 1;
            // 
            // textBoxSalary
            // 
            this.textBoxSalary.Location = new System.Drawing.Point(5, 58);
            this.textBoxSalary.Name = "textBoxSalary";
            this.textBoxSalary.Size = new System.Drawing.Size(100, 20);
            this.textBoxSalary.TabIndex = 2;
            // 
            // buttonCreateWorker
            // 
            this.buttonCreateWorker.Location = new System.Drawing.Point(4, 193);
            this.buttonCreateWorker.Name = "buttonCreateWorker";
            this.buttonCreateWorker.Size = new System.Drawing.Size(100, 41);
            this.buttonCreateWorker.TabIndex = 3;
            this.buttonCreateWorker.Text = "Stworz pracownika";
            this.buttonCreateWorker.UseVisualStyleBackColor = true;
            this.buttonCreateWorker.Click += new System.EventHandler(this.buttonCreateWorker_Click);
            // 
            // textBoxListOfWorkers
            // 
            this.textBoxListOfWorkers.Location = new System.Drawing.Point(228, 6);
            this.textBoxListOfWorkers.Multiline = true;
            this.textBoxListOfWorkers.Name = "textBoxListOfWorkers";
            this.textBoxListOfWorkers.Size = new System.Drawing.Size(196, 244);
            this.textBoxListOfWorkers.TabIndex = 4;
            // 
            // labelPosition
            // 
            this.labelPosition.AutoSize = true;
            this.labelPosition.Location = new System.Drawing.Point(111, 37);
            this.labelPosition.Name = "labelPosition";
            this.labelPosition.Size = new System.Drawing.Size(96, 13);
            this.labelPosition.TabIndex = 5;
            this.labelPosition.Text = "Nazwa stanowiska";
            // 
            // labelSalary
            // 
            this.labelSalary.AutoSize = true;
            this.labelSalary.Location = new System.Drawing.Point(111, 61);
            this.labelSalary.Name = "labelSalary";
            this.labelSalary.Size = new System.Drawing.Size(39, 13);
            this.labelSalary.TabIndex = 6;
            this.labelSalary.Text = "Pensja";
            // 
            // buttonShowWorkers
            // 
            this.buttonShowWorkers.Location = new System.Drawing.Point(264, 256);
            this.buttonShowWorkers.Name = "buttonShowWorkers";
            this.buttonShowWorkers.Size = new System.Drawing.Size(99, 41);
            this.buttonShowWorkers.TabIndex = 7;
            this.buttonShowWorkers.Text = "Pokaz pracownikow";
            this.buttonShowWorkers.UseVisualStyleBackColor = true;
            this.buttonShowWorkers.Click += new System.EventHandler(this.buttonShowWorkers_Click);
            // 
            // textBoxHandAmount
            // 
            this.textBoxHandAmount.Location = new System.Drawing.Point(5, 84);
            this.textBoxHandAmount.Name = "textBoxHandAmount";
            this.textBoxHandAmount.Size = new System.Drawing.Size(100, 20);
            this.textBoxHandAmount.TabIndex = 8;
            // 
            // buttonSuperWorkerCreate
            // 
            this.buttonSuperWorkerCreate.Location = new System.Drawing.Point(5, 256);
            this.buttonSuperWorkerCreate.Name = "buttonSuperWorkerCreate";
            this.buttonSuperWorkerCreate.Size = new System.Drawing.Size(99, 41);
            this.buttonSuperWorkerCreate.TabIndex = 9;
            this.buttonSuperWorkerCreate.Text = "Stwórz superpracownika";
            this.buttonSuperWorkerCreate.UseVisualStyleBackColor = true;
            this.buttonSuperWorkerCreate.Click += new System.EventHandler(this.buttonSuperWorkerCreate_Click);
            // 
            // labelHandAmount
            // 
            this.labelHandAmount.AutoSize = true;
            this.labelHandAmount.Location = new System.Drawing.Point(111, 87);
            this.labelHandAmount.Name = "labelHandAmount";
            this.labelHandAmount.Size = new System.Drawing.Size(56, 13);
            this.labelHandAmount.TabIndex = 10;
            this.labelHandAmount.Text = "Liczba rąk";
            // 
            // dataGridViewWorkerOfMonth
            // 
            this.dataGridViewWorkerOfMonth.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWorkerOfMonth.Location = new System.Drawing.Point(430, 12);
            this.dataGridViewWorkerOfMonth.Name = "dataGridViewWorkerOfMonth";
            this.dataGridViewWorkerOfMonth.Size = new System.Drawing.Size(362, 238);
            this.dataGridViewWorkerOfMonth.TabIndex = 11;
            // 
            // buttonAddWorkerOfMonth
            // 
            this.buttonAddWorkerOfMonth.Location = new System.Drawing.Point(114, 256);
            this.buttonAddWorkerOfMonth.Name = "buttonAddWorkerOfMonth";
            this.buttonAddWorkerOfMonth.Size = new System.Drawing.Size(100, 41);
            this.buttonAddWorkerOfMonth.TabIndex = 12;
            this.buttonAddWorkerOfMonth.Text = "Pracownik miesiąca";
            this.buttonAddWorkerOfMonth.UseVisualStyleBackColor = true;
            this.buttonAddWorkerOfMonth.Click += new System.EventHandler(this.buttonAddWorkerOfMonth_Click);
            // 
            // textBoxPrize
            // 
            this.textBoxPrize.Location = new System.Drawing.Point(6, 110);
            this.textBoxPrize.Name = "textBoxPrize";
            this.textBoxPrize.Size = new System.Drawing.Size(100, 20);
            this.textBoxPrize.TabIndex = 13;
            // 
            // labelPrize
            // 
            this.labelPrize.AutoSize = true;
            this.labelPrize.Location = new System.Drawing.Point(111, 113);
            this.labelPrize.Name = "labelPrize";
            this.labelPrize.Size = new System.Drawing.Size(48, 13);
            this.labelPrize.TabIndex = 15;
            this.labelPrize.Text = "Nagroda";
            // 
            // checkBoxHandshake
            // 
            this.checkBoxHandshake.AutoSize = true;
            this.checkBoxHandshake.Location = new System.Drawing.Point(6, 138);
            this.checkBoxHandshake.Name = "checkBoxHandshake";
            this.checkBoxHandshake.Size = new System.Drawing.Size(116, 17);
            this.checkBoxHandshake.TabIndex = 17;
            this.checkBoxHandshake.Text = "Uścisk dłoni szefa ";
            this.checkBoxHandshake.UseVisualStyleBackColor = true;
            // 
            // buttonRemoveWorkerOfMonth
            // 
            this.buttonRemoveWorkerOfMonth.Location = new System.Drawing.Point(114, 193);
            this.buttonRemoveWorkerOfMonth.Name = "buttonRemoveWorkerOfMonth";
            this.buttonRemoveWorkerOfMonth.Size = new System.Drawing.Size(100, 41);
            this.buttonRemoveWorkerOfMonth.TabIndex = 18;
            this.buttonRemoveWorkerOfMonth.Text = "Usuń zaznaczone";
            this.buttonRemoveWorkerOfMonth.UseVisualStyleBackColor = true;
            this.buttonRemoveWorkerOfMonth.Click += new System.EventHandler(this.buttonRemoveWorkerOfMonth_Click);
            // 
            // buttonSummarizePrizes
            // 
            this.buttonSummarizePrizes.Location = new System.Drawing.Point(430, 256);
            this.buttonSummarizePrizes.Name = "buttonSummarizePrizes";
            this.buttonSummarizePrizes.Size = new System.Drawing.Size(100, 41);
            this.buttonSummarizePrizes.TabIndex = 19;
            this.buttonSummarizePrizes.Text = "Podlicz nagrody";
            this.buttonSummarizePrizes.UseVisualStyleBackColor = true;
            this.buttonSummarizePrizes.Click += new System.EventHandler(this.buttonSummarizePrizes_Click);
            // 
            // labelSumOfPrizes
            // 
            this.labelSumOfPrizes.AutoSize = true;
            this.labelSumOfPrizes.Location = new System.Drawing.Point(536, 270);
            this.labelSumOfPrizes.Name = "labelSumOfPrizes";
            this.labelSumOfPrizes.Size = new System.Drawing.Size(76, 13);
            this.labelSumOfPrizes.TabIndex = 20;
            this.labelSumOfPrizes.Text = "Suma nagród: ";
            // 
            // FormDoZmiany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(799, 302);
            this.Controls.Add(this.labelSumOfPrizes);
            this.Controls.Add(this.buttonSummarizePrizes);
            this.Controls.Add(this.buttonRemoveWorkerOfMonth);
            this.Controls.Add(this.checkBoxHandshake);
            this.Controls.Add(this.labelPrize);
            this.Controls.Add(this.textBoxPrize);
            this.Controls.Add(this.buttonAddWorkerOfMonth);
            this.Controls.Add(this.dataGridViewWorkerOfMonth);
            this.Controls.Add(this.labelHandAmount);
            this.Controls.Add(this.buttonSuperWorkerCreate);
            this.Controls.Add(this.textBoxHandAmount);
            this.Controls.Add(this.buttonShowWorkers);
            this.Controls.Add(this.labelSalary);
            this.Controls.Add(this.labelPosition);
            this.Controls.Add(this.textBoxListOfWorkers);
            this.Controls.Add(this.buttonCreateWorker);
            this.Controls.Add(this.textBoxSalary);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelNameSurname);
            this.Name = "FormDoZmiany";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkerOfMonth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNameSurname;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSalary;
        private System.Windows.Forms.Button buttonCreateWorker;
        private System.Windows.Forms.TextBox textBoxListOfWorkers;
        private System.Windows.Forms.Label labelPosition;
        private System.Windows.Forms.Label labelSalary;
        private System.Windows.Forms.Button buttonShowWorkers;
        private System.Windows.Forms.TextBox textBoxHandAmount;
        private System.Windows.Forms.Button buttonSuperWorkerCreate;
        private System.Windows.Forms.Label labelHandAmount;
        private System.Windows.Forms.DataGridView dataGridViewWorkerOfMonth;
        private System.Windows.Forms.Button buttonAddWorkerOfMonth;
        private System.Windows.Forms.TextBox textBoxPrize;
        private System.Windows.Forms.Label labelPrize;
        private System.Windows.Forms.CheckBox checkBoxHandshake;
        private System.Windows.Forms.Button buttonRemoveWorkerOfMonth;
        private System.Windows.Forms.Button buttonSummarizePrizes;
        private System.Windows.Forms.Label labelSumOfPrizes;
    }
}

