﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaciejMajkaLab1
{
    public partial class NewWindow : Form
    {
        private int treeSize; 
        System.Media.SoundPlayer playMusic = new System.Media.SoundPlayer();
        private bool hasGift = false;

        public NewWindow()
        {
            InitializeComponent();
            buttonMusicControl.Visible = false;
            try
            {
                pictureBoxGift.Image = Image.FromFile(@"graphics\prezent.png");
            }
            catch
            {
                pictureBoxGift.BackColor = Color.White;
            }
            
            textBoxTreeChar.Text = "*";
        }

        /// <summary>
        /// Set flag hasGift, display picBox in new window, display message in textBox
        /// </summary>
        /// <returns></returns>
        public bool getGift()
        {
            if (hasGift)
                return hasGift;
            else
                hasGift = true;
                pictureBoxGift.Visible = true;
                string giftRecived = "\n Otrzymałeś prezent od gościa! Kliknij na nim aby go otworzyć!";
                colorTextAppend(richTextBoxTree, giftRecived, Color.DarkRed, HorizontalAlignment.Center);

            return !hasGift;
        }
       
        

          /// <summary>
          /// Loads wav file from executable catalog and plays it in loop
          /// </summary>
         private void playSound()
         {
            try // try to load file, dispose of player 
            {
                playMusic.SoundLocation = @"sounds\jbells.wav";
                playMusic.LoadAsync();
                playMusic.PlayLooping();
                buttonMusicControl.Visible = true;
            }
            catch 
            {
                MessageBox.Show("Wystąpił problem odtwarzaniem dźwięku w tej karcie.", "Uwaga");
            }

        }

        /// <summary>
        /// this function allows to write colored string into richTextBox,
        /// </summary>
        /// <param name="textBoxUsed"></param>
        /// <param name="text"></param>
        /// <param name="color"></param>
        /// <param name="len"></param>
        public void colorTextAppend(RichTextBox textBoxUsed,String text, Color color)
        {
            textBoxUsed.SelectionStart = textBoxUsed.TextLength;
            textBoxUsed.SelectionLength = text.Length;
            textBoxUsed.SelectionColor = color;
            textBoxUsed.AppendText(text);
        }

        /// <summary>
        /// this function allows to write colored string into richTextBox, to given aligment 
        /// </summary>
        /// <param name="textBoxUsed"></param>
        /// <param name="text"></param>
        /// <param name="color"></param>
        /// <param name="align"></param>
        public void colorTextAppend(RichTextBox textBoxUsed, String text, Color color, HorizontalAlignment align)
        {
            textBoxUsed.SelectAll();
            textBoxUsed.SelectionAlignment = align;
            textBoxUsed.SelectionStart = textBoxUsed.TextLength;
            textBoxUsed.SelectionLength = text.Length;
            textBoxUsed.SelectionColor = color;
            textBoxUsed.AppendText(text);
        }

        /// <summary>
        /// Generates christmas tree in RichTextBox
        /// </summary>
        private void treeGenerate()
        {
            if (int.TryParse(textBoxTreeSize.Text, out treeSize) && treeSize < 5 && treeSize > 0) // acquire tree size as int
            { // conversion ok
                string trunkChar = " | ";

                richTextBoxTree.Text = null; // clear window before reprint
                playSound(); // play sound only if tree has been printed out properly 

                for (int levels = 0; levels < treeSize; levels++) // depending on user input, creates treeSize levels of tree
                {
                    for (int i = 0; i < (treeSize + levels); i++) // iterates rows in single level
                    {
                        richTextBoxTree.SelectAll();
                        richTextBoxTree.SelectionAlignment = HorizontalAlignment.Center;

                        for (int y = 0; y < (2 * i + 1); y++) // prints out chars on each row
                        {
                            richTextBoxTree.AppendText(" " + textBoxTreeChar.Text + " ");
                        }
                        richTextBoxTree.AppendText(Environment.NewLine);

                    }

                    if (levels == treeSize - 1) // print trunk after last level
                    {
                        //for()
                        for (int trunk = 0; trunk < treeSize; trunk++)
                        {
                            richTextBoxTree.ForeColor = Color.Green;

                            colorTextAppend(richTextBoxTree, trunkChar, Color.Brown); // outputs Trunk in given color
                        

                        }
                    } // endif

                } // end main for

            } // endof treeGenerate

            else // if arguments used to generate tree are not integer, too big or too small value
            {
                richTextBoxTree.Text = null;
                playMusic.Stop(); 
                string invalidArg = "Podano nieprawidłowy argument."; // conversion fro string to int failed
                colorTextAppend(richTextBoxTree, invalidArg, Color.Red,HorizontalAlignment.Center);

                if (treeSize > 4)
                {
                    string tooBigArg = " Maksymalny rozmiar drzewka wynosi 4.";
                    colorTextAppend(richTextBoxTree, tooBigArg, Color.Red);            
                }

                if (treeSize == 0)
                {
                    string tooSmallArg = " Minimalny rozmiar drzewka wynosi 1.";
                    colorTextAppend(richTextBoxTree, tooSmallArg, Color.Red);
                }
            }
        }

        /// <summary>
        /// Controls button shown after music is played for the first time
        /// </summary>
         private void musicStartStop()
        {
            if (buttonMusicControl.Text == "STOP")
            {
                buttonMusicControl.Text = "GRAJ";
                playMusic.Stop();
            }
            else if (buttonMusicControl.Text == "GRAJ")
            {
                buttonMusicControl.Text = "STOP";
                try
                {
                    playMusic.PlayLooping();
                }
                catch(System.IO.FileNotFoundException ex)
                {
                    MessageBox.Show("" + ex, "Wystąpił wyjątek");

                }
            }
        }
          
        /// <summary>
        /// Event generates tree in textBox, plays music using System media 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            treeGenerate();
        }

        private void buttonMusicControl_Click(object sender, EventArgs e)
        {
            musicStartStop();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBoxTreeSize_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxLoopExample_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBoxTree_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void pictureBoxPresent_Click(object sender, EventArgs e)
        {
            try
            {
                pictureBoxGift.Image = Image.FromFile(@"graphics\cookies.png");
                pictureBoxGift.Update();
            }
            catch
            {
                pictureBoxGift.BackColor = Color.White;
            }
        }

        /// <summary>
        /// Overrides normal handler of closing event of this window
        /// </summary>
        /// <param name="e"></param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            playMusic.Stop();
            buttonMusicControl.Visible = false;
        }

        private void textBoxTreeChar_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
