#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "i2c_twi.h"
#include <avr/wdt.h>


#define KOD_KA (1<<PC3)
#define KOD_KB (1<<PC0)
#define KOD_KC (1<<PC1)
#define KOD_KD (1<<PC2)

#define ANODE1 (1<<PD1)
#define ANODE2 (1<<PD2)
#define ANODE3 (1<<PD3)
#define ANODE4 (1<<PD4)
#define SEPT (1<<PD0)

#define HVC1 (1<<PB2)
#define HVC0 (1<<PB0)

#define SETUP (1<<PB6)
#define MODE (1<<PB7)
#define INCREMENT (1<<PD5)
#define DECREMENT (1<<PD6)
#define SETUP_ACTIVE_LED (1<<PD7)

#define SDA (1<<PC4)
#define SCL (1<<PC5)
#define ITT (1<<PB1)

#define DS3231_ADDR 0xD0 //1101000 adres scalaka

// W��CZ WATCHDOGA I ZAPTELIJ SIE DO RESETU
#define soft_reset()        \
do                          \
{                           \
    wdt_enable(WDTO_15MS);  \
    for(;;)                 \
    {                       \
    }                       \
} while(0)



// Wy��cz watchdoga
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));

void wdt_init(void)
{
    MCUSR = 0;
    wdt_disable();

    return;
}

typedef union{
	uint8_t bytes[7];
	struct {
		uint8_t ss;
		uint8_t mm;
		uint8_t hh;
		uint8_t dayofweek;
		uint8_t day;
		uint8_t month;
		uint8_t year;

	};
}TDATETIME;

typedef struct {
	int16_t cel;
	uint8_t fract;
}TTEMP;



void DS3231_init(void);
void DS3231_get_datetime(TDATETIME * dt);
void DS3231_set_datetime( uint8_t set_minuty, uint8_t set_godziny);
void DS3231_get_temperature(TTEMP * temp);
uint8_t dec2bcd(uint8_t dec);
uint8_t bcd2dec(uint8_t bcd);

TDATETIME datetime;
TTEMP temperature;

void DS3231_get_temperature(TTEMP * temp)
{
	uint8_t buf[2];
	uint8_t status[1];
	TWI_read_buf(DS3231_ADDR,0x11,2,buf);
	temp->cel = buf[0];
	temp->fract = buf[1]>>6;

	TWI_read_buf(DS3231_ADDR,0x0e,1,buf);
	buf[0] |= (1<<5);

	TWI_read_buf(DS3231_ADDR,0x0f,1,status);
	if(!(status[0] & (1<<3))) // testuj czy czasem nie ma konwersji w trakcie
	{
		TWI_write_buf(DS3231_ADDR,0x0e,1,buf); // jesli not busy wymus konw
	}


}


//// ZMIENNE GLOBALNE ////

volatile uint8_t sekundy;
volatile uint8_t minuty;
volatile uint8_t godziny;
volatile uint8_t low_sek;
volatile uint8_t high_sek;
volatile uint8_t low_min;
volatile uint8_t high_min;

volatile uint8_t tempmsb_high;
volatile uint8_t tempmsb_low;
volatile uint8_t templsb_hdigit;
volatile uint8_t templsb_ldigit;

uint8_t set_minuty;
uint8_t set_godziny;


uint8_t CYFRY[] = {0b0000,0b1100,0b0100,0b1000,0b0001,0b1001,0b0010,0b1010,0b0011,0b1011}; // WARTOSCI 0-9 na lampach, kolejnosc -> hardware

int main( void )
{
	//wdt_init();
	i2cSetBitrate(100);
	DS3231_init();

	//////////// TIMER0 MULTIPLEKSUJACY LAMPY CTC///////////////
	TCCR0A |= (1<<WGM01);
	TCCR0B |= (1<<CS02) | (1<<CS00);
	OCR0A =	30;
	TIMSK0 |= (1<<OCIE0A); // compare match z OCIE0A;

	///////////// TIMER2 PWM  programowy DLA SEPT'a ////////////////
	TCCR2A |= (1<<WGM21); // TRYB CTC
	TCCR2B |= (1<<CS22) | (1<<CS21) | (1<<CS20); // prescaler 1024
	OCR2A =	10;
	TIMSK2 |= (1<<OCIE2A);

	DDRB &= ~(1<<PB6);
	PORTB |= (1<<PB6);
	PCICR |= (1<<PCIE0);
	PCMSK0 |= (1<<PCINT6);

	sei(); // URUCHOM GLOBALNE PRZERWANIA

	DDRD &= ~(INCREMENT);
	PORTD |= (INCREMENT);
	DDRD &= ~(DECREMENT);
	PORTD |= (DECREMENT);
	DDRB &= ~(MODE);
	PORTB |= (MODE);





	DDRC |= (KOD_KA) | (KOD_KB) | (KOD_KC) | (KOD_KD);
	DDRD |= (ANODE1) | (ANODE2) | (ANODE3) | (ANODE4);
	DDRD |= SEPT;

	DDRB |= HVC1;
	DDRB |= HVC0;

// DEKLARACJE ZMIENNYCH //


	PORTD &= ~SEPT;

	while(1)
	{
		 //// �ADUJ AKTUALNY CZAS DO ZMIENNYCH ///

			DS3231_get_datetime(&datetime);
			DS3231_get_temperature(&temperature);


			sekundy = datetime.ss;
			minuty = datetime.mm;
			godziny = datetime.hh;

			low_sek = minuty%10; // tak naprawde minuty
			high_sek = (minuty/10)%10;
			low_min = (godziny%10);  // tak naprawde godziny
			high_min = (godziny/10)%10;


			// konwersja temperatury z 10 bit na 4 segmenty


			while(!(PINB & MODE))
			{

			//	templsb_hdigit=((temperature.fract%100)/10); // konwersja formatu np 25.25 stopni tj. 2525 wiec odciecie dziesi�tek i odrzucenie jednostek
			//	templsb_ldigit=((temperature.fract%100)%10); // jw. ale zostawiamy jednosci
			//	tempmsb_high = (temperature.cel/100)/10;
			//	tempmsb_low = (temperature.cel/100)%10;
				tempmsb_low = (temperature.cel%10);
				tempmsb_high = (temperature.cel/10)%10;
				templsb_ldigit = 0;
				templsb_hdigit = ((25*temperature.fract)/10);

				low_min = tempmsb_low;
				high_min = tempmsb_high;
				low_sek = templsb_ldigit;
				high_sek = templsb_hdigit;

			}



			////////////////////////////////////
	}



} // koniec main

void DS3231_init(void)
{
	uint8_t ctrl = 0;
	TWI_write_buf(0xD0, 0x0e, 1, &ctrl);
}


void DS3231_get_datetime( TDATETIME * dt){
	uint8_t i;
	uint8_t buf[7];
	TWI_read_buf(DS3231_ADDR,0x00,7,buf);
	for(i=0; i<7; i++)
	{
		dt->bytes[i] = bcd2dec(buf[i]);
	}
}

void DS3231_set_datetime( uint8_t set_minuty, uint8_t set_godziny){
	uint8_t buf[3];
	buf[0] = 0;
	buf[1] = dec2bcd(set_minuty);
	buf[2] = dec2bcd(set_godziny);
	TWI_write_buf(DS3231_ADDR,0x00,3,buf);
}


uint8_t dec2bcd(uint8_t dec)
{
	return ((dec / 10) << 4) | (dec % 10);
}

uint8_t bcd2dec(uint8_t bcd)
{
	return ((((bcd) >> 4) & 0x0F) * 10) + ((bcd) & 0x0F);
}


ISR (PCINT0_vect)
{

	if(!(PINB & SETUP))
	{
		_delay_ms(500);
		PORTD |= SETUP_ACTIVE_LED;
		PORTD &= ~(SEPT);
		//PORTC &= 0b11110000;
		PORTD |= (ANODE1);
		PORTD |= (ANODE2);
		PORTD |= (ANODE3);
		PORTD |= (ANODE4);

		uint8_t menu = 1;

		while(1)
		{

			wdt_reset();
			uint8_t setting_low=0;
			uint8_t setting_high=0;

switch(menu)
{

case 1:
				PORTC &= 0b11110000;
				PORTD &=~(ANODE2 | ANODE3 | ANODE4);
				PORTD |= ANODE1;

			while(PINB & MODE)
			{

				if(!(PIND & INCREMENT))
						{
							while(!(PIND & INCREMENT))
							{
								//czekaj
							}
							setting_low++;
							setting_low=setting_low%10;
						}

				else if(!(PIND & DECREMENT))
						{
								while(!(PIND & DECREMENT))
										{
													//czekaj
										}
							_delay_ms(20);
							if(setting_low==0)
								{
									setting_low = 9;
								}
							else
									setting_low--;

						}
				PORTC &= 0b11110000;
				PORTC |= CYFRY[setting_low];
				_delay_ms(100);
				menu++;

			}
			while(!(PINB & MODE))
			{
				_delay_ms(20); // czekaj
			}


case 2:

				PORTC &= 0b11110000;
				PORTD &=~(ANODE1 | ANODE3 | ANODE4);
				PORTD |= (ANODE2);

					while(PINB & MODE)
						{

							if(!(PIND & INCREMENT))
								{
									while(!(PIND & INCREMENT))
										{
												//czekaj
										}
											setting_high++;
											setting_high=setting_high%6;
										}

								else if(!(PIND & DECREMENT))
										{
											while(!(PIND & DECREMENT))
												{
																	//czekaj
												}
											if(setting_high==0)
												{
													setting_high = 5;
												}
											else
													setting_high--;

										}
								PORTC &= 0b11110000;
								PORTC |= CYFRY[setting_high];
								_delay_ms(100);
								//menu++;

							}
					while(!(PINB & MODE))
								{
									_delay_ms(20); // czekaj
								}

			set_minuty = (setting_high*10+setting_low);
			setting_low=0;
			setting_high=0;

case 3:

				PORTC &= 0b11110000;
				PORTD &=~(ANODE1 | ANODE2 | ANODE4);
				PORTD |= (ANODE3);

				while(PINB & MODE)
							{

								if(!(PIND & INCREMENT))
										{
											while(!(PIND & INCREMENT))
											{
												//czekaj
											}
											setting_low++;
											setting_low=setting_low%10;
										}

								else if(!(PIND & DECREMENT))
										{
												while(!(PIND & DECREMENT))
														{
																	//czekaj
														}
											_delay_ms(20);
											if(setting_low==0)
												{
													setting_low = 9;
												}
											else
													setting_low--;

										}
								PORTC &= 0b11110000;
								PORTC |= CYFRY[setting_low];
								_delay_ms(100);
								menu++;

							}
				while(!(PINB & MODE))
								{
									_delay_ms(20); // czekaj
								}

case 4:

				PORTC &= 0b11110000;
				PORTD &=~(ANODE1 | ANODE2 | ANODE3);
				PORTD |= (ANODE4);

					while(PINB & MODE)
						{

							if(!(PIND & INCREMENT))
								{
									while(!(PIND & INCREMENT))
										{
												//czekaj
										}
											setting_high++;
											setting_high=setting_high%3;
										}

								else if(!(PIND & DECREMENT))
										{
											while(!(PIND & DECREMENT))
												{
																	//czekaj
												}
											if(setting_high==0)
												{
													setting_high = 2;
												}
											else
													setting_high--;

										}
								PORTC &= 0b11110000;
								PORTC |= CYFRY[setting_high];
								_delay_ms(100);
								menu++;

							}
					while(!(PINB & MODE))
								{
									_delay_ms(20); // czekaj
								}

			set_godziny = (setting_high*10+setting_low);
			if(set_godziny>23)
				set_godziny = 0;

			PORTD |= (ANODE1)|(ANODE2)|(ANODE3)|(ANODE4);


	break;
}


			if(!(PINB & SETUP))
			{
				PORTD &= ~SETUP_ACTIVE_LED;
				for(int i=0;i<5;i++)
				{
					_delay_ms(100);
					PORTD &= ~(ANODE1);
					PORTD &= ~(ANODE2);
					PORTD &= ~(ANODE3);
					PORTD &= ~(ANODE4);
					_delay_ms(100);
					PORTD |= (ANODE1);
					PORTD |= (ANODE2);
					PORTD |= (ANODE3);
					PORTD |= (ANODE4);
				}

				DS3231_set_datetime(set_minuty,set_godziny);
				_delay_ms(100);
				soft_reset();
				break;
			}
		}

	}

}



ISR(TIMER0_COMPA_vect)
{
	static uint8_t counter=1;


	if(counter == 1)
	{
		PORTD &= ~(ANODE4);
		PORTC &= 0b11110000;
		PORTC |=(CYFRY[low_sek]);
		PORTD |=(ANODE1);

	}
	else if(counter==2)
	{
		PORTD &= ~(ANODE1);
		PORTC &= 0b11110000;
		PORTC |=(CYFRY[high_sek]);
		PORTD |=(ANODE2);

	}
	else if(counter==3)
	{

		PORTD &= ~(ANODE2);
		PORTC &= 0b11110000;
		PORTC |=(CYFRY[low_min]);
		PORTD |=(ANODE3);

	}
	else if(counter==4)
	{
		PORTD &= ~(ANODE3);
		PORTC &= 0b11110000;
		PORTC |=(CYFRY[high_min]);
		PORTD |=(ANODE4);
	}

	counter += 1;
	if(counter ==5)
	{
		counter = 1;
	}




}

ISR(TIMER2_COMPA_vect)
{
	if(sekundy%2==0)
	{
	PORTD ^= SEPT;

	}
	else
	PORTD &= ~SEPT;

}


