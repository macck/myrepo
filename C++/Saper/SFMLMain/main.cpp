//////////// Maciej Majka 218044 ////
// Gra przystosowana do plansz minimum 3x3  z uwagi na specyficzn¹ funkcje sprawdzajaca pola dookola  ///
//  Ewentualny zauwazony blad, to mozliwosc postawienia niewidocznej flagi na odkrytym polu/ do poprawienia//

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <SFML/Graphics.hpp>
#define WYS 6
#define SZER 6
#define ROZ 40

using namespace std;

    class Pole{

    bool mina; // czy pole zawiera mine?
    bool czy_odkryte; // czy zostalo odkryte
    bool flaga; // czy gracz oznaczyl pole flaga

    public:

        Pole();
        bool odkryj(); // czy odkryte = true
        bool oznacz(); // "stawia" flage,
        bool zaminuj(); // minuje pole podczas generacji planszy
        bool odznacz(); // zdejmuje flage

                        // WSZYSTKIE PONIZSZE SLUZA DO SPRAWDZANIA STANU POL PRZEZ PLANSZE
                            bool chk_odkryte()
                            {
                                return czy_odkryte;
                            }
                             bool chk_mina()
                            {
                                return mina;
                            }
                             bool chk_flaga()
                            {
                                return flaga;
                            }


    };

                    Pole::Pole()
                            {
                                mina=false;
                                czy_odkryte=false;
                                flaga=false;
                            }


                      bool Pole::zaminuj()
                            {
                                if(mina==false)
                                    {
                                        mina=true;
                                        return true;
                                    }
                                else
                                    return false;
                            }


                    bool Pole::odkryj()
                            {
                                if(czy_odkryte==false)
                                    {
                                        czy_odkryte=1;
                                        return true;
                                    }
                                else
                                    return false;
                            }


                      bool Pole::oznacz() // stawia flagi
                            {
                                if(flaga==false && czy_odkryte==false)
                                    {
                                        flaga=1;
                                        return true;
                                    }
                                else
                                    return false;
                            }

                    bool Pole::odznacz() // zdejmuje flagi
                            {
                                if(flaga==true)
                                    {
                                        flaga=0;
                                        return true;
                                    }
                                 else
                                    return false;
                            }





    class Plansza{

    int N;
    int ileMin;
    int ileFlag;
    int ileOdkrytych;
    Pole tab[WYS][SZER];
    public:

        Plansza();
        Plansza(int N);
        void losujMiny(int N); // losowo rozrzuca N min
        int odkryjPole(int x, int y); // odkrywa pole x,y zwraca : 0 - pole juz odkryte 1 - pole zostalo odkryte 2 - zaminowane (koniec gry)
        bool flaga_oznacz(int x, int y); // zmienia stan pola flaga z 0 na 1 zwraca : true - ok false - stan niezmieniony
        bool flaga_odznacz(int x, int y); // zmienia stan pola flaga z 1 na 0  zwraca : true - ok false - stan niezmieniony
        void wyswietl(); // generuje plansze
        char  pole_status(int x,int y); // pobiera parametry konkretnego pola x,y zwraca literowo odpowiadajacy stan
        int chk_miny(); // zwraca ile min ustawionych jest na planszy
        int chk_flagi(); // zwraca ile flag gracz ustawil
        int minySasiadujace(int x,int y); // sprawdza sasiednie pola czy zawieraja miny
        void rysuj(sf::RenderWindow &okno,int x,int y,sf::Font font,int win);
        int Mouse_Fit_x(int x); // przelicz wspolzedne myszki na polozenie w polu *nieskalowalne dla rozciagania pola rob.
        int Mouse_Fit_y(int y);
    };



                      Plansza::Plansza(int N)
                        {
                        ileMin=N;
                        ileFlag=0;
                        ileOdkrytych=0;
                        losujMiny(N);
                        }

                      void Plansza::losujMiny(int N) // losowo rozrzuca N min na planszy
                      {

                                  int wys_pos=0;
                                  int szer_pos=0;
                                 for(int i=0;i<N;i++)
                                 {
                                      wys_pos=(rand()%(WYS));
                                       szer_pos=(rand()%(SZER));


                                     if(Plansza::tab[wys_pos][szer_pos].chk_mina()==0)
                                     {
                                         Plansza::tab[wys_pos][szer_pos].zaminuj();

                                     }


                                     else i--; // mina byla, losuj ponownie (zwroc 1 do puli losowan)
                                 }
                      }



                      int  Plansza::odkryjPole(int x, int y)
                                    {
                                        if(Plansza::tab[x][y].chk_mina()==1)
                                        {
                                            Plansza::tab[x][y].odkryj();
                                            return 2; // zwroc 2 jesli jest mina
                                        }
                                        if(Plansza::tab[x][y].chk_odkryte()==0)
                                        {
                                        Plansza::ileOdkrytych++;
                                        if(Plansza::tab[x][y].chk_flaga()==1)
                                        {

                                            Plansza::ileFlag--;
                                            Plansza::tab[x][y].odznacz();
                                        }

                                        return Plansza::tab[x][y].odkryj(); // 0 jesli juz bylo odkryte, 1 jesli udalo sie odkryc

                                        }

                                        if(Plansza::ileOdkrytych==((WYS*SZER)-Plansza::ileMin))
                                        {
                                            return 3; // wygrana
                                        }



                                    }



                      bool  Plansza::flaga_oznacz(int x, int y)
                                {
                                    if(ileFlag<ileMin)
                                        {
                                            if(Plansza::tab[x][y].oznacz()==1 )
                                                        {
                                                            Plansza::ileFlag++;
                                                            return 1;
                                                        }
                                        }


                                    return 0;

                                }

                      bool  Plansza::flaga_odznacz(int x, int y)
                                {
                                    if(Plansza::tab[x][y].odznacz()==1)
                                    {
                                        Plansza::ileFlag--;
                                        return 1;
                                    }
                                    return 0;
                                }





                      void Plansza::wyswietl() // generuje macierz rozgrywki i wyswietla na ekran
                        {
                                        for(int i=0;i<WYS;i++)
                                        {
                                            cout << " ";
                                                for(int j=0;j<SZER;j++)
                                                {

                                                    cout << "["<< Plansza::pole_status(i,j) << "]" << "  ";
                                                    if(j==SZER-1)
                                                        cout << endl << endl;
                                                }
                                                if(i==WYS-1)
                                                {
                                                    cout << " \n MIN: " << chk_miny() << " USTAWIONYCH FLAG: " << chk_flagi() << endl;
                                                }
                                        }
                         }

                       void Plansza::rysuj(sf::RenderWindow &okno, int x, int y, sf::Font font,int win)
                       {

                            sf::RectangleShape background(sf::Vector2f(WYS*ROZ,SZER*ROZ));
                                    background.setFillColor(sf::Color::Transparent);
                                    background.setOutlineColor(sf::Color::Black);
                                    background.setOutlineThickness(-1);


                           sf::Text tresc;
                           tresc.setCharacterSize(ROZ-2);
                           tresc.setFont(font);
                           tresc.setStyle(sf::Text::Bold);
                           tresc.setColor(sf::Color::Red);
                           tresc.move(x*ROZ+(ROZ/4),y*ROZ-(ROZ/8));
                           string cyfry="012345678";
                           tresc.setString(cyfry[minySasiadujace(x,y)]);



                           sf::RectangleShape kwad(sf::Vector2f(ROZ,ROZ));
                           kwad.move(x*ROZ,y*ROZ);
                           kwad.setOutlineColor(sf::Color::Black);
                           kwad.setOutlineThickness(-1);



                             if(Plansza::tab[x][y].chk_odkryte()==1 && Plansza::tab[x][y].chk_mina()==1)
                            {
                                kwad.setFillColor(sf::Color::Red);
                                 if(Plansza::ileOdkrytych!=(WYS*SZER))
                                 {
                                     for(int id=0;id<WYS;id++)
                                        for(int jd=0;jd<SZER;jd++)
                                            {
                                                Plansza::odkryjPole(id,jd);
                                            }
                                 }
                            }
                            if(Plansza::tab[x][y].chk_odkryte()==0 && Plansza::tab[x][y].chk_flaga()==0 )
                            kwad.setFillColor(sf::Color::White);
                            if(Plansza::tab[x][y].chk_flaga()==1 && Plansza::tab[x][y].chk_odkryte()==0)
                            kwad.setFillColor(sf::Color::Blue);
                            if(Plansza::tab[x][y].chk_odkryte()==1 && Plansza::tab[x][y].chk_mina()==0)
                            {
                                kwad.setFillColor(sf::Color::Cyan);

                            }

                            okno.draw(kwad);
                            if(Plansza::tab[x][y].chk_odkryte()==1)
                            okno.draw(tresc);
                            if(win==1)
                                {
                                   background.setFillColor(sf::Color::Green);
                                }

                            okno.draw(background);
                       }


                       char Plansza::pole_status(int x, int y)
                           {
                                       if(Plansza::tab[x][y].chk_odkryte()==1)
                                           {
                                              return (48+Plansza::minySasiadujace(x,y)) ; // 48 (kod zera ASCII) + ile min do wyswietlenia tego jako char
                                           }
                                        else if(Plansza::tab[x][y].chk_odkryte()==0 && Plansza::tab[x][y].chk_flaga()==1)
                                           {
                                             return 'P';
                                           }



                                        else if(Plansza::tab[x][y].chk_odkryte()==0) // jesli pole jest zakryte wyswietl O
                                            {
                                              return 'X';
                                            }
                           }

                           int Plansza::chk_miny()
                                    {
                                        return Plansza::ileMin;
                                    }

                           int Plansza::chk_flagi()
                                    {
                                        return Plansza::ileFlag;
                                    }

                           int Plansza::minySasiadujace(int x,int y) // bada pola naokolo czy posiadaja miny
                           {
                               int i=0;

                                     if(y>0) // nie zadziala dla 0-wego rzedu
                                     {
                                            if(x>0) // nie zadziala dla 0-wego rzedu
                                            {
                                                if(Plansza::tab[x-1][y-1].chk_mina()==1)
                                                i++;
                                            }

                                                if(Plansza::tab[x][y-1].chk_mina()==1)
                                                i++;

                                            if(x<SZER-1) // nie zadziala dla skrajnego z prawej
                                            {
                                                if(Plansza::tab[x+1][y-1].chk_mina()==1)
                                                i++;
                                            }

                                     }


                                    if(x>0) // nie zadziala dla 0-wej kolumny
                                            {
                                                if(Plansza::tab[x-1][y].chk_mina()==1)
                                                i++;
                                            }

                                    if(x<SZER-1) // nie zadziala dla skrajnego z prawej
                                            {
                                                if(Plansza::tab[x+1][y].chk_mina()==1)
                                                i++;
                                            }

                                   if(y<WYS-1) // ograniczenie dla dolnego rzedu
                                       {
                                            if(x>0) // nie zadziala dla 0-wej kolumny
                                                {
                                                    if(Plansza::tab[x-1][y+1].chk_mina()==1)
                                                    i++;
                                                }

                                                if(Plansza::tab[x][y+1].chk_mina()==1)
                                                i++;

                                            if(x<SZER-1) // nie zadziala dla skrajnego z prawej
                                                {
                                                    if(Plansza::tab[x+1][y+1].chk_mina()==1)
                                                    i++;
                                                }

                                       }




                                return i;
                           }

                    int Plansza::Mouse_Fit_x(int x)
                        {

                            for(int i=0;i<WYS;i++)
                            {
                                if(x>=i*ROZ && x<(i+1)*ROZ)
                                {
                                    return i;
                                }
                            }


                        }

                    int Plansza::Mouse_Fit_y(int y)
                        {

                            for(int i=0;i<SZER;i++)
                            {
                                if(y>=i*ROZ && y<(i+1)*ROZ)
                                {
                                    return i;
                                }
                            }

                        }




int main()
{
    srand(time(NULL));

    int ile_min=0; //
    int win=0;


    sf::Font font;
    font.loadFromFile("arial.ttf");



    cout << "Ile min rozlosowac? MAX: " << (WYS*SZER-1) << " (automatyczne ograniczenie) " << endl;
    cin >> ile_min;


                                if(ile_min>=WYS*SZER)
                                {
                                    ile_min=(WYS*SZER)-1;

                                }
                                if(ile_min<1)// jesli 0 lub ujemna liczba min, postaw 1 mine
                                {
                                    ile_min=1;
                                }


    Plansza p(ile_min); // generuj plansze o predefiniowanych rozmiarach, rozlosuj miny

     sf::RenderWindow okno(sf::VideoMode((WYS*ROZ),(SZER*ROZ)),"SAPER"); // nowe okno SFML

     while(okno.isOpen())  // glowna petla rysujaca
        {
            sf::Event zdarzenie;  // obsluga zdarzen
            okno.pollEvent(zdarzenie);
            if(zdarzenie.type == sf::Event::Closed) // zamykanie okna
            {
                okno.close();
            }

            okno.clear();

            for(int i=0;i<SZER;i++)  // przekazanie do okna prostokatow
            {
                for(int j=0;j<WYS;j++)
                {

                    p.rysuj(okno,i,j,font,win);

                             if(zdarzenie.type== sf::Event::MouseButtonPressed)
                                {
                                       if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
                                        {
                                        int mouse_x = zdarzenie.mouseButton.x;
                                        int mouse_y = zdarzenie.mouseButton.y;

                                        int os_x = p.Mouse_Fit_x(mouse_x);
                                        int os_y = p.Mouse_Fit_y(mouse_y);


                                        if(p.pole_status(i,j)=='X')
                                        p.flaga_oznacz(os_x,os_y);
                                        }
                                }

                            if(zdarzenie.type== sf::Event::MouseButtonPressed)
                                {
                                       if(sf::Mouse::isButtonPressed(sf::Mouse::Middle))
                                        {
                                        int mouse_x = zdarzenie.mouseButton.x;
                                        int mouse_y = zdarzenie.mouseButton.y;

                                        int os_x = p.Mouse_Fit_x(mouse_x);
                                        int os_y = p.Mouse_Fit_y(mouse_y);


                                        if(p.pole_status(i,j)=='X')
                                        p.flaga_odznacz(os_x,os_y);
                                        }
                                }

                             if(zdarzenie.type== sf::Event::MouseButtonPressed)
                                {
                                       if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
                                        {
                                        int mouse_x = zdarzenie.mouseButton.x;
                                        int mouse_y = zdarzenie.mouseButton.y;

                                        int os_x = p.Mouse_Fit_x(mouse_x);
                                        int os_y = p.Mouse_Fit_y(mouse_y);


                                        if(p.pole_status(i,j)=='X'  )
                                        {

                                            if(p.odkryjPole(os_x,os_y)==3)
                                            {
                                                win=1;
                                                cout << win;
                                            }

                                        }


                                        }
                                }

                }

            }

            okno.display();  // wyrysuj prostokaty/ odswiez okno

        }




    return 0;
}
