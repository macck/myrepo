#include "prowadzacy.h"



Prowadzacy::Prowadzacy(Gracz & G1,Gracz & G2,Gracz & G3, Gracz & G4) : g1(G1),g2(G2),g3(G3),g4(G4)
{
    CreateHands();
    this->AtutCardColor=0;
}


void Prowadzacy::CreateHands()
{
     for(int i=1;i<5;i++)
    {
        for(int j=2;j<15;j++)
        {
            Karta k(i,j);
            Talia.push_back(k);
        }
    }
}




void Prowadzacy::ShuffleCards()
{
    std::random_shuffle(Talia.begin(),Talia.end());
}


void Prowadzacy::PassToPlayers()
{
    for(int i=0;i<13;i++)
        {
              g1.HandTakeCard(Talia.back());
                  Talia.pop_back();
              g2.HandTakeCard(Talia.back());
                  Talia.pop_back();
              g3.HandTakeCard(Talia.back());
                  Talia.pop_back();
              g4.HandTakeCard(Talia.back());
                  Talia.pop_back();
        }
}

void Prowadzacy::RaffleAtutColor()
{
    AtutCardColor=rand()%4+1;
}

std::string Prowadzacy::DisplayAtut()
{
    if(AtutCardColor==1)
        return "PIK";
    if(AtutCardColor==2)
        return "KIER";
    if(AtutCardColor==3)
        return "KARO";
    if(AtutCardColor==4)
        return "TREFL";

    else return "NULL";

}

void Prowadzacy::WhatsInVector()
    {
        std::cout << "\nKarty na stole:("<< Talia.size() <<")\n";
        for(unsigned int i=0;i<Talia.size();i++)
        {
            std::cout << Talia[i];
        }
    }


////////////////////////////////////////// UWAGA, ZAWAL PROGRAMISTYCZNY ///////////////////////////////////////////
void Prowadzacy::PlayerAskForCard(int PlayerIndex,int StColor,int AtutColor)
{
    unsigned int PlayerCardChoice=0;

    if(PlayerIndex==1)
        {
            g1.HandShowDown();
            std::cout << "\n\n\nGracz " << g1.PlayerName() << " " << g1.PlayerSurname() << " kladzie karte: ...\n" << std::endl;
            while(true)
            {
                    PlayerCardChoice=g1.ReturnDecision();

            while(PlayerCardChoice>g1.HandSize() || PlayerCardChoice<=0)
                    {
                        std::cout << " Nie posiadasz takiej karty! " << std::endl;
                        PlayerCardChoice=g1.ReturnDecision();
                    }
            if(StColor!=0 && (g1.HandCardColorCheck(PlayerCardChoice)!=StColor && g1.HandCardColorCheck(PlayerCardChoice)!=AtutColor))
            {
                int isValid=0;
                            for(unsigned int i=0;i<g1.HandSize();i++)
                            {
                                  if(g1.HandCardColorCheck(i)==StColor || g1.HandCardColorCheck(i)==AtutColor)
                                    isValid++;
                            }
                            if(isValid==0)
                            {
                                Talia.push_back(g1.HandPassCard(PlayerCardChoice));
                                  break;
                            }
                            else
                                std::cout << "Nie mozesz polozyc karty w tym kolorze! Posiadasz odpowiednie karty..." << std::endl;
            }

            else
                {
                    Talia.push_back(g1.HandPassCard(PlayerCardChoice));
                    break;
                }

            }


        }

    if(PlayerIndex==2)
        {
            g2.HandShowDown();
            std::cout << "\n\n\nGracz " << g2.PlayerName() << " " << g2.PlayerSurname() << " kladzie karte: ...\n" << std::endl;

              while(true)
                {
                    PlayerCardChoice=g2.ReturnDecision();

                    while(PlayerCardChoice>g2.HandSize() || PlayerCardChoice<=0)
                        {
                            std::cout << " Nie posiadasz takiej karty! " << std::endl;
                            PlayerCardChoice=g2.ReturnDecision();
                        }
                    if(StColor!=0 && (g2.HandCardColorCheck(PlayerCardChoice)!=StColor && g2.HandCardColorCheck(PlayerCardChoice)!=AtutColor))
                        {
                            int isValid=0;
                            for(unsigned int i=0;i<g2.HandSize();i++)
                            {
                                if(g2.HandCardColorCheck(i)==StColor || g2.HandCardColorCheck(i)==AtutColor)
                                  isValid++;
                            }

                            if(isValid==0)
                            {
                                Talia.push_back(g2.HandPassCard(PlayerCardChoice));
                                  break;
                            }
                            else
                                std::cout << "Nie mozesz polozyc karty w tym kolorze! Posiadasz odpowiednie karty..." << std::endl;

                        }

                    else
                        {
                            Talia.push_back(g2.HandPassCard(PlayerCardChoice));
                            break;
                        }

                }
        }

    if(PlayerIndex==3)
        {
            g3.HandShowDown();
            std::cout << "\n\n\nGracz " << g3.PlayerName() << " " << g3.PlayerSurname() << " kladzie karte: ...\n" << std::endl;

           while(true)
                {
                    PlayerCardChoice=g3.ReturnDecision();

                    while(PlayerCardChoice>g3.HandSize() || PlayerCardChoice<=0)
                        {
                            std::cout << " Nie posiadasz takiej karty! " << std::endl;
                            PlayerCardChoice=g3.ReturnDecision();
                        }
                    if(StColor!=0 && (g3.HandCardColorCheck(PlayerCardChoice)!=StColor && g3.HandCardColorCheck(PlayerCardChoice)!=AtutColor))
                        {
                            int isValid=0;
                            for(unsigned int i=0;i<g3.HandSize();i++)
                            {
                                if(g3.HandCardColorCheck(i)==StColor || g3.HandCardColorCheck(i)==AtutColor)
                                  isValid++;
                            }
                            if(isValid==0)
                            {
                                Talia.push_back(g3.HandPassCard(PlayerCardChoice));
                                break;
                            }

                            else
                            std::cout << "Nie mozesz polozyc karty w tym kolorze! Posiadasz odpowiednie karty..." << std::endl;
                        }

                    else
                        {
                            Talia.push_back(g3.HandPassCard(PlayerCardChoice));
                            break;
                        }

                }

        }

    if(PlayerIndex==4)
        {
            g4.HandShowDown();
            std::cout << "\n\n\nGracz " << g4.PlayerName() << " " << g4.PlayerSurname() << " kladzie karte: ...\n" << std::endl;
          while(true)
                {
                    PlayerCardChoice=g4.ReturnDecision();

                    while(PlayerCardChoice>g4.HandSize() || PlayerCardChoice<=0)
                        {
                            std::cout << " Nie posiadasz takiej karty! " << std::endl;
                            PlayerCardChoice=g4.ReturnDecision();
                        }
                    if(StColor!=0 && (g4.HandCardColorCheck(PlayerCardChoice)!=StColor && g4.HandCardColorCheck(PlayerCardChoice)!=AtutColor))
                        {
                            int isValid=0;
                            for(unsigned int i=0;i<g4.HandSize();i++)
                            {
                                if(g4.HandCardColorCheck(i)==StColor || g4.HandCardColorCheck(i)==AtutColor)
                                  isValid++;
                            }
                            if(isValid==0)
                            {
                                Talia.push_back(g4.HandPassCard(PlayerCardChoice));
                                  break;
                            }
                            else
                            std::cout << "Nie mozesz polozyc karty w tym kolorze! Posiadasz odpowiednie karty..." << std::endl;
                        }

                    else
                        {
                            Talia.push_back(g4.HandPassCard(PlayerCardChoice));
                            break;
                        }

                }
        }
}


//////////////////////////////////////////////////// KONIEC KROWIASTEJ METODY /////////////////////////////////////





void Prowadzacy::MatchPlay() // przeprowadzenie gry
{

    int PointsTable[]={0,0,0,0};
    int StartingPlayerIndex=1;
    for(int RoundIndex=0;RoundIndex<13;RoundIndex++) ////// 13 kolejnych rund
    {

                                                            /* WARTOSCI POCZATKOWE STOLU */
                                                             // zaczyna gracz pierwszy -- modyfikacja pod koniec rundy.
                                                            int NextPlayer=0;

                                                            RaffleAtutColor(); // Wylosuj jakiego koloru ma byc karta atutowa
                                                             Karta TableFirstCardColor; // kolor polozony na spod
                                                             Talia.clear();


        /*  OBSLUGA GRACZY */

        std::cout << "\n Runda numer: " << (RoundIndex+1) << " Kolor atutowy: " << DisplayAtut() << std::endl;


        for(int PlayerTableCounter=0;PlayerTableCounter<4;PlayerTableCounter++) // petla dla 4 graczy
            {

                if(PlayerTableCounter==0) // jesli pierwszy ruch w danej rundzie
                        {
                            PlayerAskForCard(StartingPlayerIndex,TableFirstCardColor.CheckColor(),AtutCardColor);
                            TableFirstCardColor=Talia.back();
                            std::cout << "\nPierwsza karta w turze to: " << TableFirstCardColor << "\n";
                            WhatsInVector();
                            NextPlayer=(StartingPlayerIndex%4)+1;
                        }

                        else
                            {
                            PlayerAskForCard(NextPlayer,TableFirstCardColor.CheckColor(),AtutCardColor);
                            WhatsInVector();
                            std::cout << std::endl;
                            NextPlayer=(NextPlayer%4)+1;
                            }

                 if(PlayerTableCounter==3)
                            {
                                     int VectorOfAtutes[4]={0,0,0,0};
                                     int VectorOfMatchinColors[4]={0,0,0,0};
                                     int VectorOfCardPoints[4]={0,0,0,0};
/////////////////////////////////////////// SPRAWDZANIE POSIADANYCH KART PRZYPISYWANIE PKT DO OB. Atut *10, bez kolou 0 pkt bo ktos rzucil "w kolorze" tak czy siak
                            for(int CardOnTable=0;CardOnTable<4;CardOnTable++)
                                       {
                                           if(Talia[CardOnTable].CheckColor()==AtutCardColor)
                                               {
                                                   VectorOfAtutes[((StartingPlayerIndex-1)+CardOnTable)%4]=1;

                                                   VectorOfCardPoints[((StartingPlayerIndex-1)+CardOnTable)%4]=Talia[CardOnTable].CheckValue();
                                                   std::cout << " Gracz: " << ((StartingPlayerIndex-1)+CardOnTable)%4+1 << " ma ATUT: " << VectorOfAtutes[((StartingPlayerIndex-1)+CardOnTable)] <<
                                                    " Wartosc " <<  VectorOfCardPoints[((StartingPlayerIndex-1)+CardOnTable)%4]  << " punktow.\n";
                                               }

                                       }


                                    for(int CardOnTable=0;CardOnTable<4;CardOnTable++)
                                       {
                                           if(Talia[CardOnTable].CheckColor()==TableFirstCardColor.CheckColor() && TableFirstCardColor.CheckColor()!=AtutCardColor)
                                                {
                                                    VectorOfMatchinColors[((StartingPlayerIndex-1)+CardOnTable)%4]=1;

                                                    VectorOfCardPoints[((StartingPlayerIndex-1)+CardOnTable)%4]=Talia[CardOnTable].CheckValue();
                                                    std::cout << " Gracz: " << ((StartingPlayerIndex-1)+CardOnTable)%4+1 << " ma PASUJACY KOLOR: " << VectorOfMatchinColors[((StartingPlayerIndex-1)+CardOnTable)] <<
                                                    " Wartosc: " <<  VectorOfCardPoints[((StartingPlayerIndex-1)+CardOnTable)%4]  << " punktow.\n";
                                                }

                                       }

                                    for(int CardOnTable=0;CardOnTable<4;CardOnTable++)
                                       {
                                           if(Talia[CardOnTable].CheckColor()!=TableFirstCardColor.CheckColor() && Talia[CardOnTable].CheckColor()!=AtutCardColor)
                                                {
                                                    VectorOfMatchinColors[((StartingPlayerIndex-1)+CardOnTable)%4]=0;

                                                    VectorOfCardPoints[((StartingPlayerIndex-1)+CardOnTable)%4]=Talia[CardOnTable].CheckValue();
                                                    std::cout << " Gracz: " << ((StartingPlayerIndex-1)+CardOnTable)%4+1 << " Karta poza kolorem: " << VectorOfMatchinColors[((StartingPlayerIndex-1)+CardOnTable)] <<
                                                    " Wartosc: " <<  VectorOfCardPoints[((StartingPlayerIndex-1)+CardOnTable)%4]  << " punktow.\n";
                                                }

                                       }


//////////////////////////////////////////////// TLUMACZENIE WEKTOROW POSIADANYCH KOLOROW I PKT NA PUNKTY GRACZA

                                    if(PlayerTableCounter==3)
                                            {
                                                int VectorTotalPoints[4]={0,0,0,0};
                                                int PlayerWon=0;

                                                for(int PlayerInVector=0;PlayerInVector<4;PlayerInVector++)
                                                                {
                                                                    if(VectorOfAtutes[PlayerInVector]==1)
                                                                    VectorTotalPoints[PlayerInVector]=(10*VectorOfCardPoints[PlayerInVector]);

                                                                    if(VectorOfMatchinColors[PlayerInVector]==1)
                                                                    VectorTotalPoints[PlayerInVector]=(VectorOfCardPoints[PlayerInVector]);

                                                                    std::cout << "Gracz "<< PlayerInVector+1 << " posiada : " <<VectorTotalPoints[PlayerInVector] << " punktow.\n" ;
                                                                }

                                                for(int Iterator=0;Iterator<4;Iterator++)
                                                            {
                                                                if(VectorTotalPoints[Iterator]>VectorTotalPoints[PlayerWon])
                                                                {
                                                                    PlayerWon=Iterator;
                                                                }
                                                            }

                                                std::cout << " \n RUNDE WYGRAL GRACZ: " << PlayerWon+1 << ". ";
                                                PointsTable[PlayerWon]++;


                                                switch(PlayerWon)
                                                {

                                                  case 0:
                                                  std::cout << g1.PlayerName() << " " << g1.PlayerSurname() << "!" <<std::endl;
                                                  break;

                                                  case 1:
                                                  std::cout << g2.PlayerName() << " " << g2.PlayerSurname() << "!" <<std::endl;
                                                  break;

                                                  case 2:
                                                  std::cout << g3.PlayerName() << " " << g3.PlayerSurname() << "!" <<std::endl;
                                                  break;

                                                  case 3:
                                                  std::cout << g4.PlayerName() << " " << g4.PlayerSurname() << "!" <<std::endl;
                                                  break;

                                                }

                                                StartingPlayerIndex=PlayerWon+1; // ustaw gracza rozpoczynajacego nastepna runde


                                            }

                                        }
                                    }

//////////////////////////////////// OGLOSZENIE WYNIKOW ROZGRYWKI W 13'stej RUNDZIE! ///////////////////////////////
            if(RoundIndex==12)
            {
                int maxim=0;
                for(int i=0;i<4;i++)
                    {
                        if(PointsTable[i]>PointsTable[maxim])
                        {
                            maxim=i;
                        }
                    }

                std::cout << "ZWYCIEZA GRACZ:\n";
                 switch(maxim)
                                {

                                  case 0:
                                  std::cout << g1.PlayerName() << " " << g1.PlayerSurname() << std::endl;
                                  break;

                                  case 1:
                                  std::cout << g2.PlayerName() << " " << g2.PlayerSurname() << std::endl;
                                  break;

                                  case 2:
                                  std::cout << g3.PlayerName() << " " << g3.PlayerSurname() << std::endl;
                                  break;

                                  case 3:
                                  std::cout << g4.PlayerName() << " " << g4.PlayerSurname() << std::endl;
                                  break;

                                }


                std::cout << "PUNKTACJA:\n";

                for(int i=0;i<4;i++)
                        {

                            switch(i)
                                        {

                                          case 0:
                                          std::cout << g1.PlayerName() << " " << g1.PlayerSurname() << " konczy z wynikiem: "  << PointsTable[i] << " punktow!" <<std::endl;
                                          break;

                                          case 1:
                                          std::cout << g2.PlayerName() << " " << g2.PlayerSurname() << " konczy z wynikiem: "  << PointsTable[i] << " punktow!" <<std::endl;
                                          break;

                                          case 2:
                                          std::cout << g3.PlayerName() << " " << g3.PlayerSurname() << " konczy z wynikiem: "  << PointsTable[i] << " punktow!" <<std::endl;
                                          break;

                                          case 3:
                                          std::cout << g4.PlayerName() << " " << g4.PlayerSurname() << " konczy z wynikiem: "  << PointsTable[i] << " punktow!" <<std::endl;
                                          break;

                                        }


                        }


            }



    }


}


