﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace MusicClub
{
    class Jobs : DataBaseTable
    {

        public override SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            databaseQuery = "SELECT * FROM Jobs";
            return base.ShowRecords(sqlCon, dataGridView, databaseQuery);
        }

        public override void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            string databaseQuery = "SELECT * FROM Jobs WHERE Job LIKE" + "'" + text + "%'";
            base.SearchBy(sqlCon, dataGridViewDatabase, databaseQuery);
        }
    }
}
