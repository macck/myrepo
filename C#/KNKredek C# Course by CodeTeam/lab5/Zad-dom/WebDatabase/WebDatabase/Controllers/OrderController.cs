﻿using ApiDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using WebDatabase.Models;

namespace ApiDatabase.Controllers
{
    [RoutePrefix("api/orders")]
    public class OrderController : ApiController
    {
        [HttpGet, Route("")]
        [ResponseType(typeof(IEnumerable<Order>))]
        public IHttpActionResult Get()
        {
            IEnumerable<Order> orders;

            using (var ctx = new ContextClass() )
            {
                orders = ctx.Orders.Include( x => x.OrderedProducts).ToList();
            }

            return Ok(orders);
        }

        [HttpGet, Route("{id:int}", Name = "GetOrder")]
        [ResponseType(typeof(IEnumerable<Order>))]
        public IHttpActionResult Get(int id)
        {
            Order order;
            using (var ctx = new ContextClass())
            {
                order = ctx.Orders.Include(x => x.OrderedProducts).SingleOrDefault(x => x.Id == id);
                return Ok(order);
            }
        }

        [HttpPost, Route("")]
        public IHttpActionResult Post([FromBody]Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            using (var ctx = new ContextClass())
            {
                ctx.Orders.Add(order);
                ctx.SaveChanges();
            }

            return CreatedAtRoute("GetUser", new { id = order.Id }, order);
        }

        // PUT: api/Order/5
        [HttpPut,Route("{id:int}")]
        public IHttpActionResult Update(int id, [FromBody]Order order)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            using (var ctx = new ContextClass())
            {
                order.Id = id;
                ctx.Orders.Attach(order);
                var dbEntry = ctx.Entry(order);
                dbEntry.State = System.Data.Entity.EntityState.Modified;

                if(order.OrderedProducts != null)
                {
                    foreach (var orderedProduct in order.OrderedProducts)
                    {
                        ctx.OrderedProducts.Attach(orderedProduct);
                        var entry = ctx.Entry(orderedProduct);
                        entry.State = orderedProduct.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                        
                    }
                }
                ctx.SaveChanges();
            }
            return Ok(order);
        }

        // DELETE: api/Order/5
        [HttpDelete,Route("{id:int}")]
        public IHttpActionResult Delete(int id)
        {
            using (var ctx = new ContextClass())
            {
                try
                {
                    var master = ctx.Set<Order>().Include(x =>x.OrderedProducts).SingleOrDefault(m => m.Id == id);
                    if (master == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                       List<ProductOrder> orderedProd = new List<ProductOrder>(); // jak to cholerstwo zrobic prosciej
                        // bo to podejzewam ze jest cholernie nieoptymaly sposb
                        foreach(var ordProd in master.OrderedProducts)
                        {
                            orderedProd.Add(ordProd);
                        }

                        ctx.Orders.Remove(master);

                        foreach(var ordProd in orderedProd)
                        {
                            ctx.OrderedProducts.Remove(ordProd);
                        }

                        ctx.SaveChanges();
                    }
                }
                
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                return Ok("Object removed");
            }
           
        }
    }
}
