﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityHomework.Model
{
    public class GoldMine : Entity
    {
        public int Level { get; set; }
        public int GoldPerRound { get; set; }
    }
}
