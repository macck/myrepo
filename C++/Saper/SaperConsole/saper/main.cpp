#include <iostream>
#include <cstdlib>
#include <ctime>
#define WYS 5
#define SZER 5

using namespace std;

    class Pole{

    bool mina; // czy pole zawiera mine?
    bool czy_odkryte; // czy zostalo odkryte
    bool flaga; // czy gracz oznaczyl pole flaga

    public:

        Pole();
        bool odkryj(); // czy odkryte = true
        bool oznacz(); // "stawia" flage,
        bool zaminuj(); // minuje pole podczas generacji planszy
        bool odznacz(); // zdejmuje flage

                        // WSZYSTKIE PONIZSZE SLUZA DO SPRAWDZANIA STANU POL PRZEZ PLANSZE
                            bool chk_odkryte()
                            {
                                return czy_odkryte;
                            }
                             bool chk_mina()
                            {
                                return mina;
                            }
                             bool chk_flaga()
                            {
                                return flaga;
                            }


    };

                    Pole::Pole()
                            {
                                mina=false;
                                czy_odkryte=false;
                                flaga=false;
                            }


                      bool Pole::zaminuj()
                            {
                                if(mina==false)
                                    {
                                        mina=true;
                                        return true;
                                    }
                                else
                                    return false;
                            }


                    bool Pole::odkryj()
                            {
                                if(czy_odkryte==false)
                                    {
                                        czy_odkryte=1;
                                        return true;
                                    }
                                else
                                    return false;
                            }


                      bool Pole::oznacz() // stawia flagi
                            {
                                if(flaga==false)
                                    {
                                        flaga=1;
                                        return true;
                                    }
                                else
                                    return false;
                            }

                    bool Pole::odznacz() // zdejmuje flagi
                            {
                                if(flaga==true)
                                    {
                                        flaga=0;
                                        return true;
                                    }
                                 else
                                    return false;
                            }





    class Plansza{

    int N;
    int ileMin;
    int ileFlag;
    int ileOdkrytych;
    Pole tab[WYS][SZER];
    public:

        Plansza();
        Plansza(int N);
        void losujMiny(int N); // losowo rozrzuca N min
        int odkryjPole(int x, int y); // odkrywa pole x,y zwraca : 0 - pole juz odkryte 1 - pole zostalo odkryte 2 - zaminowane (koniec gry)
        bool flaga_oznacz(int x, int y); // zmienia stan pola flaga z 0 na 1 zwraca : true - ok false - stan niezmieniony
        bool flaga_odznacz(int x, int y); // zmienia stan pola flaga z 1 na 0  zwraca : true - ok false - stan niezmieniony
        void wyswietl(); // generuje plansze
        char  pole_status(int x,int y); // pobiera parametry konkretnego pola x,y zwraca literowo odpowiadajacy stan
        int chk_miny(); // zwraca ile min ustawionych jest na planszy
        int chk_flagi(); // zwraca ile flag gracz ustawil
    };



                      Plansza::Plansza(int N)
                        {
                        ileMin=N;
                        ileFlag=0;
                        ileOdkrytych=0;
                        losujMiny(N);
                        }

                      void Plansza::losujMiny(int N) // losowo rozrzuca N min na planszy
                      {

                                  int wys_pos=0;
                                  int szer_pos=0;
                                 for(int i=0;i<N;i++)
                                 {
                                      wys_pos=(rand()%(WYS));
                                       szer_pos=(rand()%(SZER));
                                       cout << wys_pos << " " << szer_pos << " "  << endl;

                                     if(Plansza::tab[wys_pos][szer_pos].chk_mina()==0)
                                     {
                                         Plansza::tab[wys_pos][szer_pos].zaminuj();

                                     }


                                     else i--; // mina byla, losuj ponownie (zwroc 1 do puli losowan)
                                 }
                      }



                      int  Plansza::odkryjPole(int x, int y)
                                    {
                                        if(Plansza::tab[x][y].chk_mina()==1)
                                        {
                                            return 2; // zwroc 2 jesli jest mina
                                        }
                                        Plansza::ileOdkrytych++;
                                        return Plansza::tab[x][y].odkryj(); // 0 jesli juz bylo odkryte, 1 jesli udalo sie odkryc

                                    }



                      bool  Plansza::flaga_oznacz(int x, int y)
                                {
                                    if(ileFlag<ileMin)
                                        {
                                            if(Plansza::tab[x][y].oznacz()==1)
                                                        {
                                                            Plansza::ileFlag++;
                                                            return 1;
                                                        }
                                        }
                                    else
                                    return 0;

                                }

                      bool  Plansza::flaga_odznacz(int x, int y)
                                {
                                    if(Plansza::tab[x][y].odznacz()==1)
                                    {
                                        Plansza::ileFlag--;
                                        return 1;
                                    }
                                    return 0;
                                }





                      void Plansza::wyswietl() // generuje macierz rozgrywki i wyswietla na ekran
                        {
                                        for(int i=0;i<WYS;i++)
                                        {
                                            cout << " ";
                                                for(int j=0;j<SZER;j++)
                                                {

                                                    cout << "["<< Plansza::pole_status(i,j) << "]" << "  ";
                                                    if(j==SZER-1)
                                                        cout << endl << endl;
                                                }
                                                if(i==WYS-1)
                                                {
                                                    cout << " \n MIN: " << chk_miny() << " USTAWIONYCH FLAG: " << chk_flagi() << endl;
                                                }
                                        }
                         }


                       char Plansza::pole_status(int x, int y)
                           {
                                       if(Plansza::tab[x][y].chk_odkryte()==1)
                                           {
                                             return 'X'; // tutaj trzeba bedzie zagniezdzic jakas funkcje do obliczania min dookola
                                           }
                                        else if(Plansza::tab[x][y].chk_odkryte()==0 && Plansza::tab[x][y].chk_flaga()==1)
                                           {
                                             return 'P';
                                           }

                                        else if(Plansza::tab[x][y].chk_odkryte()==0 && Plansza::tab[x][y].chk_mina()==1)
                                           {
                                             return 'B';
                                           }

                                        else  // jesli pole jest zakryte wyswietl O
                                            {
                                              return 'O';
                                            }
                           }

                           int Plansza::chk_miny()
                                    {
                                        return Plansza::ileMin;
                                    }

                             int Plansza::chk_flagi()
                                    {
                                        return Plansza::ileFlag;
                                    }




int main()
{
    srand(time(NULL));
    int ile_min=0;
    int switch_controller;
    int x_pos,y_pos;
    cout << "Ile min rozlosowac? MAX: " << (WYS*SZER-1) << " (automatyczne ograniczenie) " << endl;
    cin >> ile_min;
    if(ile_min>=WYS*SZER)
    {
        ile_min=(WYS*SZER)-1;

    }
    if(ile_min<1)// jesli 0 lub ujemna liczba min, postaw 1 mine
    {
        ile_min=1;
    }
    Plansza p(ile_min);
    //p.losujMiny(ile_min);
    //p.odkryjPole(3,3);
   // p.odkryjPole(9,8);
    //p.flaga_oznacz(4,4);

while(1)
{
    system("cls");
    p.wyswietl();


    cout << endl;
    cout << " 1. Odkryj pole \n 2. Postaw flage \n 3. Zdejmij flage \n 4. Koniec gry \n";

    cin >> switch_controller;

    switch(switch_controller)

    {
    case 1:
        cin >> x_pos >> y_pos;
        if(p.odkryjPole(x_pos,y_pos)==2)
        {
            cout << " BOOM! GAME OVER! ";
            cin.get();
            return 0;
        }
        break;


    case 2:

        cin >> x_pos >> y_pos;
        p.flaga_oznacz(x_pos,y_pos)==0;


        break;
    case 3:

        cin >> x_pos >> y_pos;
        p.flaga_odznacz(x_pos,y_pos);

        break;
    case 4:

        return 0;
        break;
    }
}




    return 0;
}
