﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace nixie
{
    public partial class SetForm : Form
    {
        public SetForm()
        {
            InitializeComponent();
        }

        private void SetForm_Load(object sender, EventArgs e)
        {
            numHour.Value = numAHour.Value = DateTime.Now.Hour;
            numMinute.Value = numAMinute.Value = DateTime.Now.Minute;
            numSecond.Value = DateTime.Now.Second;
            numDay.Value = DateTime.Now.Day;
            numMonth.Value = DateTime.Now.Month;
            numYear.Value = DateTime.Now.Year;

            comDigit1.SelectedIndex = comDigit2.SelectedIndex = comDigit3.SelectedIndex = comDigit4.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnLocalTime_Click(object sender, EventArgs e)
        {
            Uart.SetTime(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }

        private void btnSendTime_Click(object sender, EventArgs e)
        {
            Uart.SetTime((int)numHour.Value, (int)numMinute.Value, (int)numSecond.Value);
        }

        private void btnSendDate_Click(object sender, EventArgs e)
        {
            Uart.SetDate((int)numDay.Value, (int)numMonth.Value, (int)numYear.Value);
        }

        private void btnSendAlarm_Click(object sender, EventArgs e)
        {
            Uart.SetAlarm((int)numAHour.Value, (int)numAMinute.Value);
        }

        private void btnSendDigits_Click(object sender, EventArgs e)
        {
            Uart.SetDigits(comDigit1.SelectedIndex, comDigit2.SelectedIndex, comDigit3.SelectedIndex, comDigit4.SelectedIndex);
        }



        
    }
}
