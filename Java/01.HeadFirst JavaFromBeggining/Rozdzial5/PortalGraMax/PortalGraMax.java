import java.util.ArrayList;

class PortalGraMax {
	
     private   int iloscRuchow;
	 private   PomocnikGry pomocnik;
	 private	ArrayList<Portal> listaPortali;
		
	public static void main(String[] args)
	{
		PortalGraMax gra = new PortalGraMax();
		
		gra.przygotujGre();
		gra.rozpocznijGre();
		
	}
	
	void przygotujGre()
	{
		 
		 pomocnik = new PomocnikGry();
		 listaPortali = new ArrayList<Portal>();
		 Portal portal1 = new Portal();
		 portal1.setNazwa("Onet.pl");
		 Portal portal2 = new Portal();
		 portal2.setNazwa("wp.pl");
		 Portal portal3 = new Portal();
		 portal3.setNazwa("weka.pwr.edu.pl");
		 listaPortali.add(portal1);
		 listaPortali.add(portal2);
		 listaPortali.add(portal3);
		 
	System.out.println("Twoim celem jest zatopienie 3 poratli: " + portal1.getNazwa() + " " + portal2.getNazwa() + " " + portal3.getNazwa() + "\nPostaraj sie zrobic to w jak najmniejszej ilosci ruchow.");
	
	for(Portal rozmieszczanyPortal : listaPortali)
	{
		ArrayList<String> nowePolozenie = pomocnik.rozmiescPortal(3); // generuj 3 pola przylegajace w metodzie pomocnika, zwroc do portalu i przypisz
		rozmieszczanyPortal.setPolaPolozenia(nowePolozenie);
		
	}
		 
	}
	
	void rozpocznijGre(){
		
		while(!listaPortali.isEmpty()){
			String ruch = pomocnik.pobierzDaneWejsciowe("Wskaz pole: ");
				sprawdzRuchGracza(ruch);		
		}
		zakonczGre();
		
	}
	
	
	void  sprawdzRuchGracza(String ruch)
	{
		iloscRuchow++;
		String wynik = "Pud�o!";
		for(Portal sprawdzanyPortal : listaPortali)
		{
			
			wynik = sprawdzanyPortal.sprawdzPole(ruch);
			if(wynik.equals("Trafiony!"))
			{
				break;
			}
			if(wynik.equals("Zatopiony!"))
			{
				System.out.println("Zatopiles: " + sprawdzanyPortal.getNazwa());
				listaPortali.remove(sprawdzanyPortal); // jesli z portalu aktualnie sprawdzanego uzyskalismy informacje ze zostal zatopiony to wywalamy go z listy portali.
				break;
			}
		}
		
		System.out.println(wynik);
		
	}
	
	void zakonczGre(){
		
		System.out.println("Koniec gry! Zatopiles wszystkie portale w " + iloscRuchow + " ruchach.");
	}
	
	
	
	
	
}