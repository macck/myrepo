﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicClub
{
    class Drinks : DataBaseTable
    {
        public override SqlDataAdapter ShowRecords(SqlConnection sqlCon, DataGridView dataGridView, string databaseQuery)
        {
            databaseQuery = "SELECT * FROM Drinks";
            return base.ShowRecords(sqlCon, dataGridView, databaseQuery);
        }

        public override void SearchBy(SqlConnection sqlCon, DataGridView dataGridViewDatabase, string text)
        {
            string databaseQuery = "SELECT * FROM Drinks WHERE Name LIKE" + "'" + text + "%'";
            base.SearchBy(sqlCon, dataGridViewDatabase, databaseQuery);
        }
    }
}

