﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MusicClub
{
    public partial class MusicClub : Form
    {
        private SqlConnection sqlConnection;
        private List<DataBaseTable> allTables = new List<DataBaseTable>();
        private SqlCommandBuilder commandBuilder;

        /* More comfortable object interface to use though i know we used statics i find it here more usefull */
        private Concert concert = new Concert();
        private Employees employees = new Employees();
        private Equipment equipment = new Equipment();
        private Guests guests = new Guests();
        private Inventory inventory = new Inventory();
        private Jobs jobs = new Jobs();
        private Receipts receipts = new Receipts();
        private Supplies supplies = new Supplies();
        private Tickets tickets = new Tickets();
        private Schedules schedules = new Schedules();
        private Drinks drinks = new Drinks();

        /* allows to handle them as an array, so by single selection and one button make actions on any given */
        private DataSet dataSet;   
        private string queryString = "SELECT * FROM Guests"; // doesnt matter, being changed inside class

      
        public MusicClub()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source=MACCK-Komputer\\SQLEXPRESS; database=MusicClub;Trusted_Connection=yes");

            /* ADD ALL CLASSES TO LIST */
            allTables.Add(concert); allTables.Add(drinks); allTables.Add(employees); allTables.Add(equipment); allTables.Add(guests); allTables.Add(inventory);
            allTables.Add(jobs); allTables.Add(receipts); allTables.Add(supplies); allTables.Add(tickets); allTables.Add(schedules);


            comboBoxSelectTable.DataSource = allTables; // make sure comboBox contain valid information about object-index relation
            dataGridViewDatabase.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dataSet = new DataSet();

            for(int i = 0; i < allTables.Count; i++) // create usable in program array to manipulate data from SQL
            {
                dataSet.Tables.Add();
                allTables[i].ShowRecords(sqlConnection, dataGridViewDatabase, queryString).Fill(dataSet.Tables[i]); // get data adapters from every table 
                dataGridViewDatabase.DataSource = null;
            }
        }


        /// <summary>
        ///  Fills dgridview with selected table
        /// </summary>
        /// <param name="index"></param>
        private void FillGridViewWithSelected(int index)
        {
            if (index >= 0)
                dataGridViewDatabase.DataSource = dataSet.Tables[index];
        }


        /// <summary>
        ///  Refreshes data FROM SQL 
        /// </summary>
        private void UpdateAllData()
        {
            dataSet.Clear();
            for (int i = 0; i < allTables.Count; i++) // REFRESH data in ALLDATATABLES
            {
                
                allTables[i].ShowRecords(sqlConnection, dataGridViewDatabase, queryString).Fill(dataSet.Tables[i]); // get FRESH
            }
        }


        /// <summary>
        /// Add, edit, save to SQL 
        /// </summary>
        private void UpdateSelected()
        {
            try
            {
                SqlDataAdapter daAda;
                daAda = allTables[  (comboBoxSelectTable.SelectedIndex)   ].ShowRecords(sqlConnection, dataGridViewDatabase, queryString);
                commandBuilder = new SqlCommandBuilder(daAda);

                daAda.Update(dataSet,dataSet.Tables[    (comboBoxSelectTable.SelectedIndex) ].ToString()    );
               

                MessageBox.Show("Rekord zmodyfikowany!", "Informacja");

            }

            catch(Exception )
            {
                MessageBox.Show("Ten program nie potrafi edytować tabel połączonych :)","Zrypało się!");
            }

            UpdateAllData();
            FillGridViewWithSelected((comboBoxSelectTable.SelectedIndex)); // instant update of gridView

        }


        /// <summary>
        /// Delete selected rows and update SQL
        /// </summary>
        private void DeleteSelected() // deletes selected row, updates database via editing function
        {

            int rows = dataGridViewDatabase.SelectedRows.Count;
            if(dataGridViewDatabase.Rows.Count > 1)
            {
                for (int i = rows - 1; i >= 0; i--)
                {
                    dataSet.Tables[(comboBoxSelectTable.SelectedIndex)].Rows[(dataGridViewDatabase.SelectedRows[i].Index)].Delete();
                }
            }

            UpdateSelected();
        }

        /// <summary>
        /// For selected table do search on query specified in (table) class
        /// </summary>
        /// <param name="text"></param>
        private void SearchBy(string text)
        {
            allTables[comboBoxSelectTable.SelectedIndex].SearchBy(sqlConnection,    dataGridViewDatabase    ,text);
        }

        private void buttonShowAllRecords_Click(object sender, EventArgs e)
        {
            FillGridViewWithSelected(comboBoxSelectTable.SelectedIndex);
        }


        private void Edit_Click(object sender, EventArgs e)
        {
            UpdateSelected();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            DeleteSelected();
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            SearchBy(textBoxSearch.Text);

            if (textBoxSearch.Text != "")
            {
                labelInfo.Visible = true;
            }
            else
                labelInfo.Visible = false;
        }
    }
}
