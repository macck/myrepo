﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ObjectCatalog
{
    public partial class WindowGuitarAdd : Form
    {
        public WindowGuitarAdd()
        {
            InitializeComponent();
            pictureBoxGuitarPic.SizeMode = PictureBoxSizeMode.StretchImage;
            result = DialogResult.None;
        }

        public WindowGuitarAdd(List<Guitar> lista)
        {
            InitializeComponent();
            pictureBoxGuitarPic.SizeMode = PictureBoxSizeMode.StretchImage;
            result = DialogResult.None;
            this.lista = lista;
        }
        private List<Guitar> lista;
        private Guitar retObjGuitar;
        public DialogResult result;
        private string pathToImage = "";



        /// <summary>
        /// Returns path to opened file, if failed returns empty string ("")
        /// </summary>
        private string openFile()
        {
           string fileName = "";
           OpenFileDialog openFileGuitars = new OpenFileDialog();
           DialogResult result = openFileGuitars.ShowDialog();
            if (result == DialogResult.OK)
            {
                if(openFileGuitars.FileName.EndsWith(".png") || openFileGuitars.FileName.EndsWith(".jpg") || openFileGuitars.FileName.EndsWith(".jpeg"))
                {
                    pictureBoxGuitarPic.Image = Image.FromFile(openFileGuitars.FileName);
                    fileName = openFileGuitars.FileName;    
                }
        
            }

            return fileName;
        }

        /// <summary>
        /// creates new object, into class field retObjGuitar, adds this object into forwarded list
        /// </summary>
        private void AddGuitar()
        {
            if (textBoxManufacturer.Text != "" && textBoxModel.Text != "" && textBoxColor.Text != "")
            {
                int stringCount = 0;
                int.TryParse(listBoxStringCount.Text, out stringCount);
                retObjGuitar = new Guitar(textBoxManufacturer.Text, textBoxModel.Text, stringCount, listBoxType.Text, textBoxColor.Text);
                if (pathToImage != "")
                {
                    retObjGuitar.SetPathToImage(pathToImage);
                }
                retObjGuitar.SetDescription(textBoxDescription.Text);
                result = DialogResult.OK;
            }

            lista.Add(retObjGuitar);
        }




        private void buttonAddClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddImage_Click(object sender, EventArgs e)
        {
           pathToImage = openFile();
        }

        private void buttonAddGuitar_Click(object sender, EventArgs e)
        {
            AddGuitar();
            this.Close();
        }
    }
}
