/*
 * LabeledDataSource.cpp
 *
 *  Created on: 02.12.2016
 *      Author: Macck
 */

#include "LabeledDataSource.h"


LabeledDataSource::LabeledDataSource(const std::string & path)
{
	hasDataLeft = false;
	dataUsed = true;

	 inputStream.open( path.c_str());

	if(inputStream.good())
	{
		isFileOpen = true;
	}

}

bool LabeledDataSource::hasData(){
	 // returns true if there is data available to work with, also that means it puts line from stream into class internal buffer std::string upDataRead;
	// returns false if EOF flag is set in fstream, also meaning upDataRead contains old, used data.


		if(dataUsed == true) // check if previous data has been used by getData method
		{
			getline(inputStream,upDataRead); // if not read next line
			dataUsed = false; // there is new data in buffer, need to read it first

			if(inputStream.eof()) // has end of file been reached?
				{
					if(	hasDataLeft == true	) // if end of file was tested for 1st time
						{
							hasDataLeft = false; // set flag

							return true; // allow loop to throw remaining data vector
						}
				}

			else
				{
					hasDataLeft = true; // still data to read
				}

			return hasDataLeft; // return true or false in case of new reading

		}

	else

	return !hasDataLeft; // returns false - no point in reading new data


}

std::vector<int> LabeledDataSource::getData()
{
std::vector<int> dataLine;
stringParse(upDataRead,dataLine);
dataUsed = true;


return dataLine;
}

void LabeledDataSource::stringParse(std::string stringToParse, std::vector<int>& dataOut)
{
	int equalsIndex = 0;
	std::string tempBuff;
	int arrayLen = 0;


	for(int i=0; i< stringToParse.length(); i++)
	{
		if(stringToParse[i] == '=') // v1=xxxx v2=yyyy etc
		{
			equalsIndex = i;

			for(;i < stringToParse.length(); i++)
			{
				if(stringToParse[i] == ' ' || stringToParse[i] == '\n')
				{
					break;
				}
			}

			arrayLen = i - equalsIndex - 1;
			tempBuff = stringToParse.substr(equalsIndex+1, arrayLen); // copy array of chars to temporary buffer
			dataOut.push_back(static_cast<int>(std::strtol(tempBuff.c_str(),0,10))); // write to forwarded VECTOR
		}

	}

}



LabeledDataSource::~LabeledDataSource() {
	// TODO Auto-generated destructor stub
}



