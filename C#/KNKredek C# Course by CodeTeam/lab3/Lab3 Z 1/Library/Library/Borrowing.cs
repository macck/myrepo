﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    class Borrowing
    {
       private static SqlDataAdapter sqlDataAdapter;

        private static void FillDataGridViewLib(SqlDataAdapter dataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null; // clear
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;

        }

        public static void ShowAllBorrowedBooks(SqlConnection sqlCon, DataGridView dataGridViewLib)
        {
            sqlDataAdapter = new SqlDataAdapter("SELECT Users.Name AS 'Imię', Users.Surname AS 'Nazwisko', Books.Title AS 'Tytuł', Books.Author AS 'Autor', DateOfBorrowing AS 'Data wypożyczenia', DateOfReturn AS 'Data zwrotu' FROM Borrowings JOIN Users ON UserID=Users.ID JOIN Books ON BookID=Books.ID", sqlCon);
            FillDataGridViewLib(sqlDataAdapter, dataGridViewLib);
        }

        public static void ShowAllCurrentlyTaken(SqlConnection sqlCon, DataGridView dataGridViewLib)
        {
            sqlDataAdapter = new SqlDataAdapter("SELECT Books.Title AS 'Tytuł', Books.Author AS 'Autor', DateOfBorrowing AS 'Data wypożyczenia' FROM Borrowings JOIN Books ON BookID=Books.ID WHERE DateOFReturn IS NULL", sqlCon);
            FillDataGridViewLib(sqlDataAdapter, dataGridViewLib);
        }

        public static void SortBy(SqlConnection sqlCon, DataGridView dataGridViewLib, string selectedCriteria)
        {
            if(selectedCriteria != "")
            {
                sqlDataAdapter = new SqlDataAdapter("SELECT Users.Name AS 'Imię', Users.Surname AS 'Nazwisko', Books.Title AS 'Tytuł', Books.Author AS 'Autor', DateOfBorrowing AS 'Data wypożyczenia', DateOfReturn AS 'Data zwrotu' FROM Borrowings JOIN Users ON UserID=Users.ID JOIN Books ON BookID=Books.ID ORDER BY " + selectedCriteria, sqlCon);
                FillDataGridViewLib(sqlDataAdapter, dataGridViewLib);
            }
        }

    }
}
