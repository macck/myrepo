﻿using EntityHomework.Model;
using EntityHomework.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EntityHomework
{
    public partial class EntityHomework : Form
    {
        private readonly EntityHomeworkContext context;
        private readonly RWEntityHomework<User> repository;
        public RoundComponent GameMaster;
        private int currentLoggedId = 0;
        private List<User> currentlyLoggedList;
        private string messageOriginal;
        private string messageActionsOriginal;
        private User CurrentUser;

        public EntityHomework()
        {
            InitializeComponent();
            context = new EntityHomeworkContext();
             repository = new RWEntityHomework<User>(context);

            currentlyLoggedList = new List<User>();
            currentlyLoggedList.Add(new User());
            messageOriginal = labelUserInfo.Text;
            messageActionsOriginal = labelAvailableActions.Text;
            dataGridViewMain.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            buttonLogin.Enabled = false;
            if(context.Users.Count() > 0)
            {
                InitializeGamemaster();
            }
        }

        private void InitializeGamemaster()
        {
            List<int> ids = new List<int>();
            ids = repository.GetIds().ToList();
            GameMaster = new RoundComponent(ids);
            labelAvailableActions.Text = messageActionsOriginal + GameMaster.GetActionsLeft();
            buttonGameStart.Enabled = false;
            buttonGameStart.Visible = false;
            buttonLogin.Enabled = true;
        }

        private void DisplayInGridViewLoggedUser()
        {
            dataGridViewMain.DataSource = null;
            dataGridViewMain.DataSource = repository.GetAll().Where(y => y.Id == currentLoggedId).Select(x => new { Nick = x.NickName, Zloto = x.Resources.Amount, PoziomRatusza = x.Hall.Level, Obrona = x.Hall.Defence, PoziomKopalni = x.Mine.Level, Zarobek = x.Mine.GoldPerRound, PoziomBohatera = x.Heroes.First().Level, Atak = x.Heroes.First().Attack, Def = x.Heroes.First().Defence, Luck = x.Heroes.First().Coeff }).ToList();
        }
        
        private void GameOptionsVisibility(bool state)
        {
            buttonUpgradeTownHall.Visible = state;
            buttonBuyHero.Visible = state;
            buttonEarnGold.Visible = state;
            buttonUpgradeMine.Visible = state;
            dataGridViewMain.Visible = state;
            labelAvailableActions.Visible = state;
            buttonLogout.Visible = state;
            labelHeroName.Visible = !state;
            textBoxHeroName.Visible = !state;
            buttonCreateUser.Visible = !state;
        }

        private void UpdateMessage()
        {
            labelAvailableActions.Text = messageActionsOriginal + GameMaster.GetActionsLeft() + " Aktualnie jest tura gracza: " + repository.GetById(GameMaster.GetCurrentPlayerID()).NickName;
            DisplayInGridViewLoggedUser();
        }

        private bool IsValidPlayer()
        {
            if (! (currentLoggedId == GameMaster.GetCurrentPlayerID() ))
            {
                MessageBox.Show("Brak dostępnych ruchów! Teraz tura gracza: " + repository.GetById(GameMaster.GetCurrentPlayerID()).NickName, "Niepowodzenie!", MessageBoxButtons.OK);
                textBoxUsername.Text = repository.GetById(GameMaster.GetCurrentPlayerID()).NickName;
                return false;
            }
            else 

            return true;
        }
    
        private void Login()
        {
            if (textBoxUsername.Text != "" && textBoxPassword.Text != "")
            {
               currentLoggedId = repository.ValidateLogin(textBoxUsername.Text,textBoxPassword.Text);
                if(currentLoggedId != 0)
                {
                    labelUserInfo.Text = "Użytkownik : " + textBoxUsername.Text + " ID: " + currentLoggedId + " Bohater gracza: " + repository.GetById(currentLoggedId).Heroes.First().HeroName ;
                    DisplayInGridViewLoggedUser();
                    GameOptionsVisibility(true);
                }

                else
                {
                    labelUserInfo.Text = "Nieprawidłowy login lub hasło!";
                }
            }

            else
            {
                labelUserInfo.Text = "Podaj login oraz hasło!";
            }
        }

        private void Logout()
        {
            currentLoggedId = 0;
            GameOptionsVisibility(false);
            labelUserInfo.Text = messageOriginal;
        }

        private void EarnGold()
        {
            if( IsValidPlayer() )
            {
                if (GameMaster.PlayerDoAction(currentLoggedId))
                {
                    CurrentUser = repository.GetById(currentLoggedId);
                    CurrentUser.Resources.Amount += (100 + CurrentUser.Mine.GoldPerRound) ;
                    repository.EditSession(CurrentUser);
                    CurrentUser = null;
                    UpdateMessage();
                }

                else
                    UpdateMessage();
            }  
        }

        private void UpgradeTownHall()
        {
            if( IsValidPlayer() )
            {
                CurrentUser = repository.GetById(currentLoggedId);
                int UpgradeCost = ( Convert.ToInt32(Math.Pow(CurrentUser.Hall.Level, 2) * 185 ) );
                if (CurrentUser.Resources.Amount > UpgradeCost )
                {
                    if (GameMaster.PlayerDoAction(currentLoggedId))
                    {
                        CurrentUser.Resources.Amount -= UpgradeCost;
                        CurrentUser.Hall.Level++;
                        CurrentUser.Hall.Defence = CurrentUser.Hall.Level * 23;
                        repository.EditSession(CurrentUser);
                        CurrentUser = null;
                        UpdateMessage();
                    }
                    else
                        UpdateMessage();
                }
                else
                {
                    MessageBox.Show("Nie masz wystarczająco dużo zasobów aby ulepszyć ten budynek! Potrzeba " + UpgradeCost + " złota!", "Niepowodzenie!", MessageBoxButtons.OK);
                }
            }
        }

        private void UpgradeMine()
        {
            if (IsValidPlayer())
            {
                CurrentUser = repository.GetById(currentLoggedId);
                int UpgradeCost = ( Convert.ToInt32(Math.Pow( CurrentUser.Mine.Level , 2 )  * 135 ) );
                if (CurrentUser.Resources.Amount > UpgradeCost)
                {
                    if (GameMaster.PlayerDoAction(currentLoggedId))
                    {
                        CurrentUser.Resources.Amount -= UpgradeCost;
                        CurrentUser.Mine.Level++;
                        CurrentUser.Mine.GoldPerRound += CurrentUser.Mine.Level * 20 + 15 * CurrentUser.Hall.Level;
                        repository.EditSession(CurrentUser);
                        CurrentUser = null;
                        UpdateMessage();
                    }
                    else
                        UpdateMessage();
                }
                else
                {
                    MessageBox.Show("Nie masz wystarczająco dużo zasobów aby ulepszyć ten budynek! Potrzeba " + UpgradeCost + " złota!", "Niepowodzenie!", MessageBoxButtons.OK);
                }
            }
        }

        private void UpgradeHero()
        {
            if (IsValidPlayer())
            {
                CurrentUser = repository.GetById(currentLoggedId);
                int UpgradeCost = (200 + Convert.ToInt32(Math.Pow(CurrentUser.Heroes.First().Level, 2) * 400 - CurrentUser.Hall.Level * 200) );
                if (CurrentUser.Resources.Amount > UpgradeCost)
                {
                    if (GameMaster.PlayerDoAction(currentLoggedId))
                    {
                        CurrentUser.Resources.Amount -= UpgradeCost;
                        CurrentUser.Heroes.First().Level++;
                        CurrentUser.Heroes.First().Attack += 3;
                        CurrentUser.Heroes.First().Defence += 4;

                        repository.EditSession(CurrentUser);
                        CurrentUser = null;
                        UpdateMessage();
                    }
                    else
                        UpdateMessage();
                }
                else
                {
                    MessageBox.Show("Nie masz wystarczająco dużo zasobów aby ulepszyć tego bohatera! Potrzeba " + UpgradeCost + " złota!", "Niepowodzenie!", MessageBoxButtons.OK);
                }
            }
        }

        private void CreateUser()
        {
            if (textBoxUsername.Text != "" && textBoxPassword.Text != "" && textBoxHeroName.Text != "" && currentLoggedId == 0 )
            {
                Random rnd = new Random();
                Hero NewHero = new Hero() { HeroName = textBoxHeroName.Text, Attack = rnd.Next(30, 60), Defence = rnd.Next(30, 60), Coeff = rnd.Next(1, 10), Level = 1 };
                List<Hero> ToAdd = new List<Hero>();
                ToAdd.Add(NewHero);

                User user = new User()
                {
                    NickName = textBoxUsername.Text,
                    Password = textBoxPassword.Text,
                    Resources = new Resource()
                    {
                        Amount = 300
                    },

                    Hall = new TownHall()
                    {
                        Level = 1,
                        Defence = 10

                    },

                    Mine = new GoldMine()
                    {
                        Level = 1,
                        GoldPerRound = 250
                    },

                    Heroes = ToAdd

                };



                if (repository.CreateUser(user, textBoxUsername.Text))
                {
                    MessageBox.Show("Użytkownik utworzony!", "Powodzenie!", MessageBoxButtons.OK);
                    if(GameMaster != null)
                    {
                        GameMaster = null;
                        InitializeGamemaster();
                    }
                }
                else
                {
                    MessageBox.Show("Nie udało się utworzyć użytkownika. Spróbuj użyć innej nazwy.", "Niepowodzenie!", MessageBoxButtons.OK);
                }
            }

            else
            {
                MessageBox.Show("Musisz wypełnić wymagane pola!", "Niepowodzenie!", MessageBoxButtons.OK);
            }
        }



        private void buttonCreateUser_Click(object sender, EventArgs e)
        {
            CreateUser();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Logout();
        }

        private void buttonEarnGold_Click(object sender, EventArgs e)
        {
            EarnGold();
        }

        private void buttonGameStart_Click(object sender, EventArgs e)
        {
            if (context.Users.Count() > 0)
                InitializeGamemaster();
            else
                MessageBox.Show("Brak Graczy!", "Błąd!", MessageBoxButtons.OK);
        }

        private void buttonUpgradeTownHall_Click(object sender, EventArgs e)
        {
            UpgradeTownHall();
        }

        private void buttonUpgradeMine_Click(object sender, EventArgs e)
        {
            UpgradeMine();
        }

        private void buttonBuyHero_Click(object sender, EventArgs e)
        {
            UpgradeHero();
        }
    }
}
