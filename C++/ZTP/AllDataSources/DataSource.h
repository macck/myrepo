/*
 * DataSource.h
 *
 *  Created on: 18.11.2016
 *      Author: c
 */

#ifndef DATASOURCE_H_
#define DATASOURCE_H_

#include <vector>

class DataSource {

public:
	virtual std::vector<int> getData() = 0;
	virtual bool hasData() = 0;
	virtual ~DataSource();
};

#endif /* DATASOURCE_H_ */
