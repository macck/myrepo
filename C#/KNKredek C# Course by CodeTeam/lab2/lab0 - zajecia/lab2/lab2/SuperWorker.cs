﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class SuperWorker : Worker
    {

        private int handAmount;

        public SuperWorker()
        {
        }

        public SuperWorker(string workerName, float salary,int handAmount) : base(workerName,salary)
        {
            this.handAmount = handAmount;
        }

        public string GetHandAmount()
        {
            return handAmount.ToString();
        }
    }
}
