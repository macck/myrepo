public class ProstyPortalTester {
public static void main(String[] args){
	
	System.out.println("Tworzenie obiektu ProstyPortal...:");
	ProstyPortal portal = new ProstyPortal();
	System.out.println("Tworzenie tablicy polozen portali:...");
	int[] polozenia = {2,3,4};
	System.out.println("Ustawianie polozenia portalu w obiekcie ProstyPortal:...");
	portal.setPolaPolozenia(polozenia);
	String wybranePole = "2" ; // testowe pole wyboru
	System.out.println("Sprawdzanie poprawnosci odkrywania pol:...");
	String wynik = portal.sprawdzPole(wybranePole); 
	
	String wynikTestu = "Niepowodzenie";
	
	if(wynik.equals("Trafiony!"))
	{
		wynikTestu = "Zakonczony pomyslnie";
	}
	
	System.out.println(wynikTestu);
	
}
	
}