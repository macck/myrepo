#include <iostream>
#include "karta.h"
#include "gracz.h"
#include "prowadzacy.h"
#include <ctime>
#include <cstdlib>
#include <algorithm>

using namespace std;


int main()
{
     srand(time(NULL));


    HumanGracz G2("Jarjar","Binks");
    HumanGracz G1("Anakin","Skywalker");
    HumanGracz G3("Qui Gon","Jinn");
    HumanGracz G4("Obi Wan","Kenobi");
    CpuGracz   G5("BOT Waldo");
    CpuGracz   G6("BOT Toby");
    CpuGracz   G7("BOT Yuri");
    CpuGracz   G8("BOT Ulric");


    Prowadzacy p1(G5,G6,G7,G8);

    p1.PassToPlayers();


    p1.MatchPlay();
    system("PAUSE");
    return 0;
}
