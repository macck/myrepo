﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityHomework.Model
{
    public class User : Entity
    {
        public string NickName { get; set; }
        public string Password { get; set; }
        public virtual Resource Resources { get; set; }
        public virtual TownHall Hall { get; set; }
        public virtual GoldMine Mine { get; set; }
        public virtual IList<Hero> Heroes { get; set; }
    }
}
