﻿namespace MaciejMajkaLab1
{
    partial class NewWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLoopExample = new System.Windows.Forms.Label();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.textBoxTreeSize = new System.Windows.Forms.TextBox();
            this.labelTreeSize = new System.Windows.Forms.Label();
            this.richTextBoxTree = new System.Windows.Forms.RichTextBox();
            this.buttonMusicControl = new System.Windows.Forms.Button();
            this.pictureBoxGift = new System.Windows.Forms.PictureBox();
            this.textBoxTreeChar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGift)).BeginInit();
            this.SuspendLayout();
            // 
            // labelLoopExample
            // 
            this.labelLoopExample.AutoSize = true;
            this.labelLoopExample.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLoopExample.Location = new System.Drawing.Point(159, 9);
            this.labelLoopExample.Name = "labelLoopExample";
            this.labelLoopExample.Size = new System.Drawing.Size(177, 25);
            this.labelLoopExample.TabIndex = 0;
            this.labelLoopExample.Text = "Generator drzewka";
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(175, 359);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(150, 40);
            this.buttonGenerate.TabIndex = 2;
            this.buttonGenerate.Text = "GENERUJ";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // textBoxTreeSize
            // 
            this.textBoxTreeSize.Location = new System.Drawing.Point(46, 386);
            this.textBoxTreeSize.Name = "textBoxTreeSize";
            this.textBoxTreeSize.Size = new System.Drawing.Size(57, 20);
            this.textBoxTreeSize.TabIndex = 3;
            this.textBoxTreeSize.TextChanged += new System.EventHandler(this.textBoxTreeSize_TextChanged);
            // 
            // labelTreeSize
            // 
            this.labelTreeSize.AutoSize = true;
            this.labelTreeSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTreeSize.Location = new System.Drawing.Point(12, 363);
            this.labelTreeSize.Name = "labelTreeSize";
            this.labelTreeSize.Size = new System.Drawing.Size(131, 20);
            this.labelTreeSize.TabIndex = 4;
            this.labelTreeSize.Text = "Poziomy drzewka";
            this.labelTreeSize.Click += new System.EventHandler(this.label1_Click);
            // 
            // richTextBoxTree
            // 
            this.richTextBoxTree.Location = new System.Drawing.Point(12, 37);
            this.richTextBoxTree.Name = "richTextBoxTree";
            this.richTextBoxTree.Size = new System.Drawing.Size(460, 316);
            this.richTextBoxTree.TabIndex = 5;
            this.richTextBoxTree.Text = "";
            this.richTextBoxTree.TextChanged += new System.EventHandler(this.richTextBoxTree_TextChanged);
            // 
            // buttonMusicControl
            // 
            this.buttonMusicControl.Location = new System.Drawing.Point(357, 368);
            this.buttonMusicControl.Name = "buttonMusicControl";
            this.buttonMusicControl.Size = new System.Drawing.Size(75, 23);
            this.buttonMusicControl.TabIndex = 6;
            this.buttonMusicControl.Text = "STOP";
            this.buttonMusicControl.UseVisualStyleBackColor = true;
            this.buttonMusicControl.Click += new System.EventHandler(this.buttonMusicControl_Click);
            // 
            // pictureBoxGift
            // 
            this.pictureBoxGift.Location = new System.Drawing.Point(188, 405);
            this.pictureBoxGift.Name = "pictureBoxGift";
            this.pictureBoxGift.Size = new System.Drawing.Size(125, 125);
            this.pictureBoxGift.TabIndex = 7;
            this.pictureBoxGift.TabStop = false;
            this.pictureBoxGift.Visible = false;
            this.pictureBoxGift.Click += new System.EventHandler(this.pictureBoxPresent_Click);
            // 
            // textBoxTreeChar
            // 
            this.textBoxTreeChar.Location = new System.Drawing.Point(46, 431);
            this.textBoxTreeChar.MaxLength = 1;
            this.textBoxTreeChar.Name = "textBoxTreeChar";
            this.textBoxTreeChar.Size = new System.Drawing.Size(57, 20);
            this.textBoxTreeChar.TabIndex = 8;
            this.textBoxTreeChar.TextChanged += new System.EventHandler(this.textBoxTreeChar_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 408);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Znak do rysowania";
            // 
            // NewWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PapayaWhip;
            this.ClientSize = new System.Drawing.Size(484, 538);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxTreeChar);
            this.Controls.Add(this.pictureBoxGift);
            this.Controls.Add(this.buttonMusicControl);
            this.Controls.Add(this.richTextBoxTree);
            this.Controls.Add(this.labelTreeSize);
            this.Controls.Add(this.textBoxTreeSize);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.labelLoopExample);
            this.Name = "NewWindow";
            this.Text = "Wesołe okienko";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGift)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLoopExample;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.TextBox textBoxTreeSize;
        private System.Windows.Forms.Label labelTreeSize;
        private System.Windows.Forms.RichTextBox richTextBoxTree;
        private System.Windows.Forms.Button buttonMusicControl;
        private System.Windows.Forms.PictureBox pictureBoxGift;
        private System.Windows.Forms.TextBox textBoxTreeChar;
        private System.Windows.Forms.Label label1;
    }
}