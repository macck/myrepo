﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Diagnostics;
using System.IO;

namespace nixie
{
    public partial class SettingsForm : Form
    {
        private string BootloaderPath;

        public SettingsForm()
        {
            InitializeComponent();
        }


        //ladowanie aktualnych wartosci
        private void SettingsForm_Load(object sender, EventArgs e)
        {
            Baud.Value = Config.PortBaud;

            //odswiezanie listy portow
            RefreshButton_Click(null, null); 

            //przepisanie nazw obslugiwanych AVRow do comboboxa
            AvrCombo.DataSource = Enum.GetValues(typeof(AvrSignatures));
            AvrCombo.SelectedItem = Config.AvrSignature;

            BootloaderPath = Config.BootloaderPath;
        }


        //odswiezanie listy portow
        private void RefreshButton_Click(object sender, EventArgs e)
        {
            PortName.Items.Clear();
            foreach (string port in SerialPort.GetPortNames())
            {
                PortName.Items.Add(port);
            }

            PortName.SelectedItem = Config.PortName;
        }


        //anulowanie
        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }


        //wyjscie
        private void ApplyButton_Click(object sender, EventArgs e)
        {
            SaveSettings();
            DialogResult = DialogResult.OK;
        }


        //wybieranie lokalizacji bootloadera
        private void SelectBootloaderBtn_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.InitialDirectory = Path.GetDirectoryName(BootloaderPath);
            }
            catch { }

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                BootloaderPath = openFileDialog1.FileName;
                openFileDialog1.FileName = Path.GetFileName(openFileDialog1.FileName);
            }
        }


        //otwieranie bootloadera
        private void BootloaderButton_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSettings();
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = Config.BootloaderPath;
                startInfo.Arguments = "-s " + ((int)Config.AvrSignature).ToString() +
                                      " -p " + Config.PortName +
                                      " -rb " + Config.PortBaud.ToString() +
                                      " -rs " + Config.ResetString;
                Process.Start(startInfo);
            }
            catch
            {
                Common.ShowError("Wystąpił błąd podczas uruchamiania bootloadera. Prawdopodobnie podana ścieżka jest błędna.");
            }
        }


        //zapisywanie ustawien do pliku
        private void SaveSettings()
        {
            Config.PortBaud = (int)Baud.Value;
            Config.PortName = PortName.SelectedItem.ToString();
            Config.AvrSignature = (AvrSignatures)AvrCombo.SelectedItem;
            Config.BootloaderPath = BootloaderPath;
            Config.Save();
        }
    }
}
