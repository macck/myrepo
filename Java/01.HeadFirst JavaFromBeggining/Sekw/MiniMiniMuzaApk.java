import javax.sound.midi.*;
import java.util.*;

public class MiniMiniMuzaApk{
	
	public void graj()
	{
		try {
			Sequencer sekwenser = MidiSystem.getSequencer();
			sekwenser.open();
			Sequence sekw = new Sequence(Sequence.PPQ, 4);
			Track sciezka = sekw.createTrack();
			
			ShortMessage a = new  ShortMessage();
			a.setMessage(144,1,66,100);
			MidiEvent nutaP = new MidiEvent(a,1);
			sciezka.add(nutaP);
			
			ShortMessage b = new ShortMessage();
			b.setMessage(128,1,66,100);
			MidiEvent nutaK = new MidiEvent(b,6);
			sciezka.add(nutaK);
			
			ShortMessage c = new ShortMessage();
			b.setMessage(144,1,33,100);
			MidiEvent nutaP2 = new MidiEvent(c,6);
			sciezka.add(nutaP2);
			
			ShortMessage d = new ShortMessage();
			b.setMessage(128,1,66,100);
			MidiEvent nutaK2 = new MidiEvent(b,12);
			sciezka.add(nutaK2);
			
			
			
			sekwenser.setSequence(sekw);
			sekwenser.start();
			
		
		} catch (Exception ex){ ex.printStackTrace();
			System.out.println("PROBLEM!");
		}
		
		
	} // end graj
	
	
	
	public static void main(String[] args){
		MiniMiniMuzaApk mt = new MiniMiniMuzaApk();
		mt.graj();
		
	}
	
	
}