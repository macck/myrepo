﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectCatalog
{
    [Serializable]
    public class Guitar
    {
        // These are needed to construct object
        public string Producent { get; set; }
        public string Model { get; set; }
        public int Struny { get; set; }
        public string Typ { get; set; }
        public string Kolor { get; set; }
        // --------------------------------------

        protected string description;
        protected string pathToImage;

        public Guitar(string manufacturer,string model, int strings, string typeOfGuitar, string color)
        {
            this.Producent = manufacturer;
            this.Model = model;
            this.Struny = strings;
            this.Typ = typeOfGuitar;
            this.Kolor = color;
        }

        public void SetDescription(string description)
        {
            this.description = description;
        }

        public void SetPathToImage(string path)
        {
            this.pathToImage = path;
        }

        public string GetDescription()
        {
            return description;
        }

        public string GetPathToImage()
        {
            return pathToImage;
        }
    }
}
