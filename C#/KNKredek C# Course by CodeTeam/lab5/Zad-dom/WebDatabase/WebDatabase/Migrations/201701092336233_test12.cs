namespace WebDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test12 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ProductOrders", new[] { "Order_Id1" });
            DropColumn("dbo.ProductOrders", "Order_Id");
            RenameColumn(table: "dbo.ProductOrders", name: "Order_Id1", newName: "Order_Id");
            AlterColumn("dbo.ProductOrders", "Order_Id", c => c.Int());
            CreateIndex("dbo.ProductOrders", "Order_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ProductOrders", new[] { "Order_Id" });
            AlterColumn("dbo.ProductOrders", "Order_Id", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.ProductOrders", name: "Order_Id", newName: "Order_Id1");
            AddColumn("dbo.ProductOrders", "Order_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductOrders", "Order_Id1");
        }
    }
}
